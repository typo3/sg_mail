<?php

use SGalinski\SgMail\Hooks\FormEditorBeforeFormSave;
use SGalinski\SgMail\Hooks\FormManagerBeforeDeleteHook;
use SGalinski\SgMail\Hooks\ProcessDatamap;
use SGalinski\SgMail\Service\RegisterService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Controller\FormManagerController;

call_user_func(
	static function () {
		ExtensionManagementUtility::addTypoScriptSetup(
			'@import "EXT:sg_mail/Configuration/TypoScript/setup.typoscript"'
		);

		// Datamap process hook
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = ProcessDatamap::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['beforeFormSave'][] = FormEditorBeforeFormSave::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['beforeFormDelete'][] = FormManagerBeforeDeleteHook::class;

		// Cache registration
		$registerService = GeneralUtility::makeInstance(
			RegisterService::class
		);
		$registerService->registerCache();
	}
);
