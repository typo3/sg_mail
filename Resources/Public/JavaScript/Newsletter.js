/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
import DocumentService from '@typo3/core/document-service.js';

var Newsletter = {
	init: function() {
		document.getElementById('newsletter-send-real-emails-button').addEventListener('click', function () {
			const confirmMessage = TYPO3.lang['backend.newsletter.confirm_send'];
			if (window.confirm(confirmMessage)) {
				document.getElementById('sgMail-newsletter-form > div.form-group input[type=submit]').disabled = true;
				document.getElementById('newsletter-send-real-emails-hidden-field').value = 1;
				document.getElementById('sgMail-newsletter-form').submit();
				return true;
			} else {
				return false;
			}
		});

		document.getElementById('newsletter-send-preview-emails-button').addEventListener('click', function () {
			document.getElementById('sgMail-newsletter-form > div.form-group input[type=submit]').disabled = true;
			document.getElementById('newsletter-send-real-emails-hidden-field').value = 0;
			document.getElementById('sgMail-newsletter-form').submit();
			return true;
		});
	}
}

DocumentService.ready().then(() => {
	Newsletter.init();
});
