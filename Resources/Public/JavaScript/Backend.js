/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
import DocumentService from '@typo3/core/document-service.js';
import ModuleMenu from '@typo3/backend/module-menu.js';
import Modal from '@typo3/backend/modal.js';
import Viewport from '@typo3/backend/viewport.js';
import Severity from '@typo3/backend/severity.js';
import DateTimePicker from '@typo3/backend/date-time-picker.js';

const SgMail = {
	resetTemplateListener: function(_event) {
		_event.preventDefault();
		const link = _event.target.closest('a');

		const confirmModal = Modal.confirm('', TYPO3.lang['backend.delete_template'], Severity.warning, [
			{
				text: TYPO3?.lang?.['button.ok'] || 'OK',
				btnClass: 'btn-warning',
				trigger: function() {
					confirmModal.hideModal();
					window.location = link.href;
				}
			},{
				text: TYPO3?.lang?.['button.cancel'] || 'Cancel',
				trigger: function() {
					confirmModal.hideModal();
				}
			}
		]);
	},
	deleteTemplateListener: function(_event) {
		_event.preventDefault();
		_event.stopPropagation();

		const link = _event.target.closest('a');
		const confirmModal = Modal.confirm('', TYPO3.lang['backend.delete_template_custom'], Severity.warning, [
			{
				text: TYPO3?.lang?.['button.ok'] || 'OK',
				btnClass: 'btn-warning',
				trigger: function() {
					confirmModal.hideModal();

					window.location = link.href;
				}
			},{
				text: TYPO3?.lang?.['button.cancel'] || 'Cancel',
				trigger: function() {
					confirmModal.hideModal();
				}
			}
		]);
	},
	sendMailListener: function(_event) {
		_event.preventDefault();
		const link = _event.target.closest('a');

		const confirmModal = Modal.confirm('', TYPO3.lang['backend.send_mail_manually'], Severity.warning, [
			{
				text: TYPO3?.lang?.['button.ok'] || 'OK',
				btnClass: 'btn-warning',
				trigger: function() {
					confirmModal.hideModal();
					window.location = link.href;
				}
			},{
				text: TYPO3?.lang?.['button.cancel'] || 'Cancel',
				trigger: function() {
					confirmModal.hideModal();
				}
			}
		]);
	},
	resendMailListener: function(_event) {
		_event.preventDefault();
		const link = _event.target.closest('a');

		const confirmModal = Modal.confirm('', TYPO3.lang['backend.send_mail_again'], Severity.warning, [
			{
				text: TYPO3?.lang?.['button.ok'] || 'OK',
				btnClass: 'btn-warning',
				trigger: function() {
					confirmModal.hideModal();
					window.location = link.href;
				}
			},{
				text: TYPO3?.lang?.['button.cancel'] || 'Cancel',
				trigger: function() {
					confirmModal.hideModal();
				}
			}
		]);
	},
	toggleMailBody: function(_event) {
		const uid = _event.target.dataset.uid;
		document.getElementById('toggle-' + uid).detach().appendTo('body').modal('show');
	},
	init: function() {
		document.querySelectorAll('.reset-btn').forEach((button) => {
			button.addEventListener('click', SgMail.resetTemplateListener);
		});
		const deleteTemplateButton = document.getElementById('delete-template-btn');
		if (deleteTemplateButton) {
			deleteTemplateButton.addEventListener('click', SgMail.deleteTemplateListener);
		}

		document.querySelectorAll('.btn-send-now').forEach((button) => {
			button.addEventListener('click', SgMail.sendMailListener)
		});
		document.querySelectorAll('.btn-resend').forEach((button) => {
			button.addEventListener('click', SgMail.resendMailListener);
		});
		document.querySelectorAll('.btn-toggle').forEach((button) => {
			button.addEventListener('click', SgMail.toggleMailBody);
		});
		const resetFilterButton = document.getElementById('filter-reset-btn');
		if(resetFilterButton) {
			resetFilterButton.addEventListener('click', function(event) {
				event.preventDefault();
				this.form.reset();
				event.target.closest('form').querySelectorAll('select option').forEach(function(element) {
					element.selected = false;
				});

				event.target.closest('form').querySelectorAll('input[type="checkbox"]').forEach(function(element) {
					element.checked = false;
				});

				document.querySelectorAll('.reset-me').forEach((reset) => {
					reset.value = '';
				});
				document.getElementById('filters-all').checked = true;

				this.form.submit();
			});
		}

		document.querySelectorAll('.sg-mail_pageswitch').forEach((pageswitch) => {
			pageswitch.addEventListener('click', function(event) {
				if (Viewport.NavigationContainer.PageTree !== undefined) {
					event.preventDefault();
					SgMail.goTo('web_SgMailMail', event.target.dataset.page, event.target.dataset.path);
				}
			});
		});

		document.querySelectorAll('.btn-preview').forEach((button) => {
			button.addEventListener('click', function(event) {
				event.preventDefault();
				let element;
				if (!event.target.classList.contains('btn-preview')) {
					element = event.target.closest('.btn-preview');
				} else {
					element = event.target;
				}

				SgMail.openMailPreview(element.dataset.href);
			});
		});

		const paginators = document.querySelectorAll('.paginator-input');
		for (const paginator of paginators) {
			paginator.addEventListener('keypress', (event) => {
				if (event.keyCode === 13) {
					event.preventDefault();
					const page = event.target.value;
					const url = event.target.dataset.uri;
					self.location.href = url.replace('987654321', page);
				}
			})
		}

		document.querySelectorAll('.t3js-datetimepicker').forEach(function(element) {
			DateTimePicker.initialize(element);
		});
	},
	jumpExt: function(URL, anchor) {
		const anc = anchor ? anchor : "";
		window.location.href = URL + (T3_THIS_LOCATION ? "&returnUrl=" + T3_THIS_LOCATION : "") + anc;
		return false;
	},
	jumpSelf: function(URL) {
		window.location.href = URL + (T3_RETURN_URL ? "&returnUrl=" + T3_RETURN_URL : "");
		return false;
	},
	jumpToUrl: function(URL) {
		window.location.href = URL;
		return false;
	},
	setHighlight: function(id) {
		top.fsMod.recentIds["web"] = id;
		top.fsMod.navFrameHighlightedID["web"] = "pages" + id + "_" + top.fsMod.currentBank;    // For highlighting
		if (top.content && top.content.nav_frame && top.content.nav_frame.refresh_nav) {
			top.content.nav_frame.refresh_nav();
		}
	},
	goTo: function(module, id) {
		const pageTreeNodes = Viewport.NavigationContainer.PageTree.instance.nodes;
		for (let nodeIndex in pageTreeNodes) {
			if (pageTreeNodes.hasOwnProperty(nodeIndex) && pageTreeNodes[nodeIndex].identifier === parseInt(id)) {
				Viewport.NavigationContainer.PageTree.selectNode(pageTreeNodes[nodeIndex]);
				break;
			}
		}

		ModuleMenu.App.showModule(module, 'id=' + id);
	},
	openMailPreview: function(url) {
		const iframe = document.createElement('iframe');
		iframe.src = url;
		const openMailModal = Modal.advanced({
			title: 'Preview',
			severity: Severity.info,
			buttons: [
				{
					text: TYPO3.lang['backend.preview_close'],
					btnClass: 'btn-default',
					trigger: function() {
						openMailModal.hideModal();
					}
				}
			],
			content: iframe,
			additionalCssClasses: ['mail-preview']
		});
	}
};
DocumentService.ready().then(() => {
	SgMail.init();
});

if (typeof window.jumpExt === 'undefined') {
	window.jumpExt = SgMail.jumpExt;
}

if (typeof window.jumpSelf === 'undefined') {
	window.jumpSelf = SgMail.jumpSelf;
}

if (typeof window.jumpToUrl === 'undefined') {
	window.jumpToUrl = SgMail.jumpToUrl;
}
