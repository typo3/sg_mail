define(['TYPO3/CMS/Form/Backend/FormEditor/Helper'], function(Helper) {
	return (function(Helper) {
		let _formEditorApp = null;

		function getFormEditorApp() {
			return _formEditorApp;
		}

		function _helperSetup() {
			Helper.bootstrap(getFormEditorApp());
		}

		function getPublisherSubscriber() {
			return getFormEditorApp().getPublisherSubscriber();
		}

		function _subscribeEvents() {
			getPublisherSubscriber().subscribe('view/inspector/editor/insert/perform', function(topic, args) {
				if (
					args[0]['templateName'] === 'Inspector-PropertyGridEditor'
					&& args[0]['identifier'] === 'mailToReplacement'
				) {
					const element = args[1][0];
					const gridContainer = element.querySelector('[data-identifier="propertyGridContainer"]');
					const gridHeader = gridContainer.querySelector('[data-identifier="headerRow"]');
					const thElementLabel = gridHeader.querySelector('th:nth-child(2)');
					thElementLabel.textContent = 'Search';
					const thElementValue = gridHeader.querySelector('th:nth-child(3)');
					thElementValue.textContent = 'Replacement';

					const thElementSelected = gridHeader.querySelector('[data-column="selected"]');
					thElementSelected.remove();

					const gridRows = gridContainer.querySelectorAll('[data-identifier="rowItem"]');
					for (const gridRow of gridRows) {
						const columns = gridRow.querySelectorAll('[data-identifier="column"]');
						for (const column of columns) {
							column.style = 'width: auto';
						}

						const columnSelected = gridRow.querySelector('[data-column="selected"]');
						columnSelected.remove();
					}

					const addRowItem = gridContainer.querySelector('[data-identifier="addRowItem"]');
					addRowItem.querySelector('[data-column="selected"]').remove();
				}
			});
		}

		function bootstrap(formEditorApp) {
			_formEditorApp = formEditorApp;
			_subscribeEvents();
		}

		return {
			bootstrap: bootstrap
		};
	})(Helper);
});
