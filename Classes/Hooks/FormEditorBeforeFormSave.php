<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Hooks;

use SGalinski\SgMail\Service\MailTemplateService;
use SGalinski\SgMail\Service\RegisterService;
use Symfony\Component\Yaml\Yaml;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * FormEditorBeforeFormSave
 */
class FormEditorBeforeFormSave {
	/**
	 * This contains type names of fields we don't care about in sg_mail
	 *
	 * @var array
	 */
	public const IGNORE_FIELDS = [
		'StaticText', 'GridRow', 'Fieldset', 'Page'
	];

	/**
	 * This contains the names of sg_mail finishers, so we can identify if we need to generate a template registration
	 *
	 * @var array
	 */
	public const MAIL_FINISHER = [
		'MailToUserFinisher', 'MailToAdminFinisher'
	];

	/**
	 * @param string $formPersistenceIdentifier
	 * @param array $formDefinition
	 * @return array
	 * @throws NoSuchCacheException
	 */
	public function beforeFormSave(string $formPersistenceIdentifier, array $formDefinition): array {
		// immediately exit when no finisher is defined (that means no "sg_mail" integration anyway)
		if (!isset($formDefinition['finishers'])) {
			return $formDefinition;
		}

		$registrationService = GeneralUtility::makeInstance(RegisterService::class);

		/** @var array $finishers */
		$finishers = $formDefinition['finishers'];
		foreach ($finishers as $key => $finisher) {
			if (!\in_array($finisher['identifier'], self::MAIL_FINISHER, TRUE)) {
				continue;
			}

			// retrieve the extension and template key and jump out of loop
			$extensionKey = isset($finisher['options']['extension']) ?
				(string) $finisher['options']['extension'] : 'sg_mail';
			$extensionKey = $extensionKey ?: 'sg_mail';

			// if no template key was explicitly set, use the form identifier as template key
			$templateKey = $finisher['options']['template'];
			if ($templateKey === '') {
				$templateKey = $formDefinition['identifier'] . '-' . $finisher['identifier'];
				// pre-fill the template key field, since no custom key was defined by the editor
				$formDefinition['finishers'][$key]['options']['template'] = $templateKey;
			}

			// if there was no sg mail finisher or a missing key then simply exit the function
			if ($templateKey === '') {
				throw new \InvalidArgumentException('Missing template or extension key!');
			}

			// parse yaml for form fields
			if (strpos($formPersistenceIdentifier, 'EXT:') === 0) {
				$absoluteFilePath = GeneralUtility::getFileAbsFileName($formPersistenceIdentifier);
			} else {
				[$storageUid, $fileIdentifier] = explode(':', $formPersistenceIdentifier, 2);
				$storageRepository = GeneralUtility::makeInstance(StorageRepository::class);

				$storage = $storageRepository->findByUid($storageUid);
				if (!$storage instanceof ResourceStorage || !$storage->isBrowsable()) {
					throw new \InvalidArgumentException(
						sprintf('Could not access storage with uid "%d".', $storageUid)
					);
				}
				$fileObject = $storage->getFile($fileIdentifier);
				if (!$fileObject) {
					throw new \InvalidArgumentException(
						sprintf('Could not access file with uid "%d".', $fileIdentifier)
					);
				}

				$absoluteFilePath = Environment::getPublicPath() . '/' . $fileObject->getPublicUrl();
			}

			try {
				$parsedYaml = Yaml::parse(file_get_contents($absoluteFilePath));
			} catch (\Exception $exception) {
				throw new \InvalidArgumentException(
					sprintf('Could not access storage "%s".', $absoluteFilePath)
				);
			}

			$renderables = self::getFormFieldsRecursive([0 => $parsedYaml]);

			// write the new Register.php file and add it to the registrationFiles array in the RegisterService
			$this->writeRegisterFile($renderables, $extensionKey, $templateKey);
		}

		// call register function in register service class
		$registrationService->clearCaches();
		$registrationService->getRegisterArray();
		return $formDefinition;
	}

	/**
	 * Builds the register array and saves it to the configured location
	 *
	 * @param array $renderables
	 * @param string $extensionKey
	 * @param string $templateKey
	 * @return void
	 */
	private function writeRegisterFile(array $renderables, string $extensionKey, string $templateKey): void {
		$markers = [];
		foreach ($renderables as $element) {
			// if markerName is explicitly set, override the registered identifier
			$markerName = '';
			if (isset($element['properties']['markerName']) && $element['properties']['markerName'] !== '') {
				$markerArray = GeneralUtility::trimExplode(',', $element['properties']['markerName'], FALSE, 2);
				$markerName = $markerArray[0];
			}

			$type = MailTemplateService::MARKER_TYPE_STRING;
			if ($element['type'] === 'Date') {
				$type = MailTemplateService::MARKER_TYPE_DATE;
			}

			$markerName = RegisterService::normalizeFormFieldMarkerName($markerName);
			$markerLabel = (isset($markerArray) ? $markerArray[1] ?? $markerArray[0] : 'No Label given!');
			$markers[$markerName] = [
				'identifier' => $markerName,
				'type' => $type,
				'value' => $markerName,
				'description' => $markerName,
				'markerLabel' => $markerLabel
			];
		}

		GeneralUtility::makeInstance(RegisterService::class)->writeRegisterFile(
			$templateKey,
			$extensionKey,
			$markers,
			$templateKey
		);
	}

	/**
	 * @param array $formData
	 * @return array
	 */
	private static function getFormFieldsRecursive(array $formData = []): array {
		$out = [];
		$renderables = [];
		foreach ($formData as $formElement) {
			if (isset($formElement['renderables'])) {
				$renderables[] = self::getFormFieldsRecursive($formElement['renderables']);
			} elseif (!\in_array($formElement['type'], self::IGNORE_FIELDS, TRUE)) {
				$out[] = $formElement;
			}
		}
		if (count($renderables)) {
			$out = \array_merge($out, ...$renderables);
		}
		return $out;
	}
}
