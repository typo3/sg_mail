<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Hooks;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

/**
 * Class ProcessDatamap
 * Hooks for synchronize urls directly on-the-fly while editing
 *
 * @package SGalinski\SgMail\Hooks
 */
class ProcessDatamap {
	/**
	 * Hook function for handling the enabling and disabling of FE users from the BE
	 *
	 * @param string $status
	 * @param string $table
	 * @param int|string $id
	 * @param array $fieldArray
	 * @param DataHandler $dataHandler
	 * @return void
	 */
	public function processDatamap_afterDatabaseOperations(
		string $status,
		string $table,
		$id,
		array $fieldArray,
		DataHandler $dataHandler
	): void {
		if (
			$table === 'tx_sgmail_domain_model_layout' && isset($fieldArray['default']) &&
			in_array($status, ['update', 'new'], TRUE)
		) {
			if (!(bool) $fieldArray['default']) {
				return;
			}

			if ($status === 'update') {
				$updatedRow = BackendUtility::getRecord('tx_sgmail_domain_model_layout', $id, 'pid, sys_language_uid');
				if ($updatedRow === NULL || (int) $updatedRow['sys_language_uid'] > 0) {
					return;
				}

				$pid = (int) $updatedRow['pid'];
			} else {
				if ((int) $fieldArray['sys_language_uid'] > 0) {
					return;
				}

				if (!MathUtility::canBeInterpretedAsInteger($id)) {
					$id = (int) $dataHandler->substNEWwithIDs[$id];
				}
				$pid = (int) $fieldArray['pid'];
			}

			$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
				'tx_sgmail_domain_model_layout'
			);
			$queryBuilder->getRestrictions()->removeAll();
			$queryBuilder->update('tx_sgmail_domain_model_layout')
				->where(
					$queryBuilder->expr()->and($queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pid)), $queryBuilder->expr()->neq('l10n_parent', $queryBuilder->createNamedParameter($id)), $queryBuilder->expr()->neq('uid', $queryBuilder->createNamedParameter($id)))
				)->set('default', 0)->executeStatement();
		}
	}
}
