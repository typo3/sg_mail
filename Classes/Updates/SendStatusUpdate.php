<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Updates;

use SGalinski\SgMail\Domain\Model\Mail;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class SendStatusUpdate
 *
 * @package SGalinski\SgMail\Updates
 */
#[UpgradeWizard('sgmail_sendstatusupdate')]
class SendStatusUpdate implements UpgradeWizardInterface {
	/**
	 * @inheritDoc
	 */
	public function getTitle(): string {
		return 'Update the send status of mails';
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription(): string {
		return 'This wizard updates the status of mails that have a sending_time set to sent appropriately.';
	}

	/**
	 * @inheritDoc
	 */
	public function executeUpdate(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
		$queryBuilder->update('tx_sgmail_domain_model_mail')
			->set('status', Mail::STATUS_SENT)->where($queryBuilder->expr()->gt('sending_time', 0), $queryBuilder->expr()->eq('status', $queryBuilder->createNamedParameter(Mail::STATUS_PENDING)))->executeStatement();
		return TRUE;
	}

	/**
	 * @inheritDoc
	 */
	public function updateNecessary(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
		$count = $queryBuilder->count('*')
			->from('tx_sgmail_domain_model_mail')->where($queryBuilder->expr()->gt('sending_time', 0), $queryBuilder->expr()->eq('status', $queryBuilder->createNamedParameter(Mail::STATUS_PENDING)))->executeQuery()->fetchOne();
		return $count > 0;
	}

	/**
	 * @inheritDoc
	 */
	public function getPrerequisites(): array {
		return [];
	}
}
