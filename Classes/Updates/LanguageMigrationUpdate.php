<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Updates;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class LanguageMigrationUpdate
 *
 * @package SGalinski\SgMail\Updates
 */
#[UpgradeWizard('sgmail_languagemigrationupdate')]
class LanguageMigrationUpdate implements UpgradeWizardInterface {
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return 'Language migration wizard';
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'This wizard migrates the language values of mails and templates to more reasonable values then two letter isocodes';
	}

	/**
	 * @return bool
	 */
	public function executeUpdate(): bool {
		$sites = GeneralUtility::makeInstance(SiteFinder::class)->getAllSites();
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		foreach ($sites as $site) {
			$languages = $site->getAllLanguages();
			foreach ($languages as $language) {
				$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
				$queryBuilder->update('tx_sgmail_domain_model_mail')
					->set('sys_language_uid', $language->getLanguageId())->where($queryBuilder->expr()->like('language', $queryBuilder->createNamedParameter($language->getTypo3Language())))->executeStatement();

				$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_sgmail_domain_model_template');
				$queryBuilder->update('tx_sgmail_domain_model_template')
					->set('sys_language_uid', $language->getLanguageId())->where($queryBuilder->expr()->like('language', $queryBuilder->createNamedParameter($language->getTypo3Language())))->executeStatement();
			}
		}

		return TRUE;
	}

	/**
	 * @return bool
	 */
	public function updateNecessary(): bool {
		try {
			$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
			$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
			$queryBuilder->select('language')->from('tx_sgmail_domain_model_mail')->executeQuery();
		} catch (\Exception $e) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * @return string[]
	 */
	public function getPrerequisites(): array {
		return [];
	}
}
