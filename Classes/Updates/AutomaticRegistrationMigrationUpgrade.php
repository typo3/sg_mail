<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Updates;

use SGalinski\SgMail\Service\RegisterService;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('sgmail_automaticregistrationmigration')]
class AutomaticRegistrationMigrationUpgrade implements UpgradeWizardInterface {
	/**
	 * @inheritDoc
	 */
	public function getTitle(): string {
		return 'Migrate the automatically created registration files';
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription(): string {
		return 'This wizard migrates the automatically created registration files from extensions inside extensions to a location inside the fileadmin';
	}

	/**
	 * @inheritDoc
	 */
	public function executeUpdate(): bool {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)
			->getConnectionForTable('tx_sgmail_domain_model_template');
		$qb = $connection->createQueryBuilder();
		$templates = $qb->select('template_name', 'sys_language_uid')
			->from('tx_sgmail_domain_model_template')
			->where(
				$qb->expr()->eq('extension_key', $qb->createNamedParameter('sg_mail'))
			)->executeQuery()->fetchAllAssociative();
		$templateFilesFolder = 'EXT:project_theme/Configuration/MailTemplates/Forms';
		$mailTemplateFiles = GeneralUtility::getFilesInDir(GeneralUtility::getFileAbsFileName(
			$templateFilesFolder
		));
		$fileadminConfigDir = Environment::getPublicPath()
			. '/fileadmin/'
			. ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_mail']['configurationLocation'] ?? 'SgMail')
			. '/' . RegisterService::CONFIG_PATH;
		// create new config folder if not present
		GeneralUtility::mkdir_deep($fileadminConfigDir);
		foreach ($templates as $template) {
			// check if the template is located at project_theme
			$hash = md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] . '|' . $template['template_name'] . '.php');
			$filename = $hash . '_' . $template['template_name'] . '.php';
			$oldFilepath = GeneralUtility::getFileAbsFileName($templateFilesFolder) . '/' . $filename;
			$newFilePath = $fileadminConfigDir . '/' . $filename;
			if (in_array($filename, $mailTemplateFiles, TRUE) && is_file($oldFilepath) && !is_file($newFilePath)) {
				rename(
					$oldFilepath,
					$newFilePath
				);
			}
		}

		return TRUE;
	}

	/**
	 * @inheritDoc
	 */
	public function updateNecessary(): bool {
		return TRUE;
	}

	/**
	 * @inheritDoc
	 */
	public function getPrerequisites(): array {
		return [];
	}
}
