<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\SgMail\Service\RegisterService;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\DocHeaderComponent;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use function count;

/**
 * Class AbstractController
 *
 * @package SGalinski\SgMail\Controller
 */
abstract class AbstractController extends ActionController {
	public const sessionKey = 'sg_mail_controller_session';
	public const DEFAULT_EXTENSION_KEY = 'sg_mail';

	/**
	 * @var BackendUserAuthentication
	 */
	protected BackendUserAuthentication $backendUserAuthentication;

	/**
	 * @param BackendUserAuthentication $backendUserAuthentication
	 */
	public function injectBackendUserAuthentication(BackendUserAuthentication $backendUserAuthentication): void {
		$this->backendUserAuthentication = $GLOBALS['BE_USER'];
	}

	/**
	 * @var ModuleTemplate
	 */
	protected ModuleTemplate $moduleTemplate;

	/**
	 * @var RegisterService
	 */
	protected RegisterService $registerService;

	/**
	 * @var Site
	 */
	protected Site $site;

	/**
	 * @param RegisterService $registerService
	 */
	public function injectRegisterService(RegisterService $registerService): void {
		$this->registerService = $registerService;
	}

	/**
	 * @var ModuleTemplateFactory
	 */
	protected ModuleTemplateFactory $moduleTemplateFactory;

	public function injectModuleTemplateFactory(ModuleTemplateFactory $moduleTemplateFactory): void {
		$this->moduleTemplateFactory = $moduleTemplateFactory;
	}

	public function initializeAction() {
		parent::initializeAction();
		$this->moduleTemplate = $this->moduleTemplateFactory->create($this->request);
	}

	/**
	 * Get data from the session
	 *
	 * @param string $key
	 * @return string
	 */
	protected function getFromSession(string $key): string {
		return $this->backendUserAuthentication->getSessionData(self::sessionKey . '_' . $key) ?? '';
	}

	/**
	 * Write data to the session
	 *
	 * @param string $key
	 * @param mixed $data
	 */
	protected function writeToSession(string $key, $data): void {
		$this->backendUserAuthentication->setAndSaveSessionData(self::sessionKey . '_' . $key, $data);
	}

	/**
	 * Get the mode from session and switch to it if necessary
	 *
	 */
	protected function switchMode(): void {
		$mode = $this->getFromSession('mode');
		$queryParams = $this->request->getQueryParams();
		if (isset($queryParams['parameters']['lastController'])) {
			$mode = $queryParams['parameters']['lastController'];
		}

		if ($mode) {
			$this->writeToSession('mode', $mode);
			if ('SGalinski\\SgMail\\Controller\\' . $mode . 'Controller' !== static::class) {
				$redirectResponse = $this->redirect('index', $this->getFromSession('mode'));
				throw new ImmediateResponseException($redirectResponse);
			}
		}
	}

	/**
	 * Check if a pid has been set and redirect if there is none
	 *
	 */
	protected function requireSite(): ?ResponseInterface {
		$pid = $this->getPid();
		if (!$pid) {
			/** @var ExtbaseRequestParameters $extbaseRequestParameters */
			$extbaseRequestParameters = $this->request->getAttribute('extbase');
			$originalRequest = $extbaseRequestParameters->getOriginalRequest();
			if ($originalRequest === NULL) {
				$originalRequest = $this->request;
			}

			return (new ForwardResponse('index'))
				->withControllerName('Site')
				->withExtensionName('SgMail')
				->withArguments(['originalRequest' => $originalRequest]);
		}

		try {
			$this->site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($pid);
		} catch (SiteNotFoundException $exception) {
			/** @var ExtbaseRequestParameters $extbaseRequestParameters */
			$extbaseRequestParameters = $this->request->getAttribute('extbase');
			$originalRequest = $extbaseRequestParameters->getOriginalRequest();
			if ($originalRequest !== NULL) {
				$extbaseRequestParameters->setOriginalRequest($originalRequest);
			} else {
				$extbaseRequestParameters->setOriginalRequest($this->request);
			}

			return (new ForwardResponse('index'))
				->withControllerName('Site')
				->withExtensionName('SgMail')
				->withArguments($this->request->getArguments());
		}

		return NULL;
	}

	/**
	 * Add the message from the request arguments as a flash message
	 *
	 */
	protected function addMessage(): void {
		if ($this->request->hasArgument('message')) {
			$this->addFlashMessage(
				$this->request->getArgument('message'),
				'',
				ContextualFeedbackSeverity::INFO
			);
		}
	}

	/**
	 * Get the page id from get parameters
	 *
	 * @return int
	 */
	protected function getPid(): int {
		return $this->request->getQueryParams()['id'] ?? 0;
	}

	/**
	 * Get the selected extension and template from the register
	 *
	 * @return array
	 * @throws NoSuchCacheException
	 */
	protected function getSelectedExtensionAndTemplateFromRegister(): array {
		$pageUid = $this->getPid();
		$registerArray = $this->registerService->getNonBlacklistedTemplates($pageUid);
		$selectedExtension = '';
		$selectedTemplate = '';
		if (!empty($registerArray)) {
			$selectedExtension = key($registerArray);
		}

		if (!empty($registerArray[$selectedExtension])) {
			$selectedTemplate = key($registerArray[$selectedExtension]);
		}

		return [$selectedExtension, $selectedTemplate];
	}

	/**
	 * Get the selected extension and template from session or register if the session is empty
	 *
	 * @return array
	 * @throws NoSuchCacheException
	 */
	protected function getSelectedExtensionAndTemplate(): array {
		if (
			$this->getFromSession('selectedTemplate') !== NULL &&
			$this->getFromSession('selectedExtension') !== NULL &&
			!$this->registerService->isTemplateBlacklisted(
				$this->getFromSession('selectedExtension'),
				$this->getFromSession('selectedTemplate'),
				$this->getPid()
			)
		) {
			return [$this->getFromSession('selectedExtension'), $this->getFromSession('selectedTemplate')];
		}

		return $this->getSelectedExtensionAndTemplateFromRegister();
	}

	/**
	 * This function checks an array of mail addresses on validity and returns false if one of them is invalid
	 *
	 * @param array $addresses
	 * @return bool
	 */
	protected function checkMailAddresses(array $addresses): bool {
		if (count($addresses) > 0) {
			foreach ($addresses as $address) {
				if (
					trim($address) !== '' &&
					!filter_var($address, FILTER_VALIDATE_EMAIL) &&
					!(
						str_contains($address, '{')
						&& strpos($address, '{') < strpos($address, '}')
					)
				) {
					$message = LocalizationUtility::translate('backend.error_cc', 'sg_mail');
					$this->addFlashMessage($message, '', ContextualFeedbackSeverity::WARNING);
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	/**
	 * Make the docheader component
	 */
	protected function makeDocheader(): void {
		$pageUid = $this->getPid();
		$pageInfo = BackendUtility::readPageAccess($pageUid, $GLOBALS['BE_USER']->getPagePermsClause(1));
		$docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();

		if ($pageInfo === FALSE) {
			$pageInfo = ['uid' => $pageUid];
		}

		$docHeaderComponent->setMetaInformation($pageInfo);
		$this->makeButtons($docHeaderComponent);
	}

	/**
	 * create buttons for the backend module header
	 *
	 * @param DocHeaderComponent $docHeaderComponent
	 */
	protected function makeButtons(DocHeaderComponent $docHeaderComponent): void {
		$buttonBar = $docHeaderComponent->getButtonBar();
		/** @var IconFactory $iconFactory */
		$iconFactory = GeneralUtility::makeInstance(IconFactory::class);
		$locallangPath = 'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:';
		// Refresh
		$refreshButton = $buttonBar->makeLinkButton()
			->setHref(GeneralUtility::getIndpEnv('REQUEST_URI'))
			->setTitle(
				LocalizationUtility::translate(
					$locallangPath . 'labels.reload'
				)
			)
			->setIcon($iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));
		$buttonBar->addButton($refreshButton, ButtonBar::BUTTON_POSITION_RIGHT);
		// shortcut button
		$shortcutButton = $buttonBar->makeShortcutButton()
			->setDisplayName('Shortcut')
			->setRouteIdentifier($this->request->getPluginName())
			->setArguments(
				[
					'id' => [],
					'M' => []
				]
			);
		$buttonBar->addButton($shortcutButton, ButtonBar::BUTTON_POSITION_RIGHT);
	}
}
