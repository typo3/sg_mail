<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use function explode;
use function str_getcsv;
use function str_replace;

/**
 * Controller for the configuration mode of the backend module
 */
class ConfigurationController extends AbstractController {
	/**
	 * @param string $mode
	 * @param string|null $selectedTemplate
	 * @param string|null $selectedExtension
	 * @param array $filters
	 * @return ResponseInterface
	 * @throws NoSuchCacheException
	 */
	public function indexAction(
		string $mode = 'new',
		string $selectedTemplate = NULL,
		string $selectedExtension = NULL,
		array $filters = []
	): ResponseInterface {
		if ($this->request->hasArgument('message')) {
			$this->addFlashMessage($this->request->getArgument('message'), '', ContextualFeedbackSeverity::ERROR);
		}

		if ($selectedTemplate === NULL || $selectedTemplate === '') {
			[$selectedExtension, $selectedTemplate] = $this->getSelectedExtensionAndTemplate();
		}

		if ($mode === 'edit') {
			$registerArray = $this->registerService->getNonBlacklistedTemplates($this->getPid());
			$templateToEdit = $registerArray[$selectedExtension][$selectedTemplate];
			$editableMarkers = [];
			foreach ($templateToEdit['marker'] as $arr) {
				$editableMarkers[] = [
					'identifier' => $arr['marker'] . ',' . $arr['markerLabel'],
					'value' => $arr['value'],
					'description' => $arr['description']
				];
			}

			$csv = '';
			foreach ($editableMarkers as $arr) {
				$csv .= implode(';', $arr) . "\r\n";
			}

			$this->moduleTemplate->assignMultiple(
				[
					'templateName' => $selectedTemplate,
					'subject' => $templateToEdit['subject'],
					'templateContent' => $templateToEdit['templateContent'],
					'csv' => $csv,
					'extensionKey' => $selectedExtension,
					'description' => $templateToEdit['description'],
					'editMode' => 1
				]
			);
		}

		$this->makeDocheader();
		$this->moduleTemplate->assignMultiple(
			[
				'extensionKey' => self::DEFAULT_EXTENSION_KEY,
				'selectedTemplateFilter' => $filters['filterTemplate'] ?? '',
				'selectedExtensionFilter' => $filters['filterExtension'] ?? '',
				'selectedTemplateKey' => $selectedTemplate,
				'selectedExtensionKey' => $selectedExtension,
				'mode' => 'editor'
			]
		);

		return $this->moduleTemplate->renderResponse('Index');
	}

	/**
	 * Create the template or display errors that occured
	 *
	 * @throws NoSuchCacheException
	 */
	public function createAction(): ResponseInterface {
		if (!$this->request->hasArgument('configuration')) {
			$this->redirect(
				'index',
				'Configuration',
				NULL,
				['message' => LocalizationUtility::translate('backend.create_error', 'sg_mail')]
			);
		}

		/** @var array $configuration */
		$configuration = $this->request->getArgument('configuration');
		$templateName = $configuration['templateName'];
		$extensionKey = $configuration['extensionKey'];
		$csv = str_replace("\r", '', $configuration['csv']);
		$subject = $configuration['subject'];
		$description = $configuration['description'];
		$renderWithNl2br = (bool) $configuration['renderWithNl2br'];
		$markersCsv = explode("\n", $csv);
		$markers = [];
		foreach ($markersCsv as $markerCsv) {
			$rowArray = str_getcsv($markerCsv, ';');
			if (!$rowArray[0]) {
				continue;
			}

			$markerArray = GeneralUtility::trimExplode(',', $rowArray[0], FALSE, 2);
			$markerName = $markerArray[0];
			$markerLabel = $markerArray[1] ?? $markerName;
			$markers[] = [
				'identifier' => $markerName,
				'value' => $rowArray[1] ?? '',
				'description' => $rowArray[2] ?? '',
				'markerLabel' => $markerLabel
			];
		}
		$this->registerService->writeRegisterFile(
			$templateName,
			$extensionKey,
			$markers,
			$subject,
			$description,
			$renderWithNl2br
		);
		$this->registerService->clearCaches();
		// store selected template & extension key in the session
		$this->writeToSession('selectedTemplate', $templateName);
		$this->writeToSession('selectedExtension', self::DEFAULT_EXTENSION_KEY);

		return $this->redirect(
			'index',
			'Mail',
			NULL,
			['message' => LocalizationUtility::translate('backend.create_message', 'sg_mail')]
		);
	}

	/**
	 * Edit the template or display errors that occurred
	 *
	 * @throws NoSuchCacheException
	 */
	public function editAction(): ResponseInterface {
		if (!$this->request->hasArgument('configuration')) {
			$this->redirect(
				'index',
				'Configuration',
				NULL,
				['message' => LocalizationUtility::translate('backend.create_error', 'sg_mail')]
			);
		}

		/** @var array $configuration */
		$configuration = $this->request->getArgument('configuration');
		$templateName = $configuration['templateName'];
		$extensionKey = $configuration['extensionKey'];
		$oldTemplateName = $configuration['oldTemplateName'];
		$oldExtensionKey = $configuration['oldExtensionKey'];
		$csv = str_replace("\r", '', $configuration['csv']);
		$subject = $configuration['subject'];
		$description = $configuration['description'];
		$markersCsv = explode("\n", $csv);
		$markers = [];
		foreach ($markersCsv as $markerCsv) {
			$rowArray = str_getcsv($markerCsv, ';');
			if (!$rowArray[0]) {
				continue;
			}

			$markerArray = GeneralUtility::trimExplode(',', $rowArray[0], FALSE, 2);
			$markerName = $markerArray[0];
			$markerLabel = $markerArray[1] ?? $markerName;
			$markers[] = [
				'identifier' => $rowArray[0],
				'value' => $rowArray[1],
				'description' => $rowArray[2],
				'markerLabel' => $markerLabel
			];
		}

		$this->registerService->migrateTemplateEntries(
			$oldTemplateName,
			$oldExtensionKey,
			$templateName,
			$extensionKey
		);
		$this->registerService->deleteRegisterFile($oldTemplateName);
		$this->registerService->writeRegisterFile(
			$templateName,
			$extensionKey,
			$markers,
			$subject,
			$description
		);
		$this->registerService->clearCaches();
		// store selected template & extension key in the session
		$this->writeToSession('selectedTemplate', $templateName);
		$this->writeToSession('selectedExtension', self::DEFAULT_EXTENSION_KEY);

		return $this->redirect(
			'index',
			'Mail',
			NULL,
			['message' => LocalizationUtility::translate('backend.edit_message', 'sg_mail')]
		);
	}

	/**
	 * Edit the template or display errors that occured
	 *
	 * @param string $selectedTemplate
	 * @param string $selectedExtension
	 * @return ResponseInterface
	 * @throws NoSuchCacheException
	 */
	public function deleteAction(string $selectedTemplate, string $selectedExtension): ResponseInterface {
		$this->registerService->deleteTemplate($selectedExtension, $selectedTemplate);

		return $this->redirect(
			'index',
			'Mail',
			NULL,
			['message' => LocalizationUtility::translate('backend.delete_message', 'sg_mail')]
		);
	}
}
