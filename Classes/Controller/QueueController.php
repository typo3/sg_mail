<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Doctrine\DBAL\Driver\Exception;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgMail\Domain\Model\Mail;
use SGalinski\SgMail\Domain\Repository\MailRepository;
use SGalinski\SgMail\Pagination\Pagination;
use SGalinski\SgMail\Service\BackendService;
use SGalinski\SgMail\Service\MailTemplateService;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Controller for the mail templating service module
 */
class QueueController extends AbstractController {
	/**
	 * @var MailRepository
	 */
	protected MailRepository $mailRepository;

	/**
	 * Inject the MailRepository
	 *
	 * @param MailRepository $mailRepository
	 */
	public function injectMailRepository(MailRepository $mailRepository): void {
		$this->mailRepository = $mailRepository;
	}

	public function initializeAction(): void {
		parent::initializeAction();
		$this->moduleTemplate->assign('controller', 'Queue');
	}

	/**
	 * @param string|null $selectedTemplate
	 * @param string|null $selectedExtension
	 * @param array $filters
	 * @param int $currentPage
	 * @return ResponseInterface
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 */
	public function indexAction(
		string $selectedTemplate = NULL,
		string $selectedExtension = NULL,
		array $filters = [],
		int $currentPage = 1
	): ResponseInterface {
		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}

		$this->initSessionAndFilters($filters);
		$this->moduleTemplate->assignMultiple([
			'selectedTemplateFilter' => $filters['filterTemplate'] ?? '',
			'selectedExtensionFilter' => $filters['filterExtension'] ?? ''
		]);
		if ($selectedTemplate === NULL || $selectedTemplate === '') {
			[$selectedExtension, $selectedTemplate] = $this->getSelectedExtensionAndTemplateFromRegister();
		}

		// Setup Pagination
		$itemsPerPage = 10;
		$queueQueryBuilder = $this->mailRepository->findAllEntries(
			$this->getPid(),
			$itemsPerPage,
			$filters
		);
		$pagination = GeneralUtility::makeInstance(Pagination::class, $queueQueryBuilder, $currentPage, $itemsPerPage);
		$this->moduleTemplate->assign('pagination', $pagination);

		if (isset($filters['filterSent']) && !$filters['filterSent']) {
			$filters['filterSent'] = 0;
		}

		$site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($this->getPid());
		$this->moduleTemplate->assignMultiple([
			'selectedTemplateKey' => $selectedTemplate,
			'selectedExtensionKey' => $selectedExtension,
			'templates' => $this->registerService->getNonBlacklistedTemplates($this->site->getRootPageId()),
			'templatesFilter' => $this->registerService->getTemplatesForFilter($this->site->getRootPageId()),
			'languages' => $site->getAvailableLanguages($this->backendUserAuthentication),
			'mode' => 'queue',
			'filters' => $filters,
			'filterFields' => [
				BackendService::SENDER_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.from',
					'SgMail'
				),
				BackendService::RECIPIENT_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.to',
					'SgMail'
				),
				BackendService::SUBJECT_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.subject',
					'SgMail'
				),
				BackendService::MAILTEXT_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.mailtext',
					'SgMail'
				),
				BackendService::CC_FILTER_OPTION => LocalizationUtility::translate('backend.filter.cc', 'SgMail'),
				BackendService::BCC_FILTER_OPTION => LocalizationUtility::translate('backend.filter.bcc', 'SgMail'),
				BackendService::FROM_NAME_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.from_name',
					'SgMail'
				),
				BackendService::REPLY_TO_NAME_FILTER_OPTION => LocalizationUtility::translate(
					'backend.filter.reply_to',
					'SgMail'
				),
			]
		]);

		$this->makeDocheader();
		return $this->moduleTemplate->renderResponse('Index');
	}

	/**
	 * send or resend a mail in the queue
	 *
	 * @param Mail $mail
	 * @return ResponseInterface
	 */
	public function sendMailAction(Mail $mail): ResponseInterface {
		$mailService = new MailTemplateService();
		try {
			$mailService->sendMailsFromQueue([$mail]);
		} catch (\TYPO3\CMS\Core\Exception $exception) {
			$message = LocalizationUtility::translate(
				'backend.error_mail_queue',
				'sg_mail'
			);
			$this->addFlashMessage($message, '', ContextualFeedbackSeverity::ERROR);
			return $this->redirect('index', NULL, NULL, $this->request->getArguments());
		}

		if ($mail->getStatus() !== Mail::STATUS_ERROR) {
			$this->addFlashMessage(
				LocalizationUtility::translate('backend.success_mail_queue', 'sg_mail')
			);
		} else {
			$this->addFlashMessage(
				$mail->getErrorMessage(),
				'',
				ContextualFeedbackSeverity::ERROR
			);
		}

		return $this->redirect('index', NULL, NULL, $this->request->getArguments());
	}

	/**
	 * Preview for mail
	 *
	 * @param Mail $mail
	 * @return ResponseInterface
	 */
	public function previewAction(Mail $mail): ResponseInterface {
		$this->moduleTemplate->assign('mail', $mail);
		return $this->moduleTemplate->renderResponse('Preview');
	}

	/**
	 * Download the queue data as a csv file, respecting the filter settings
	 *
	 * @param array $filters
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function exportAction(array $filters = []): void {
		$this->initSessionAndFilters($filters);
		// stop output buffering because it's pointless here
		ob_end_clean();
		header('Content-Type: application/force-download');
		header('Content-Transfer-Encoding: Binary');
		header('Content-Disposition: attachment; filename="export.csv"');
		BackendService::writeCsvFromQueue($filters);
		exit(0);
	}

	/**
	 * Initializes the session & retrieves previously sent values from the session, so that the filters won't get lost,
	 * when the user switches between the backend module actions (template editor / mail queue / template layouts),
	 * or edits a mail record and comes back to the previous module action.
	 *
	 * @param array $filters
	 */
	protected function initSessionAndFilters(array &$filters): void {
		$this->switchMode();

		/** @var BackendUserAuthentication $backendUser */
		$backendUser = $GLOBALS['BE_USER'];
		if ($filters === []) {
			$filters = $backendUser->getModuleData('web_SgMailMail_filters', 'ses') ?: [];
		} else {
			if ($filters['filterTemplate'] !== '') {
				$extensionTemplateFilterArray = explode('###', $filters['filterTemplate']);
				$filters['filterExtension'] = $extensionTemplateFilterArray[0];
				$filters['filterTemplate'] = $extensionTemplateFilterArray[1];
			}

			$backendUser->pushModuleData('web_SgMailMail_filters', $filters);
		}
	}
}
