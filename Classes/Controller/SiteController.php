<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Doctrine\DBAL\Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Request;

/**
 * Class SiteController
 *
 * @package SGalinski\SgMail\Controller
 */
class SiteController extends AbstractController {
	protected ConnectionPool $connectionPool;
	protected SiteFinder $siteFinder;

	public function __construct(
		ConnectionPool $connectionPool,
		SiteFinder $siteFinder
	) {
		$this->connectionPool = $connectionPool;
		$this->siteFinder = $siteFinder;
	}

	/**
	 * Action to select a site out of the available sites
	 *
	 * @return ResponseInterface
	 * @throws Exception
	 */
	public function indexAction(): ResponseInterface {
		$this->makeDocheader(); // Add docheader and buttons

		$out = [];
		$sites = $this->siteFinder->getAllSites();
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
		$queryBuilder->getRestrictions()->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class));
		$rows = $queryBuilder->select('pid')
			->from('tx_sgmail_domain_model_mail')->groupBy('pid')->executeQuery()->fetchAllAssociative();
		$siteIds = array_map(static function ($site) {
			return $site->getRootPageId();
		}, $sites);
		$rowIds = array_column($rows, 'pid');
		$pids = array_unique(array_merge($siteIds, $rowIds));
		foreach ($pids as $pid) {
			$pageInfo = BackendUtility::readPageAccess($pid, $GLOBALS['BE_USER']->getPagePermsClause(1));
			if ($pageInfo) {
				$rootline = BackendUtility::BEgetRootLine($pageInfo['uid'] ?? 0, '', TRUE);
				ksort($rootline);
				$path = '/root';
				foreach ($rootline as $page) {
					$path .= '/p' . dechex($page['uid']);
				}
				$pageInfo['path'] = $path;
				$out[] = $pageInfo;
			}
		}

		/** @var Request $originalRequest */
		$originalRequest = $this->request;
		if ($this->request->hasArgument('originalRequest')) {
			$originalRequest = $this->request->getArgument('originalRequest');
		}

		$controllerName = $originalRequest->getAttribute('extbase')->getControllerName();
		$actionName = $originalRequest->getAttribute('extbase')->getControllerActionName();

		$this->moduleTemplate->assignMultiple([
			'sites' => $out,
			'controller' => $controllerName,
			'action' => $actionName
		]);

		return $this->moduleTemplate->renderResponse('Index');
	}
}
