<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Doctrine\DBAL\Exception;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgMail\Domain\Repository\LayoutRepository;
use TYPO3\CMS\Backend\Clipboard\Clipboard;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\View\ViewInterface;
use function count;
use function is_array;

/**
 * Controller for the mail templating service module
 */
class LayoutController extends AbstractController {
	/**
	 * @var LayoutRepository
	 */
	protected LayoutRepository $layoutRepository;

	/**
	 * Inject the LayoutRepository
	 *
	 * @param LayoutRepository $layoutRepository
	 */
	public function injectLayoutRepository(LayoutRepository $layoutRepository): void {
		$this->layoutRepository = $layoutRepository;
	}

	/**
	 * Command array on the form [tablename][uid][command] = value.
	 * This array may get additional data set internally based on clipboard commands send in clipboardCommandArray var!
	 *
	 * @var array
	 */
	protected array $command;

	/**
	 * Clipboard command array. May trigger changes in "command"
	 *
	 * @var array
	 */
	protected array $clipboardCommandArray;

	/**
	 * @var Clipboard
	 */
	protected Clipboard $clipObj;

	/**
	 * Initializes the Action calls
	 *
	 * @return void
	 */
	public function initializeAction(): void {
		parent::initializeAction();
		$this->moduleTemplate->assign('controller', 'Layout');
		$queryParams = $this->request->getQueryParams();
		$this->command = $queryParams['cmd'] ?? [];
		$this->clipboardCommandArray = $queryParams['CB'] ?? [];
	}

	/**
	 * @return ResponseInterface
	 * @throws Exception
	 */
	public function indexAction(): ResponseInterface {
		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}

		$this->switchMode();

		$pageInfo = BackendUtility::readPageAccess(
			$this->site->getRootPageId(),
			$GLOBALS['BE_USER']->getPagePermsClause(1)
		);
		$layouts = $this->layoutRepository->findByPidForModule($this->site->getRootPageId());
		$this->moduleTemplate->assign('layouts', $layouts);
		$this->initClipboard();
		$pasteData = $this->clipObj->elFromTable('tx_sgmail_domain_model_layout');
		if (count($pasteData)) {
			$pasteButton = [
				'message' => $this->clipObj->confirmMsgText('pages', $pageInfo, 'into', $pasteData),
				'url' => $this->clipObj->pasteUrl('', $this->site->getRootPageId())
			];
			$this->moduleTemplate->assign('pasteButton', $pasteButton);
		}

		$this->moduleTemplate->assign('pageUid', $this->site->getRootPageId());
		$this->makeDocheader();

		return $this->moduleTemplate->renderResponse('Index');
	}

	/**
	 * Clipboard pasting and deleting.
	 *
	 * @return void
	 * @throws InvalidArgumentException
	 */
	protected function initClipboard(): void {
		if (!isset($this->clipObj)) {
			$this->clipObj = GeneralUtility::makeInstance(Clipboard::class);
		}

		$this->clipObj->initializeClipboard();
		if (isset($this->clipboardCommandArray['paste'])) {
			$this->clipObj->setCurrentPad($this->clipboardCommandArray['pad']);
			$this->command = $this->makePasteCmdArray(
				$this->clipboardCommandArray['paste'],
				$this->command,
				$this->clipboardCommandArray['update'] ?? NULL
			);
		}

		if (isset($this->clipboardCommandArray['delete'])) {
			$this->clipObj->setCurrentPad($this->clipboardCommandArray['pad']);
			$this->command = $this->makeDeleteCmdArray($this->command);
		}

		if (isset($this->clipboardCommandArray['el'])) {
			$this->clipboardCommandArray['setP'] = 'normal';
			$this->clipObj->setCmd($this->clipboardCommandArray);
			$this->clipObj->cleanCurrent();
			$this->clipObj->endClipboard();
		}
	}

	/**
	 * Backported removed makePasteCmdArray function for TYPO3 v11
	 * Only changed some calls from $this to $this->clipObj́
	 *
	 * @param string $ref
	 * @param array $CMD
	 * @param array|null $update
	 * @return array
	 */
	public function makePasteCmdArray($ref, $CMD, array $update = NULL) {
		[$pTable, $pUid] = explode('|', $ref);
		$pUid = (int) $pUid;
		// pUid must be set and if pTable is not set (that means paste ALL elements)
		// the uid MUST be positive/zero (pointing to page id)
		if ($pTable || $pUid >= 0) {
			$elements = $this->clipObj->elFromTable($pTable);
			// So the order is preserved.
			$elements = array_reverse($elements);
			$mode = $this->clipObj->currentMode() === 'copy' ? 'copy' : 'move';
			// Traverse elements and make CMD array
			foreach ($elements as $tP => $value) {
				[$table, $uid] = explode('|', $tP);
				if (!is_array($CMD[$table])) {
					$CMD[$table] = [];
				}
				if (is_array($update)) {
					$CMD[$table][$uid][$mode] = [
						'action' => 'paste',
						'target' => $pUid,
						'update' => $update,
					];
				} else {
					$CMD[$table][$uid][$mode] = $pUid;
				}
				if ($mode === 'move') {
					$this->clipObj->removeElement($tP);
				}
			}
			$this->clipObj->endClipboard();
		}
		return $CMD;
	}

	/**
	 * Backported removed makeDeleteCmdArray function for TYPO3 v11
	 * Only changed some calls from $this to $this->clipObj́
	 *
	 * @param array $CMD
	 * @return array
	 */
	public function makeDeleteCmdArray($CMD) {
		// all records
		$elements = $this->clipObj->elFromTable('');
		foreach ($elements as $tP => $value) {
			[$table, $uid] = explode('|', $tP);
			if (!is_array($CMD[$table])) {
				$CMD[$table] = [];
			}
			$CMD[$table][$uid]['delete'] = 1;
			$this->clipObj->removeElement($tP);
		}
		$this->clipObj->endClipboard();
		return $CMD;
	}
}
