<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use Doctrine\DBAL\Exception;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgMail\Domain\Model\Layout;
use SGalinski\SgMail\Domain\Model\Template;
use SGalinski\SgMail\Domain\Repository\FrontendUserGroupRepository;
use SGalinski\SgMail\Domain\Repository\LayoutRepository;
use SGalinski\SgMail\Domain\Repository\TemplateRepository;
use SGalinski\SgMail\Exceptions\TemplateNotFoundException;
use SGalinski\SgMail\Service\BackendService;
use SGalinski\SgMail\Service\MailTemplateService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Controller for the mail templating service module
 */
class NewsletterController extends AbstractController {
	/**
	 * @var TemplateRepository
	 */
	protected TemplateRepository $templateRepository;

	/**
	 * Inject the TemplateRepository
	 *
	 * @param TemplateRepository $templateRepository
	 */
	public function injectTemplateRepository(TemplateRepository $templateRepository): void {
		$this->templateRepository = $templateRepository;
	}

	/**
	 * @var FrontendUserGroupRepository
	 */
	protected FrontendUserGroupRepository $frontendUserGroupRepository;

	/**
	 * Inject the FrontendUserGroupRepository
	 *
	 * @param FrontendUserGroupRepository $frontendUserGroupRepository
	 */
	public function injectFrontendUserGroupRepository(FrontendUserGroupRepository $frontendUserGroupRepository): void {
		$this->frontendUserGroupRepository = $frontendUserGroupRepository;
	}

	/**
	 * @var LayoutRepository
	 */
	protected LayoutRepository $layoutRepository;

	/**
	 * Inject the LayoutRepository
	 *
	 * @param LayoutRepository $layoutRepository
	 */
	public function injectLayoutRepository(LayoutRepository $layoutRepository): void {
		$this->layoutRepository = $layoutRepository;
	}

	public function initializeAction(): void {
		parent::initializeAction();
		$this->moduleTemplate->assign('controller', 'Newsletter');
	}

	/**
	 * Show template Selection and enable content input + mail preview
	 *
	 * @param array $parameters
	 * @return ResponseInterface
	 * @throws InvalidQueryException
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 * @throws TemplateNotFoundException
	 * @throws Exception
	 * @throws \JsonException
	 */
	public function indexAction(array $parameters = []): ResponseInterface {
		if (empty($parameters)) {
			$parameters = $this->request->getArguments();
		}
		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}
		$this->switchMode();
		$this->addMessage();

		$registerArray = $this->registerService->getNonBlacklistedTemplates($this->site->getRootPageId());
		// if no extensions are registered, redirect to empty action
		if (!\is_array($registerArray) || \count($registerArray) <= 0) {
			return (new ForwardResponse('empty'))
				->withControllerName('Site')
				->withExtensionName('SgMail');
		}

		$arguments = $this->request->getArguments();
		$site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($this->site->getRootPageId());

		// if no template & extensionKey is selected look for them in the session
		if (
			!isset($parameters['selectedTemplate'])
			|| !isset($parameters['selectedExtension'])
		) {
			[$parameters['selectedExtension'], $parameters['selectedTemplate']] = $this->getSelectedExtensionAndTemplate(
			);
		}
		if (!isset($parameters['selectedLanguage'])) {
			if ($this->getFromSession('selectedLanguage') !== '') {
				$parameters['selectedLanguage'] = (int) $this->getFromSession('selectedLanguage');
			} else {
				$parameters['selectedLanguage'] = $site->getDefaultLanguage()->getLanguageId();
			}
		}

		// store selected template & extension key in the session
		$this->writeToSession('selectedTemplate', $parameters['selectedTemplate']);
		$this->writeToSession('selectedExtension', $parameters['selectedExtension']);
		$this->writeToSession('selectedLanguage', $parameters['selectedLanguage']);

		$selectedLanguage = $site->getLanguageById($parameters['selectedLanguage']);
		$selectedLanguageId = $selectedLanguage->getLanguageId();
		$languages = [$selectedLanguage];
		// this should only lead to one entry that corresponds to the selected language
		$templatesFromDb = $this->templateRepository->findByTemplateProperties(
			$parameters['selectedExtension'],
			$parameters['selectedTemplate'],
			$languages,
			$this->site->getRootPageId()
		);

		$templates = [];
		// this is inline with the MailController Listview, but since we will only ever have a single Language, there
		// should always just be a single Template
		foreach ($languages as $singleLanguage) {
			$template = NULL;
			foreach ($templatesFromDb as $_template) {
				/** @var Template $_template */
				if ($_template->getSiteLanguage() === $singleLanguage) {
					$template = $_template;
					break;
				}
			}
			// if no templates are in the db, get the default from the files
			if ($template === NULL) {
				$templateFromFile = $this->registerService->findTemplate(
					$parameters['selectedExtension'],
					$parameters['selectedTemplate'],
					$singleLanguage
				);
				$template = GeneralUtility::makeInstance(Template::class);
				$template->setSiteLanguage($singleLanguage);
				$templateFromFile['pid'] = $this->site->getRootPageId();
				$this->templateRepository->fillTemplate(
					$template,
					$templateFromFile
				);
			} else {
				$template->setIsOverwritten(TRUE);
			}

			$templates[$selectedLanguageId] = $template;
		}

		if (!empty($arguments['content'])) {
			$templates[$selectedLanguageId]->setContent($arguments['content']);
		}

		if (!empty($arguments['cc'])) {
			$templates[$selectedLanguageId]->setCc($arguments['cc']);
		}

		if (!empty($arguments['bcc'])) {
			$templates[$selectedLanguageId]->setBcc($arguments['bcc']);
		}

		if (!empty($arguments['fromName'])) {
			$templates[$selectedLanguageId]->setFromName($arguments['fromName']);
		}

		if (!empty($arguments['fromMail'])) {
			$templates[$selectedLanguageId]->setFromMail($arguments['fromMail']);
		}

		if (!empty($arguments['replyTo'])) {
			$templates[$selectedLanguageId]->setReplyTo($arguments['replyTo']);
		}

		if (!empty($arguments['layout'])) {
			$templates[$selectedLanguageId]->setLayout($arguments['layout']);
		}

		if (!empty($arguments['subject'])) {
			$templates[$selectedLanguageId]->setSubject($arguments['subject']);
		}

		// calculating optimal column width for the view
		$colspace = 4;
		$templateCount = \count($templates);
		if ($templateCount % 2 === 0 && $templateCount <= 4) {
			$colspace = 6;
		} elseif ($templateCount === 1) {
			$colspace = 12;
		}

		$this->moduleTemplate->assignMultiple([
			'colspace' => $colspace,
			'templates' => $templates,
			'register' => $registerArray,
			'isManual' => $this->registerService->isManuallyRegisteredTemplate($parameters['selectedTemplate'])
		]);
		$templateDescription = $registerArray[$parameters['selectedExtension']][$parameters['selectedTemplate']]['description'];
		if (\is_array($templateDescription)) {
			$templateDescription = $templateDescription[$site->getDefaultLanguage()->getLocale()->getLanguageCode()];
		} elseif (str_contains($templateDescription, 'LLL:')) {
			$templateDescription = LocalizationUtility::translate(
				$templateDescription,
				$parameters['selectedExtension']
			);
		}

		// create doc header component
		$layouts = $this->layoutRepository->findByPidForModule($this->site->getRootPageId());
		$layoutOptions = [
			Layout::DEFAULT_LAYOUT => LocalizationUtility::translate('backend.layout.default', 'SgMail'),
			Layout::NO_LAYOUT => LocalizationUtility::translate('backend.layout.none', 'SgMail')
		];
		foreach ($layouts as $layout) {
			$layoutOptions[(int) $layout['uid']] = $layout['name'];
		}

		$this->moduleTemplate->assignMultiple([
			'templateDescription' => $templateDescription,
			'selectedTemplate' => $registerArray[$parameters['selectedExtension']][$parameters['selectedTemplate']],
			'selectedTemplateKey' => $parameters['selectedTemplate'],
			'selectedExtensionKey' => $parameters['selectedExtension'],
			'layoutOptions' => $layoutOptions
		]);

		$this->makeDocheader();
		// get the default language label and pass it to the view
		$pageTsConfig = BackendUtility::getPagesTSconfig($this->site->getRootPageId());
		$defaultLanguageLabel = LocalizationUtility::translate(
			'LLL:EXT:core/Resources/Private/Language/locallang_mod_web_list.xlf:defaultLanguage'
		);
		if (isset($pageTsConfig['mod.']['SHARED.']['defaultLanguageLabel'])) {
			$defaultLanguageLabel = $pageTsConfig['mod.']['SHARED.']['defaultLanguageLabel'];
		}

		$arguments = $this->request->getArguments();
		if (!isset($arguments['selectedGroups']) || !is_array($arguments['selectedGroups'])) {
			$selectedGroups = [];
		} else {
			$selectedGroups = $arguments['selectedGroups'];
		}

		$selectedGroupsTree = $this->frontendUserGroupRepository->getFrontendUserGroupsTree($selectedGroups);
		$this->moduleTemplate->assignMultiple([
			'beUserMail' => (!empty($arguments['emailAddress']) ? $arguments['emailAddress'] : $GLOBALS['BE_USER']->user['email']),
			'defaultLanguageLabel' => $defaultLanguageLabel,
			'selectedLanguage' => $parameters['selectedLanguage'],
			'selectedGroups' => $selectedGroupsTree,
			'languages' => $this->site->getAllLanguages()
		]);
		return $this->moduleTemplate->renderResponse('Index');
	}

	/**
	 * send a test email to a given address redirect to index action
	 *
	 * Oh my... Also sends the real mails. Pretty stupid naming.
	 *
	 * @param array $parameters
	 * @return ResponseInterface
	 * @throws IllegalObjectTypeException
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 * @throws UnknownObjectException
	 */
	public function sendTestMailAction(array $parameters = []): ResponseInterface {
		$this->requireSite();
		$arguments = [];
		if ($parameters['selectedExtension']) {
			$arguments['selectedExtension'] = $parameters['selectedExtension'];
		}
		if ($parameters['selectedTemplate']) {
			$arguments['selectedTemplate'] = $parameters['selectedTemplate'];
		}

		foreach ((array) $parameters['templates'] as $parameter) {
			$addresses = array_merge(
				GeneralUtility::trimExplode(',', $parameter['bcc']),
				GeneralUtility::trimExplode(',', $parameter['cc'])
			);
			if (!$this->checkMailAddresses($addresses)) {
				return $this->redirect('index', NULL, NULL, $arguments);
			}
		}

		$mailTemplateService = GeneralUtility::makeInstance(
			MailTemplateService::class,
			$parameters['selectedTemplate'],
			$parameters['selectedExtension']
		);
		$mailIsSend = FALSE;
		$mailTemplateService->setTemplateName($parameters['selectedTemplate']);
		$mailTemplateService->setExtensionKey($parameters['selectedExtension']);
		$mailTemplateService->setFromName((string) ($parameters['fromName'] ?? ''));
		$mailTemplateService->setFromAddress((string) ($parameters['fromMail'] ?? ''));
		$mailTemplateService->setSubject((string) ($parameters['subject'] ?? ''));
		$mailTemplateService->setReplyToAddress((string) ($parameters['replyTo'] ?? ''));
		$mailTemplateService->setPid($this->getPid());

		if (
			!array_key_exists('sendRealEmails', $parameters)
			|| $parameters['sendRealEmails'] === 0
		) {
			// Send test emails
			$message = LocalizationUtility::translate('backend.success_mail', 'sg_mail');
			$markers = [
				'user' => [
					'username' => 'username@example.com',
					'name' => 'Max Mustermann',
					'title' => 'Dr.',
					'first_name' => 'Max',
					'last_name' => 'Mustermann',
					'address' => 'Berlin, Germany',
					'zip' => '89073',
					'city' => 'Ulm',
					'country' => 'Germany',
					'telephone' => '491535555555',
					'email' => 'max.mustermann@example.com',
					'company' => 'Muster GmbH',
					'www' => '	www.example.com',
				]
			];

			foreach ((array) $parameters['templates'] as $languageId => $template) {
				$mailTemplateService->setSiteLanguage($this->site->getLanguageById($languageId));
				$mailTemplateService->setToAddresses($parameters['emailAddress']);
				$mailTemplateService->setOverwrittenEmailBody($template['content']);
				$mailTemplateService->setSubject($template['subject']);
				$mailTemplateService->setFromName($template['fromName']);
				$mailTemplateService->setFromAddress($template['fromMail']);
				$mailTemplateService->setMarkers($markers);
				$mailIsSend = $mailTemplateService->sendEmail(TRUE, TRUE);
			}
		} else {
			$errorRecipients = [];
			$message = LocalizationUtility::translate('backend.success_mail_queue', 'sg_mail');
			if (is_array($parameters['selectedGroups'])) {
				$recipients = BackendService::getRecipientsByGroups($parameters['selectedGroups']);
				foreach ((array) $parameters['templates'] as $languageId => $template) {
					$mailTemplateService->setSiteLanguage($this->site->getLanguageById($languageId));
					$mailTemplateService->setOverwrittenEmailBody($template['content']);
					$mailTemplateService->setSubject($template['subject']);
					$mailTemplateService->setBccAddresses($template['bcc']);
					$mailTemplateService->setCcAddresses($template['cc']);

					foreach ($recipients as $recipient) {
						try {
							$mailTemplateService->setToAddresses($recipient['email']);
							$mailTemplateService->setMarkers(['user' => $recipient]);
							// no real error handling here, one must check the MailQueue
							$mailTemplateService->sendEmail(FALSE, TRUE);
						} catch (\Exception $e) {
							// Invalid email address could not be loaded to queue
							$errorRecipients[] = $recipient['uid'] . ' - '
								. $recipient['email'] . ' (' . $e->getMessage() . ')';
						}
					}

					$mailIsSend = TRUE;
				}
			}

			if (count($errorRecipients) > 0) {
				$errorMessage = LocalizationUtility::translate('backend.newsletter.failure_mail', 'sg_mail');
				foreach ($errorRecipients as $errorRecipient) {
					$errorMessage .= "$errorRecipient\n";
				}

				$this->addFlashMessage($errorMessage, '', ContextualFeedbackSeverity::ERROR);
			}
		}

		if ($mailIsSend) {
			$this->addFlashMessage($message);
		} else {
			$message = LocalizationUtility::translate('backend.failure_mail', 'sg_mail');
			$this->addFlashMessage($message, '', ContextualFeedbackSeverity::ERROR);
		}

		$arguments['selectedGroups'] = $parameters['selectedGroups'];
		$arguments = array_merge($arguments, $parameters);
		$arguments['emailAddress'] = $parameters['emailAddress'];

		return $this->redirect('index', NULL, NULL, $arguments);
	}

	/**
	 * show a notice when no extension is registered
	 */
	public function emptyAction(): ResponseInterface {
		// Nothing to do here, the fluid template will handle it
		return $this->moduleTemplate->renderResponse('Empty');
	}
}
