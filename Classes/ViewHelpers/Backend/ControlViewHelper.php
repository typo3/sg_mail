<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\ViewHelpers\Backend;

use InvalidArgumentException;
use TYPO3\CMS\Backend\RecordList\DatabaseRecordList;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Type\Bitmask\Permission;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use UnexpectedValueException;

/**
 * Class ControlViewHelper
 **/
class ControlViewHelper extends AbstractViewHelper {
	/**
	 * Initialize the ViewHelper arguments
	 */
	public function initializeArguments(): void {
		parent::initializeArguments();
		$this->registerArgument('table', 'string', 'The table to control', TRUE);
		$this->registerArgument('row', 'mixed', 'The row of the record', TRUE);
		$this->registerArgument('clipboard', 'bool', 'If true, renders the clipboard controls', FALSE);
	}

	/**
	 * Renders the control buttons for the specified record
	 *
	 * @return string
	 * @throws InvalidArgumentException
	 * @throws UnexpectedValueException
	 */
	public function render(): string {
		$table = $this->arguments['table'];
		$row = $this->arguments['row'];

		/** @var DatabaseRecordList $databaseRecordList */
		$databaseRecordList = GeneralUtility::makeInstance(DatabaseRecordList::class);
		$databaseRecordList->setRequest($this->renderingContext->getRequest());
		$pageInfo = BackendUtility::readPageAccess($row['pid'], $GLOBALS['BE_USER']->getPagePermsClause(1));
		$databaseRecordList->calcPerms = new Permission($GLOBALS['BE_USER']->calcPerms($pageInfo));
		return $databaseRecordList->makeControl($table, $row);
	}
}
