<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Command;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Exception;
use SGalinski\SgMail\Domain\Repository\MailRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DeleteOldMailsCommand
 *
 * @package SGalinski\SgMail\Command
 */
class DeleteOldMailsCommand extends Command {
	public const DEFAULT_LIMIT = 1000;
	private const TABLE_NAME_MAIL = 'tx_sgmail_domain_model_mail';
	private const TABLE_NAME_FILE_REFERENCE = 'sys_file_reference';
	private const TABLE_NAME_FILE = 'sys_file';

	/**
	 * array containing ids of sys_file records for deletion
	 *
	 * @var array
	 */
	protected array $sysFileUidsToDelete = [];

	/**
	 * array containing ids of sys_file_reference records for deletion
	 *
	 * @var array
	 */
	protected array $sysFileReferenceUidsToDelete = [];

	/**
	 * @var int
	 */
	private int $amountOfDeletedRecords = 0;

	/**
	 * Configure the command by defining the name, options and arguments
	 */
	protected function configure(): void {
		$this->setDescription(
			'Automatically deletes records from `tx_sgmail_domain_model_mail`, which are older than the specified age. Attachments sent as part of the email are deleted as well.'
		)->addArgument(
			'maxAge',
			InputArgument::REQUIRED,
			'The maximum age (in days), mails older than this will be deleted.'
		)->addArgument(
			'limit',
			InputArgument::OPTIONAL,
			'The maximum amount of old mails to delete during one task execution, if left blank a default value of ' . self::DEFAULT_LIMIT . ' is used.',
			self::DEFAULT_LIMIT
		);
	}

	/**
	 * Executes the command for the deletion of old mails
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int {
		$io = new SymfonyStyle($input, $output);
		$io->title($this->getDescription());

		$limit = (int) $input->getArgument('limit');
		if ($limit === 0) {
			$limit = self::DEFAULT_LIMIT;
		}

		$maxAgeInDays = (int) $input->getArgument('maxAge');
		if ($maxAgeInDays <= 0) {
			$io->error('Please enter a maximum age (in days) for the mail\'s to be deleted.');
			return self::FAILURE;
		}

		$mailRepository = GeneralUtility::makeInstance(MailRepository::class);

		$mailUidsForDeletion = $mailRepository->findMailsForDeletion($maxAgeInDays, $limit);
		if (!$mailUidsForDeletion) {
			$io->success("No matching mails found for deletion criteria: older than $maxAgeInDays days");
			return self::SUCCESS;
		}

		$this->gatherAttachmentsForDeletion($mailUidsForDeletion);
		$this->deleteAttachmentsAndRelatedFiles();
		$this->deleteOldMails($mailUidsForDeletion);

		if ($this->amountOfDeletedRecords > 0) {
			$io->success('Successfully deleted ' . $this->amountOfDeletedRecords . ' records.');
		} else {
			$io->note('No records deleted');
		}

		return self::SUCCESS;
	}

	/**
	 * Deletes all tx_sgmail_domain_model_mail records, where the uid matches one of the uids in $mailUidsForDeletion
	 *
	 * @param array $mailUidsForDeletion
	 */
	private function deleteOldMails(array $mailUidsForDeletion): void {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			self::TABLE_NAME_MAIL
		);
		$this->amountOfDeletedRecords += $queryBuilder
			->delete(self::TABLE_NAME_MAIL)->where(
				$queryBuilder->expr()->in(
					'uid',
					$queryBuilder->createNamedParameter($mailUidsForDeletion, ArrayParameterType::INTEGER)
				)
			)->executeStatement();
	}

	/**
	 * Deletes the attachments (sys_file_reference / sys_file records), referenced in the mails to be deleted.
	 * For every file-reference found in a mail record, we check if it is the only reference,
	 * or if we can find more records, referencing the same sys_file.
	 *
	 * The sys_file_reference found in the mail record is always deleted.
	 * If the referenced file has no more references, besides the one we just deleted, both the sys_file record and
	 * the actual file in the file system is deleted as well.
	 *
	 * @param array $mailUidsForDeletion
	 * @throws Exception
	 */
	private function gatherAttachmentsForDeletion(array $mailUidsForDeletion): void {
		$sysFileReferencesToDelete = $this->gatherRelatedFileReferences($mailUidsForDeletion);
		$this->gatherFilesForDeletion($sysFileReferencesToDelete);
	}

	/**
	 * Iterates the gathered sysFileUidsToDelete and deletes them via the resource factory
	 *
	 * @return void
	 */
	private function deleteAttachmentsAndRelatedFiles(): void {
		$resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
		$fileReferencesQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getConnectionForTable(self::TABLE_NAME_FILE_REFERENCE)->createQueryBuilder();

		// delete sys_file records & the corresponding files in the filesystem
		foreach ($this->sysFileUidsToDelete as $sysFileUid) {
			try {
				$file = $resourceFactory->getFileObject($sysFileUid);
				if ($file->exists()) {
					$file->delete();
					++$this->amountOfDeletedRecords;
				}
			} catch (FileDoesNotExistException $e) {
				continue;
			}
		}

		// delete the sys_file_reference records
		$this->amountOfDeletedRecords += $fileReferencesQueryBuilder
			->delete(self::TABLE_NAME_FILE_REFERENCE)->where(
				$fileReferencesQueryBuilder->expr()->in(
					'uid',
					$fileReferencesQueryBuilder->createNamedParameter(
						$this->sysFileReferenceUidsToDelete,
						ArrayParameterType::INTEGER
					)
				)
			)->executeStatement();
	}

	/**
	 * Fetches all sys_file_reference records, with a relation to the mails we want to delete.
	 *
	 * @param array $mailUidsForDeletion
	 * @return array
	 */
	private function gatherRelatedFileReferences(array $mailUidsForDeletion): array {
		$fileReferenceQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable(self::TABLE_NAME_FILE_REFERENCE);
		try {
			return $fileReferenceQueryBuilder
				->select('uid', 'uid_local')
				->from(self::TABLE_NAME_FILE_REFERENCE)->where(
					$fileReferenceQueryBuilder->expr()->in(
						'uid_foreign',
						$fileReferenceQueryBuilder->createNamedParameter(
							$mailUidsForDeletion,
							ArrayParameterType::INTEGER
						)
					),
					$fileReferenceQueryBuilder->expr()->eq(
						'tablenames',
						$fileReferenceQueryBuilder->createNamedParameter(self::TABLE_NAME_MAIL)
					),
					$fileReferenceQueryBuilder->expr()->eq(
						'fieldname',
						$fileReferenceQueryBuilder->createNamedParameter('attachments')
					)
				)->executeQuery()->fetchAllAssociative();
		} catch (Exception $e) {
			return [];
		}
	}

	/**
	 * @param array $sysFileReferencesToDelete
	 * @return void
	 * @throws Exception
	 */
	private function gatherFilesForDeletion(array $sysFileReferencesToDelete): void {
		$fileReferenceQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			self::TABLE_NAME_FILE_REFERENCE
		);

		// get `uid_local` section of the array
		$sysFileReferencesUidLocalColumn = array_column($sysFileReferencesToDelete, 'uid_local');

		// sys_file_reference.uid_local -> sys_file.uid
		// sys_file_reference.uid_foreign -> tx_sgmail_domain_model_mail.uid
		$sysFileReferenceUsageCount = $fileReferenceQueryBuilder
			->count('uid_local')
			->from(self::TABLE_NAME_FILE_REFERENCE)
			->where(
				$fileReferenceQueryBuilder->expr()->in(
					'uid_local',
					$fileReferenceQueryBuilder->createNamedParameter(
						$sysFileReferencesUidLocalColumn,
						ArrayParameterType::INTEGER
					)
				)
			)->executeQuery()->fetchOne();

		$sysFileReferencesToDeleteCount = count($sysFileReferencesToDelete);

		// check if the file references we want to delete have more usages, than the amount of file references we found.
		// if the count result is bigger than our $sysFileReferencesToDeleteCount,
		// we know the referenced files are still used elsewhere, thus we cannot delete them & only delete the
		// mail attachment file references.
		if ($sysFileReferenceUsageCount === $sysFileReferencesToDeleteCount) {
			// same count, so we can safely delete the related files as well
			array_push($this->sysFileUidsToDelete, ...$sysFileReferencesUidLocalColumn);
		}

		$sysFileReferencesUidColumn = array_column($sysFileReferencesToDelete, 'uid');
		array_push($this->sysFileReferenceUidsToDelete, ...$sysFileReferencesUidColumn);
	}
}
