<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Command;

use SGalinski\SgMail\Domain\Repository\MailRepository;
use SGalinski\SgMail\Service\MailTemplateService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;

/**
 * Command controller for the mailing feature
 */
class SendMailCommandController extends Command {
	/**
	 * Configure command
	 */
	public function configure(): void {
		$this->setDescription('Executes the sending of mails with a maximum of the given sentCount.')
			->addArgument('sendCount', InputArgument::OPTIONAL, 'Send count', 50);
	}

	/**
	 * @return MailRepository
	 */
	protected function getMailRepository(): MailRepository {
		return GeneralUtility::makeInstance(MailRepository::class);
	}

	/**
	 * Execute the command
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function execute(InputInterface $input, OutputInterface $output): int {
		$sendCount = $input->getArgument('sendCount');

		$mailsToSend = $this->getMailRepository()->findMailsToSend($sendCount)->toArray();
		$mailService = GeneralUtility::makeInstance(MailTemplateService::class);
		$mailService->sendMailsFromQueue($mailsToSend);
		return self::SUCCESS;
	}
}
