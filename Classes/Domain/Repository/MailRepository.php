<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Repository;

use SGalinski\SgMail\Command\DeleteOldMailsCommand;
use SGalinski\SgMail\Service\BackendService;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Query;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * Repository for the Mail object
 */
class MailRepository extends AbstractRepository {
	public const SENT = '1';
	public const NOT_SENT = '2';

	/**
	 * Returns all mails that are still not sent ordered by priority.
	 *
	 * @param int|null $limit
	 * @return QueryResultInterface
	 */
	public function findMailsToSend(int $limit = NULL): QueryResultInterface {
		$constraintsAnd = [];
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		if ($limit) {
			$query->setLimit($limit);
		}

		$query->setOrderings(['priority' => Query::ORDER_DESCENDING]);

		// ignore blacklisted templates
		$constraintsAnd[] = $query->equals('blacklisted', FALSE);
		$constraintsAnd[] = $query->equals('sending_time', 0);
		return $query->matching($query->logicalAnd(...$constraintsAnd))->execute();
	}

	/**
	 * Find all mails (sent & unsent) for extension key and template name
	 * We use Doctrine for better performance
	 *
	 * @param int $pid
	 * @param int|null $limit
	 * @param array $filters
	 * @return QueryBuilder
	 */
	public function findAllEntries(int $pid = 0, int $limit = NULL, array $filters = []): QueryBuilder {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
		$queryBuilder->select('*')
			->from('tx_sgmail_domain_model_mail');
		if ($limit) {
			$queryBuilder->setMaxResults($limit);
		}

		if (isset($filters['filterExtension']) && $filters['filterExtension'] !== 0
			&& $filters['filterExtension'] !== '0' && $filters['filterExtension'] !== ''
		) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->eq(
					'extension_key',
					$queryBuilder->createNamedParameter($filters['filterExtension'])
				)
			);
		}

		if (isset($filters['filterTemplate']) && $filters['filterTemplate'] !== 0
			&& $filters['filterTemplate'] !== '0' && $filters['filterTemplate'] !== ''
		) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->eq(
					'template_name',
					$queryBuilder->createNamedParameter($filters['filterTemplate'])
				)
			);
		}

		if (isset($filters['filterLanguage']) && $filters['filterLanguage'] !== '') {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($filters['filterLanguage'])
				)
			);
		}

		$constraintsOr = [];
		if (isset($filters['filterSearch'])) {
			$search = $queryBuilder->createNamedParameter('%' . $filters['filterSearch'] . '%');
			if ($filters['filterFields'] && trim('' !== $filters['filterFields'])) {
				foreach ($filters['filterFields'] as $field) {
					switch ($field) {
						case BackendService::SENDER_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('from_address', $search);
							break;
						case BackendService::RECIPIENT_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('to_address', $search);
							break;
						case BackendService::SUBJECT_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('mail_subject', $search);
							break;
						case BackendService::MAILTEXT_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('mail_body', $search);
							break;
						case BackendService::CC_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('cc_addresses', $search);
							break;
						case BackendService::BCC_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('bcc_addresses', $search);
							break;
						case BackendService::FROM_NAME_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('from_name', $search);
							break;
						case BackendService::REPLY_TO_NAME_FILTER_OPTION:
							$constraintsOr[] = $queryBuilder->expr()->like('reply_to', $search);
							break;
					}
				}
			} else { // if nothing selected, search in all fields
				$constraintsOr[] = $queryBuilder->expr()->like('from_address', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('to_address', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('mail_subject', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('mail_body', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('cc_addresses', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('bcc_addresses', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('from_name', $search);
				$constraintsOr[] = $queryBuilder->expr()->like('reply_to', $search);
			}
		}
		if (count($constraintsOr)) {
			$queryBuilder->andWhere($queryBuilder->expr()->or(...$constraintsOr));
		}

		$fromTime = strtotime($filters['filterFromDate'] ?? '');
		if ($fromTime) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->gte('last_sending_time', $queryBuilder->createNamedParameter($fromTime))
			);
		}

		$toTime = strtotime($filters['filterToDate'] ?? '');
		if ($toTime) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->lte('last_sending_time', $queryBuilder->createNamedParameter($toTime))
			);
		}

		if (isset($filters['filterBlacklist']) && (int) $filters['filterBlacklist'] !== 1) {
			$queryBuilder->andWhere($queryBuilder->expr()->eq('blacklisted', 0));
		}

		if (isset($filters['filterSent'])) {
			switch ($filters['filterSent']) {
				case self::SENT:
					$queryBuilder->andWhere($queryBuilder->expr()->gt('last_sending_time', 0));
					break;
				case self::NOT_SENT:
					$queryBuilder->andWhere($queryBuilder->expr()->eq('last_sending_time', 0));
					break;
			}
		}

		$queryBuilder->andWhere($queryBuilder->expr()->in('pid', [$pid, 0]));
		return $queryBuilder->orderBy('tstamp', 'desc');
	}

	/**
	 * Returns an array with uids of all mails that are older than $maxAgeInDays
	 *
	 * @param int $maxAgeInDays
	 * @param int $limit
	 * @return array
	 */
	public function findMailsForDeletion(
		int $maxAgeInDays = 0,
		int $limit = DeleteOldMailsCommand::DEFAULT_LIMIT
	): array {
		/** @var Query $query */
		$query = $this->createQuery();
		$query->statement(
			'SELECT uid FROM tx_sgmail_domain_model_mail WHERE FROM_UNIXTIME(crdate) < now() - interval ? day',
			[$maxAgeInDays]
		)->setLimit($limit);

		// "flatten" the array, so that we end up with one numerical array with just the uids
		$mailUidsForDeletion = $query->execute(TRUE);
		foreach ($mailUidsForDeletion as $key => $value) {
			if (is_array($value) && array_key_exists('uid', $value)) {
				$mailUidsForDeletion[$key] = $value['uid'];
			}
		}

		return $mailUidsForDeletion;
	}
}
