<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Repository;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Frontend User Group Repository
 */
class FrontendUserGroupRepository extends Repository {
	protected ConnectionPool $connectionPool;

	/**
	 * FrontendUserGroupRepository constructor.
	 *
	 */
	public function __construct(ConnectionPool $connectionPool) {
		parent::__construct();
		$this->connectionPool = $connectionPool;
		$querySettings = GeneralUtility::makeInstance(Typo3QuerySettings::class);
		$querySettings->setRespectStoragePage(FALSE);
		$this->setDefaultQuerySettings($querySettings);
	}

	/**
	 * Build a tree of the fe_groups
	 *
	 * @param array $selectedGroups
	 * @return array
	 * @throws Exception
	 */
	public function getFrontendUserGroupsTree(array $selectedGroups): array {
		$tree = [];
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->getTableName());

		/**
		 * Get root nodes first
		 */
		$rootNodes = $queryBuilder->select('uid', 'title', $this->getChildrenField())
			->from($this->getTableName())
			->where(
				'NOT find_in_set(uid, (SELECT GROUP_CONCAT(' . $this->getChildrenField() . ') FROM ' . $this->getTableName() . '))'
			)->executeQuery();
		$rootNodes = $rootNodes->fetchAllAssociative();

		foreach ($rootNodes as $rootNode) {
			if (!empty($rootNode['subgroup'])) {
				$rootNode['inc'] = $this->getChildrenTreeOfGroup($rootNode, $selectedGroups);
			}

			if (in_array($rootNode['uid'], $selectedGroups, TRUE)) {
				$rootNode['selected'] = TRUE;
			}

			unset($rootNode['subgroup']);
			$tree[] = $rootNode;
		}

		return $tree;
	}

	/**
	 * Get the children tree of the group
	 *
	 * @param array $node
	 * @param array $selectedGroups
	 * @return array
	 * @throws Exception
	 */
	protected function getChildrenTreeOfGroup(array $node, array $selectedGroups): array {
		$children = [];
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->getTableName());
		$groupIds = GeneralUtility::intExplode(',', $node['subgroup']);

		foreach ($groupIds as $groupId) {
			$group = $queryBuilder->select('uid', 'title', $this->getChildrenField())
				->from($this->getTableName())
				->where($queryBuilder->expr()->eq('uid', $groupId))->executeQuery()->fetchAssociative();
			if (!empty($group['subgroup'])) {
				$group['inc'] = $this->getChildrenTreeOfGroup($group, $selectedGroups);
			}

			if (in_array($group['uid'], $selectedGroups, TRUE)) {
				$group['selected'] = TRUE;
			}

			$children[] = $group;
		}

		return $children;
	}

	/**
	 * Get the children of the group
	 *
	 * @param array $node
	 * @param array $selectedGroups
	 * @return array
	 * @throws Exception
	 */
	protected function getChildrenOfGroup(array $node, array &$selectedGroups): array {
		$children = [];
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->getTableName());
		$groupIds = GeneralUtility::intExplode(',', $node['subgroup']);

		foreach ($groupIds as $groupId) {
			$group = $queryBuilder->select('uid', 'title', $this->getChildrenField())
				->from($this->getTableName())
				->where($queryBuilder->expr()->eq('uid', $groupId))->executeQuery()->fetchAssociative();
			if (!empty($group['subgroup'])) {
				$this->getChildrenOfGroup($group, $selectedGroups);
			}

			if (!in_array($group['uid'], $selectedGroups, TRUE)) {
				$selectedGroups[] = $group['uid'];
			}

			$children[] = $group;
		}

		return $children;
	}

	/**
	 * Returns the table name
	 * @return string
	 */
	protected function getTableName(): string {
		return 'fe_groups';
	}

	/**
	 * Returns the field name where the child nodes are stored
	 * @return string
	 */
	protected function getChildrenField(): string {
		return 'subgroup';
	}

	/**
	 * Returns an array with all the subgroups of these groups
	 *
	 * @param array $groupIds
	 * @return array
	 * @throws Exception
	 */
	public function getFullGroupIdsWithChildren(array $groupIds): array {
		$selectedGroups = $groupIds;
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable($this->getTableName());

		/**
		 * Get root nodes first
		 */
		$rootNodes = $queryBuilder->select('uid', 'subgroup')
			->from($this->getTableName())
			->where($queryBuilder->expr()->in('uid', implode(',', $groupIds)))->executeQuery();
		$rootNodes = $rootNodes->fetchAllAssociative();

		foreach ($rootNodes as $rootNode) {
			if (!empty($rootNode['subgroup'])) {
				$this->getChildrenOfGroup($rootNode, $selectedGroups);
			}
		}

		return $selectedGroups;
	}
}
