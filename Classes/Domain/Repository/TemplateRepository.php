<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Repository;

use SGalinski\SgMail\Domain\Model\Template;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * Repository for the Template object
 */
class TemplateRepository extends AbstractRepository {
	/**
	 * @var array
	 */
	protected $defaultOrderings = ['tstamp' => QueryInterface::ORDER_DESCENDING];

	/**
	 * Find a list of templates by their properties
	 *
	 * @param string $extensionKey
	 * @param string $templateName
	 * @param SiteLanguage[] $languages
	 * @param int $pid
	 * @return QueryResultInterface
	 * @throws InvalidQueryException
	 */
	public function findByTemplateProperties(
		string $extensionKey,
		string $templateName,
		array $languages,
		int $pid
	): QueryResultInterface {
		$constraintsAnd = [];
		$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setRespectStoragePage(TRUE);
		$querySettings->setStoragePageIds([$pid]);
		$querySettings->setRespectSysLanguage(FALSE);
		$constraintsAnd[] = $query->equals('extension_key', $extensionKey);
		$constraintsAnd[] = $query->equals('template_name', $templateName);
		$languageUids = [];
		foreach ($languages as $language) {
			if ($language instanceof SiteLanguage) {
				$languageUids[] = $language->getLanguageId();
			}
		}

		$constraintsAnd[] = $query->in('sys_language_uid', $languageUids);
		return $query->matching(
			$query->logicalAnd(...$constraintsAnd)
		)->execute();
	}

	/**
	 * Delete a Template
	 *
	 * @param string $extensionKey
	 * @param string $templateName
	 * @param int $pid
	 */
	public function deleteTemplate(string $extensionKey, string $templateName, int $pid): void {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_sgmail_domain_model_template'
		);
		$queryBuilder->delete('tx_sgmail_domain_model_template')
			->where(
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->eq('extension_key', $queryBuilder->createNamedParameter($extensionKey)),
					$queryBuilder->expr()->eq('template_name', $queryBuilder->createNamedParameter($templateName)),
					$queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pid, Connection::PARAM_INT))
				)
			)->executeStatement();
	}

	/**
	 * Update Template by its uid
	 *
	 * @param int $uid
	 * @param array $templateData
	 * @return Template
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function updateByUid(int $uid, array $templateData): Template {
		/** @var Template $template */
		$template = $this->findByUid($uid);
		$this->fillTemplate($template, $templateData);

		$this->update($template);
		$this->persist();
		return $template;
	}

	/**
	 * Create and save a new Template
	 *
	 * @param array $templateData
	 * @return Template
	 * @throws IllegalObjectTypeException
	 */
	public function create(array $templateData): Template {
		$template = GeneralUtility::makeInstance(Template::class);
		$this->fillTemplate($template, $templateData);
		$this->add($template);
		$this->persist();
		return $template;
	}

	/**
	 * Create and save a new Template
	 *
	 * @param array $templateData
	 * @return Template
	 * @throws IllegalObjectTypeException
	 */
	public function createWithoutPersist(array $templateData): Template {
		$template = GeneralUtility::makeInstance(Template::class);
		$this->fillTemplate($template, $templateData);
		return $template;
	}

	/**
	 * Fill the given Template with the provided data array
	 *
	 * @param Template $template
	 * @param array $templateData
	 */
	public function fillTemplate(Template $template, array $templateData): void {
		if (isset($templateData['extensionKey'])) {
			$template->setExtensionKey($templateData['extensionKey']);
		} else {
			$template->setExtensionKey($templateData['extension']);
		}

		$template->setTemplateName($templateData['templateName']);
		$template->setLanguageUid($templateData['sys_language_uid']);
		$template->setPid($templateData['pid']);
		$template->setContent((string) $templateData['content']);
		$template->setSubject($templateData['subject']);
		$template->setRenderWithNl2br((bool) $templateData['render_with_nl2br']);

		if (isset($templateData['layout'])) {
			$template->setLayout($templateData['layout']);
		}

		if (isset($templateData['fromName'])) {
			$template->setFromName($templateData['fromName']);
		}

		if (isset($templateData['fromMail'])) {
			$template->setFromMail($templateData['fromMail']);
		}

		if (isset($templateData['cc'])) {
			$template->setCc($templateData['cc']);
		}

		if (isset($templateData['bcc'])) {
			$template->setBcc($templateData['bcc']);
		}

		if (isset($templateData['replyTo'])) {
			$template->setReplyTo($templateData['replyTo']);
		}

		if (isset($templateData['toAddress'])) {
			$template->setToAddress($templateData['toAddress']);
		}

		if (isset($templateData['defaultAttachments'])) {
			$template->setDefaultAttachments($templateData['defaultAttachments']);
		}
	}
}
