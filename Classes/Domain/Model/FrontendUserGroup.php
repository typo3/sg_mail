<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class FrontendUserGroup extends AbstractEntity {
	/**
	 * @var string
	 */
	protected string $title = '';

	/**
	 * @var string
	 */
	protected string $description = '';

	/**
	 * @var ObjectStorage<FrontendUserGroup>
	 */
	protected ObjectStorage $subgroup;

	/**
	 * Constructs a new Frontend User Group
	 *
	 * @param string $title
	 */
	public function __construct(string $title = '') {
		$this->setTitle($title);
		$this->subgroup = new ObjectStorage();
	}

	/**
	 * Sets the title value
	 *
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * Returns the title value
	 *
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * Sets the description value
	 *
	 * @param string $description
	 */
	public function setDescription(string $description): void {
		$this->description = $description;
	}

	/**
	 * Returns the description value
	 *
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * Sets the subgroups. Keep in mind that the property is called "subgroup"
	 * although it can hold several subgroups.
	 *
	 * @param ObjectStorage<FrontendUserGroup> $subgroup An object storage containing the subgroups to add
	 */
	public function setSubgroup(ObjectStorage $subgroup): void {
		$this->subgroup = $subgroup;
	}

	/**
	 * Adds a subgroup to the frontend user
	 *
	 * @param FrontendUserGroup $subgroup
	 */
	public function addSubgroup(FrontendUserGroup $subgroup): void {
		$this->subgroup->attach($subgroup);
	}

	/**
	 * Removes a subgroup from the frontend user group
	 *
	 * @param FrontendUserGroup $subgroup
	 */
	public function removeSubgroup(FrontendUserGroup $subgroup): void {
		$this->subgroup->detach($subgroup);
	}

	/**
	 * Returns the subgroups. Keep in mind that the property is called "subgroup"
	 * although it can hold several subgroups.
	 *
	 * @return ObjectStorage<FrontendUserGroup> An object storage containing the subgroups
	 */
	public function getSubgroup(): ObjectStorage {
		return $this->subgroup;
	}
}
