<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Model;

use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Template domain model
 */
class Template extends AbstractEntity {
	/**
	 * @var string
	 */
	protected $extensionKey = '';

	/**
	 * @var int
	 */
	protected $layout = 0;

	/**
	 * @var string
	 */
	protected $subject = '';

	/**
	 * @var string
	 */
	protected $templateName = '';

	/**
	 * @var string
	 */
	protected $content = '';

	/**
	 * @var string
	 */
	protected $fromName = '';

	/**
	 * @var string
	 */
	protected $fromMail = '';

	/**
	 * @var string
	 */
	protected $cc = '';

	/**
	 * @var string
	 */
	protected $bcc = '';

	/**
	 * @var string
	 */
	protected $replyTo = '';

	/**
	 * @var string
	 */
	protected $toAddress = '';

	/**
	 * @var boolean
	 */
	protected $isOverwritten = FALSE;

	/**
	 * @var SiteLanguage
	 */
	protected $siteLanguage;

	/**
	 * @var string
	 */
	protected $defaultAttachments = '';

	protected bool $renderWithNl2br = TRUE;

	/**
	 * @return string
	 */
	public function getExtensionKey(): string {
		return $this->extensionKey;
	}

	/**
	 * @param string $extensionKey
	 */
	public function setExtensionKey(string $extensionKey): void {
		$this->extensionKey = $extensionKey;
	}

	/**
	 * @return string
	 */
	public function getSubject(): string {
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject(string $subject): void {
		$this->subject = $subject;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string {
		return $this->templateName;
	}

	/**
	 * @param string $templateName
	 */
	public function setTemplateName(string $templateName): void {
		$this->templateName = $templateName;
	}

	/**
	 * @return int
	 */
	public function getLanguageUid(): int {
		return $this->_languageUid;
	}

	/**
	 * @param int $languageUid
	 */
	public function setLanguageUid(int $languageUid): void {
		$this->_languageUid = $languageUid;
	}

	/**
	 * DEAD CODE?
	 * See the getSiteLanguage method of the MailTemplateService for a better approach.
	 *
	 * @return SiteLanguage|null
	 */
	public function getSiteLanguage(): ?SiteLanguage {
		if ($this->siteLanguage === NULL) {
			if ($this->pid === 0 && isset($GLOBALS['TSFE'])) {
				$pid = $GLOBALS['TSFE']->id;
			} else {
				$pid = $this->pid;
			}

			try {
				$this->siteLanguage = GeneralUtility::makeInstance(SiteFinder::class)
					?->getSiteByPageId($pid)->getLanguageById($this->_languageUid);
			} catch (\Exception $exception) {
				return NULL;
			}
		}

		return $this->siteLanguage;
	}

	/**
	 * @param SiteLanguage $language
	 */
	public function setSiteLanguage(SiteLanguage $language): void {
		$this->siteLanguage = $language;
		$this->_languageUid = $language->getLanguageId();
	}

	/**
	 * @return string
	 */
	public function getContent(): string {
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content): void {
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getFromName(): string {
		return $this->fromName;
	}

	/**
	 * @param string $fromName
	 */
	public function setFromName(string $fromName): void {
		$this->fromName = $fromName;
	}

	/**
	 * @return string
	 */
	public function getFromMail(): string {
		return $this->fromMail;
	}

	/**
	 * @param string $fromMail
	 */
	public function setFromMail(string $fromMail): void {
		$this->fromMail = trim($fromMail);
	}

	/**
	 * @return string
	 */
	public function getCc(): string {
		return $this->cc;
	}

	/**
	 * @param string $cc
	 */
	public function setCc(string $cc): void {
		$this->cc = trim($cc);
	}

	/**
	 * @return string
	 */
	public function getBcc(): string {
		return $this->bcc;
	}

	/**
	 * @param string $bcc
	 */
	public function setBcc(string $bcc): void {
		$this->bcc = trim($bcc);
	}

	/**
	 * @return string
	 */
	public function getReplyTo(): string {
		return $this->replyTo;
	}

	/**
	 * @param string $replyTo
	 */
	public function setReplyTo(string $replyTo): void {
		$this->replyTo = trim($replyTo);
	}

	/**
	 * @return bool
	 */
	public function getIsOverwritten(): bool {
		return $this->isOverwritten;
	}

	/**
	 * @param bool $isOverwritten
	 */
	public function setIsOverwritten(bool $isOverwritten): void {
		$this->isOverwritten = $isOverwritten;
	}

	/**
	 * @return string
	 */
	public function getToAddress(): string {
		return $this->toAddress;
	}

	/**
	 * @param string $toAddress
	 */
	public function setToAddress(string $toAddress): void {
		$this->toAddress = $toAddress;
	}

	/**
	 * @return int
	 */
	public function getLayout(): int {
		return $this->layout;
	}

	/**
	 * @param int $layout
	 */
	public function setLayout(int $layout): void {
		$this->layout = $layout;
	}

	/**
	 * @return string
	 */
	public function getDefaultAttachments(): string {
		return $this->defaultAttachments;
	}

	/**
	 * @param string $defaultAttachments
	 */
	public function setDefaultAttachments(string $defaultAttachments): void {
		$this->defaultAttachments = $defaultAttachments;
	}

	/**
	 * @return bool
	 */
	public function getRenderWithNl2br(): bool {
		return $this->renderWithNl2br;
	}

	/**
	 * @param mixed $renderWithNl2br
	 * @return void
	 */
	public function setRenderWithNl2br($renderWithNl2br) {
		$this->renderWithNl2br = (bool) $renderWithNl2br;
	}
}
