<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Model;

use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Mail domain model
 */
class Mail extends AbstractEntity {
	// Just some default values for better API calls.
	public const PRIORITY_LOWEST = 0;
	public const PRIORITY_LOW = 50;
	public const PRIORITY_MEDIUM = 100;
	public const PRIORITY_HIGH = 150;
	public const PRIORITY_HIGHEST = 200;
	public const STATUS_PENDING = 'pending';
	public const STATUS_SENT = 'sent';
	public const STATUS_ERROR = 'error';

	/**
	 * @var string
	 */
	protected $mailSubject = '';

	/**
	 * @var string
	 */
	protected $mailBody = '';

	/**
	 * @var string
	 */
	protected $toAddress = '';

	/**
	 * @var string
	 */
	protected $fromAddress = '';

	/**
	 * @var int
	 */
	protected $priority = 0;

	/**
	 * @var string
	 */
	protected $bccAddresses = '';

	/**
	 * @var string
	 */
	protected $ccAddresses = '';

	/**
	 * @var string
	 */
	protected $fromName = '';

	/**
	 * @var string
	 */
	protected $extensionKey = '';

	/**
	 * @var string
	 */
	protected $templateName = '';

	/**
	 * @var string
	 */
	protected $replyTo = '';

	/**
	 * @var int
	 */
	protected $sendingTime = 0;

	/**
	 * @var int
	 */
	protected $lastSendingTime = 0;

	/**
	 * @var boolean
	 */
	protected $blacklisted = FALSE;

	/**
  * @var ObjectStorage<FileReference>
  */
	protected $attachments;

	/**
	 * @var string
	 */
	protected $attachmentPaths = '';

	/**
	 * @var string
	 */
	protected $status = self::STATUS_PENDING;

	/**
	 * @var string
	 */
	protected $errorMessage = '';

	/**
	 * @var SiteLanguage
	 */
	protected $siteLanguage;

	/**
	 * Mail constructor.
	 */
	public function __construct() {
		$this->initializeObject();
	}

	/**
	 * Initialize the object
	 */
	public function initializeObject(): void {
		$this->attachments = new ObjectStorage();
	}

	/**
	 * @return string
	 */
	public function getMailSubject(): string {
		return $this->mailSubject;
	}

	/**
	 * @param string $mailSubject
	 * @return void
	 */
	public function setMailSubject(string $mailSubject): void {
		$this->mailSubject = $mailSubject;
	}

	/**
	 * @return string
	 */
	public function getMailBody(): string {
		return $this->mailBody;
	}

	/**
	 * @param string $mailBody
	 * @return void
	 */
	public function setMailBody(string $mailBody): void {
		$this->mailBody = $mailBody;
	}

	/**
	 * @return string
	 */
	public function getToAddress(): string {
		return $this->toAddress;
	}

	/**
	 * @param string $toAddress
	 * @return void
	 */
	public function setToAddress(string $toAddress): void {
		$this->toAddress = $toAddress;
	}

	/**
	 * @return string
	 */
	public function getFromAddress(): string {
		return $this->fromAddress;
	}

	/**
	 * @param string $fromAddress
	 * @return void
	 */
	public function setFromAddress(string $fromAddress): void {
		$this->fromAddress = $fromAddress;
	}

	/**
	 * @return int
	 */
	public function getPriority(): int {
		return $this->priority;
	}

	/**
	 * @param int $priority
	 * @return void
	 */
	public function setPriority(int $priority): void {
		$this->priority = $priority;
	}

	/**
	 * @return string
	 */
	public function getBccAddresses(): string {
		return $this->bccAddresses;
	}

	/**
	 * @param string $bccAddresses
	 */
	public function setBccAddresses(string $bccAddresses): void {
		$this->bccAddresses = $bccAddresses;
	}

	/**
	 * @return string
	 */
	public function getCcAddresses(): string {
		return $this->ccAddresses;
	}

	/**
	 * @param string $ccAddresses
	 */
	public function setCcAddresses(string $ccAddresses): void {
		$this->ccAddresses = $ccAddresses;
	}

	/**
	 * @return string
	 */
	public function getFromName(): string {
		return $this->fromName;
	}

	/**
	 * @param string $fromName
	 */
	public function setFromName(string $fromName): void {
		$this->fromName = $fromName;
	}

	/**
	 * @return string
	 */
	public function getExtensionKey(): string {
		return $this->extensionKey;
	}

	/**
	 * @param string $extensionKey
	 */
	public function setExtensionKey(string $extensionKey): void {
		$this->extensionKey = $extensionKey;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string {
		return $this->templateName;
	}

	/**
	 * @param string $templateName
	 */
	public function setTemplateName(string $templateName): void {
		$this->templateName = $templateName;
	}

	/**
	 * @return string
	 */
	public function getReplyTo(): string {
		return $this->replyTo;
	}

	/**
	 * @param string $replyTo
	 */
	public function setReplyTo(string $replyTo): void {
		$this->replyTo = $replyTo;
	}

	/**
	 * @return int
	 */
	public function getSendingTime(): int {
		return $this->sendingTime;
	}

	/**
	 * @param int $sendingTime
	 */
	public function setSendingTime(int $sendingTime): void {
		$this->sendingTime = $sendingTime;
	}

	/**
	 * @return int
	 */
	public function getLastSendingTime(): int {
		return $this->lastSendingTime;
	}

	/**
	 * @param int $sendingTime
	 */
	public function setLastSendingTime(int $sendingTime): void {
		$this->lastSendingTime = $sendingTime;
	}


	/**
	 * @return SiteLanguage
	 * @throws SiteNotFoundException
	 */
	public function getSiteLanguage(): SiteLanguage {
		if ($this->siteLanguage === NULL) {
			$this->siteLanguage = GeneralUtility::makeInstance(SiteFinder::class)
				->getSiteByPageId($this->pid)
				->getLanguageById($this->_languageUid);
		}

		return $this->siteLanguage;
	}

	/**
	 * @param SiteLanguage $language
	 */
	public function setSiteLanguage(SiteLanguage $language): void {
		$this->siteLanguage = $language;
		$this->_languageUid = $language->getLanguageId();
	}

	/**
	 * @return bool
	 */
	public function getBlacklisted(): bool {
		return $this->blacklisted;
	}

	/**
	 * @param bool $blacklisted
	 */
	public function setBlacklisted(bool $blacklisted): void {
		$this->blacklisted = $blacklisted;
	}

	/**
	 * @return ObjectStorage
	 */
	public function getAttachments(): ObjectStorage {
		return $this->attachments;
	}

	/**
	 * @param ObjectStorage $attachments
	 */
	public function setAttachments(ObjectStorage $attachments): void {
		$this->attachments = $attachments;
	}

	/**
	 * Add an attachment
	 *
	 * @param FileReference $attachment
	 */
	public function addAttachment(FileReference $attachment): void {
		$this->attachments->attach($attachment);
	}

	/**
	 * @return string
	 */
	public function getAttachmentPaths(): string {
		return $this->attachmentPaths;
	}

	/**
	 * @param string $attachmentPaths
	 */
	public function setAttachmentPaths(string $attachmentPaths): void {
		$this->attachmentPaths = $attachmentPaths;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void {
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getErrorMessage(): string {
		return $this->errorMessage;
	}

	/**
	 * @param string $errorMessage
	 */
	public function setErrorMessage(string $errorMessage): void {
		$this->errorMessage = $errorMessage;
	}
}
