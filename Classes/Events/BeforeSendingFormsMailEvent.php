<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Events;

use SGalinski\SgMail\Service\MailTemplateService;
use TYPO3\CMS\Form\Domain\Finishers\FinisherContext;

/**
 * This event is triggered right before a mail is sent in the forms finisher
 * It contains the mailTemplateService to modify the mail template
 * and the finisherContext to receive information about the submitted form
 */
final class BeforeSendingFormsMailEvent {
	/**
	 * @var MailTemplateService
	 */
	private $mailTemplateService;

	/**
	 * @var FinisherContext
	 */
	private $finisherContext;

	/**
	 * @param MailTemplateService $mailTemplateService
	 * @param FinisherContext $finisherContext
	 */
	public function __construct(MailTemplateService $mailTemplateService, FinisherContext $finisherContext) {
		$this->mailTemplateService = $mailTemplateService;
		$this->finisherContext = $finisherContext;
	}

	/**
	 * @return MailTemplateService
	 */
	public function getMailTemplateService(): MailTemplateService {
		return $this->mailTemplateService;
	}

	/**
	 * @param MailTemplateService $mailTemplateService
	 */
	public function setMailTemplateService(MailTemplateService $mailTemplateService): void {
		$this->mailTemplateService = $mailTemplateService;
	}

	/**
	 * @return FinisherContext
	 */
	public function getFinisherContext(): FinisherContext {
		return $this->finisherContext;
	}
}
