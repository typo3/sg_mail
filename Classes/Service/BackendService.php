<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Service;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Result;
use Psr\Http\Message\ServerRequestInterface;
use SGalinski\SgMail\Domain\Repository\FrontendUserGroupRepository;
use SGalinski\SgMail\Domain\Repository\MailRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use function count;
use function in_array;

/**
 * Backend Service class
 */
class BackendService {
	// options for the queue search filter
	public const SENDER_FILTER_OPTION = 0;
	public const RECIPIENT_FILTER_OPTION = 1;
	public const SUBJECT_FILTER_OPTION = 2;
	public const MAILTEXT_FILTER_OPTION = 3;
	public const CC_FILTER_OPTION = 4;
	public const BCC_FILTER_OPTION = 5;
	public const FROM_NAME_FILTER_OPTION = 6;
	public const REPLY_TO_NAME_FILTER_OPTION = 7;

	// constants for deetermining the backend mode
	public const BACKEND_MODE_EDITOR = 'editor';
	public const BACKEND_MODE_EDITOR_CONTROLLER = 'Mail';
	public const BACKEND_MODE_QUEUE = 'queue';
	public const BACKEND_MODE_QUEUE_CONTROLLER = 'Queue';
	public const BACKEND_MODE_QUEUE_NEWSLETTER = 'Newsletter';
	public const BACKEND_MODE_LAYOUT_CONTROLLER = 'Layout';

	public const CSV_EXPORT_BATCH_SIZE = 10000;

	/**
	 * Generate a csv string from the queues, respecting the given filters
	 *
	 * @param array $filters
	 * @return void
	 * @throws Exception
	 */
	public static function writeCsvFromQueue(array $filters = []): void {
		$mailRepository = GeneralUtility::makeInstance(MailRepository::class);

		/** @var ServerRequestInterface $request */
		$request = $GLOBALS['TYPO3_REQUEST'];
		$pageUid = (int) $request->getQueryParams()['id'];
		$ignoreFields = ['uid', 'pid', 'tstamp',
			'password', 'starttime', 'endtime', 'deleted', 'sent', 'priority', 'crdate', 'cruser_id', 'hidden'];
		$dateFields = ['tstamp', 'starttime', 'endtime', 'crdate'];

		$doctrineQuery = $mailRepository->findAllEntries($pageUid, 0, $filters);
		$doctrineQuery->count('*');
		$totalRows = $doctrineQuery->executeQuery()->fetchOne();
		$doctrineQuery->select('*');

		$offset = 0;
		$first = TRUE;
		$handle = fopen('php://output', 'wb+');
		do {
			$doctrineQuery->setFirstResult($offset);
			$doctrineQuery->setMaxResults(self::CSV_EXPORT_BATCH_SIZE);
			$rows = $doctrineQuery->executeQuery();
			while ($mail = $rows->fetchAssociative()) {
				if ($first) { // Write column headers before the first row
					$first = FALSE;
					$row = [];
					foreach ($mail as $field => $value) {
						if (!in_array($field, $ignoreFields, TRUE)) {
							$label = $GLOBALS['TCA']['tx_sgmail_domain_model_mail']['columns'][$field]['label'] ?? '';
							if (str_starts_with($label, 'LLL:')) {
								$label = $GLOBALS['LANG']->sL($label);
							}
							$row[] = $label ?: $field;
						}
					}

					fputcsv($handle, $row, ',');
				}

				$row = [];
				foreach ($mail as $field => $value) {
					if (!in_array($field, $ignoreFields, TRUE)) {
						if (in_array($field, $dateFields, TRUE)) {
							$row[] = $value ? date('d.m.Y', $value) : '';
						} else {
							$row[] = (string) $value;
						}
					}
				}

				fputcsv($handle, $row, ',');
			}

			$offset += self::CSV_EXPORT_BATCH_SIZE;
		} while ($offset <= $totalRows);
	}

	/**
	 * Fetches an array of FE_Users by groups
	 *
	 * @param array $groupIds
	 * @return array|Result|int
	 * @throws Exception
	 */
	public static function getRecipientsByGroups(array $groupIds) {
		if (count($groupIds) < 1) {
			return [];
		}

		$frontendUserGroupRepository = GeneralUtility::makeInstance(FrontendUserGroupRepository::class);
		$groupIds = $frontendUserGroupRepository->getFullGroupIdsWithChildren($groupIds);
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		$queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
		$queryBuilder->select('*')
			->from('fe_users');
		foreach ($groupIds as $groupId) {
			$queryBuilder->orWhere($queryBuilder->expr()->inSet('usergroup', (int) $groupId));
		}

		$queryBuilder->groupBy('email');
		return $queryBuilder->executeQuery()->fetchAllAssociative();
	}
}
