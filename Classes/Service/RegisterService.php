<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Service;

use BadFunctionCallException;
use InvalidArgumentException;
use SGalinski\SgMail\Exceptions\TemplateNotFoundException;
use TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Provides an api for registering your mail templates
 */
class RegisterService implements SingletonInterface {
	public const CACHE_NAME = 'sg_mail_registerArrayCache';
	public const CACHE_LIFETIME_IN_SECONDS = 86400;
	public const DEFAULT_TEMPLATE_PATH = 'Resources/Private/Templates/SgMail/';
	public const CONFIG_PATH = 'MailTemplates/Forms';

	/**
	 * contains the actual registration data
	 *
	 * @var array
	 */
	private array $registerArray = [];

	/**
	 * Get all registered templates from the cache. if the cache expired, the entries are generated
	 *
	 * @return array
	 * @throws NoSuchCacheException
	 * @throws BadFunctionCallException
	 * @throws InvalidArgumentException
	 */
	public function getRegisterArray(): array {
		if (count($this->registerArray) > 1) {
			return $this->registerArray;
		}

		$cacheManager = GeneralUtility::makeInstance(CacheManager::class);
		$cache = NULL;
		if ($cacheManager->hasCache(self::CACHE_NAME)) {
			$cache = $cacheManager->getCache(self::CACHE_NAME);
			$registerArray = $cache->get('sg_mail');
		} else {
			$registerArray = [];
		}

		if (!$registerArray || !is_array($registerArray)) {
			$registerArray = $this->registerExtensions();
			if (is_array($registerArray) && $cache) {
				$cache->set('sg_mail', $registerArray, [], self::CACHE_LIFETIME_IN_SECONDS);
			}
		}

		if (!is_array($registerArray)) {
			$registerArray = [];
		}

		return $registerArray;
	}

	/**
	 * Find a template from the registry
	 *
	 * @param string $extKey
	 * @param string $templateName
	 * @param SiteLanguage|null $siteLanguage
	 * @return array
	 * @throws NoSuchCacheException
	 * @throws TemplateNotFoundException
	 */
	public function findTemplate(string $extKey, string $templateName, SiteLanguage $siteLanguage = NULL): array {
		$registerArray = $this->getRegisterArray();
		if (!isset($registerArray[$extKey][$templateName])) {
			throw new TemplateNotFoundException(
				'The template "%s" is not registered',
				1620111957029,
				['extensionKey' => $extKey, 'templateName' => $templateName]
			);
		}

		$template = $registerArray[$extKey][$templateName];
		$template['sys_language_uid'] = ($siteLanguage ? $siteLanguage->getLanguageId() : 0);
		$typo3Language = ($siteLanguage ? $siteLanguage->getTypo3Language() : 'default');
		if (str_starts_with($template['subject'], 'LLL:')) {
			$template['subject'] = LocalizationUtility::translate(
				$template['subject'],
				NULL,
				NULL,
				$typo3Language
			);
		}

		if (isset($template['templateContent'])) {
			$template['content'] = $template['templateContent'];
		} elseif (isset($template['templatePath']) && file_exists($template['templatePath'] . 'template.html')) {
			$template['content'] = file_get_contents($template['templatePath'] . 'template.html');
		} else {
			$template['content'] = '';
		}

		return $template;
	}

	/**
	 * Filter the register array to have only whitelisted templates for this domain
	 *
	 * @param int $siteRootId
	 * @return array
	 * @throws InvalidArgumentException
	 * @throws BadFunctionCallException
	 * @throws NoSuchCacheException
	 */
	public function getNonBlacklistedTemplates(int $siteRootId): array {
		$registerArray = $this->getRegisterArray();
		$extensionConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_mail'] ?? [];
		if (isset($extensionConfiguration['excludeTemplates']) && $extensionConfiguration['excludeTemplates'] !== '') {
			$excludedTemplatesWithSiteId = GeneralUtility::trimExplode(
				';',
				$extensionConfiguration['excludeTemplates'],
				TRUE
			);

			foreach ($excludedTemplatesWithSiteId as $currentSite) {
				$currentSiteBlacklist = GeneralUtility::trimExplode(',', $currentSite, TRUE);
				if ((int) $currentSiteBlacklist[0] === $siteRootId) {
					foreach ($currentSiteBlacklist as $excludedTemplate) {
						if (strpos($excludedTemplate, '.') !== FALSE) {
							[$extensionKey, $templateName] = GeneralUtility::trimExplode('.', $excludedTemplate);
							if ($extensionKey && $templateName && isset($registerArray[$extensionKey][$templateName])) {
								unset($registerArray[$extensionKey][$templateName]);
							}
						}
					}
				}
			}
		}

		// filter out excluded templates from all domains
		if (isset($extensionConfiguration['excludeTemplatesAllDomains']) &&
			$extensionConfiguration['excludeTemplatesAllDomains'] !== ''
		) {
			$excludedTemplates = GeneralUtility::trimExplode(
				',',
				$extensionConfiguration['excludeTemplatesAllDomains'],
				TRUE
			);
			foreach ($excludedTemplates as $excludedTemplate) {
				[$extensionKey, $templateName] = GeneralUtility::trimExplode('.', $excludedTemplate);
				if ($extensionKey && $templateName && isset($registerArray[$extensionKey][$templateName])) {
					unset($registerArray[$extensionKey][$templateName]);
				}
			}
		}

		// remove empty entries
		foreach ($registerArray as $extensionKey => $extensionTemplates) {
			if (!count($extensionTemplates)) {
				unset($registerArray[$extensionKey]);
			} else {
				ksort($extensionTemplates);
				$registerArray[$extensionKey] = $extensionTemplates;
			}
		}

		reset($registerArray);
		return $registerArray;
	}

	/**
	 * Checks if a template is blacklisted for a given siteroot id
	 *
	 * @param string $extensionKey
	 * @param string $templateName
	 * @param int $siteRootId
	 * @return boolean
	 * @throws InvalidArgumentException
	 * @throws BadFunctionCallException
	 * @throws NoSuchCacheException
	 */
	public function isTemplateBlacklisted(string $extensionKey, string $templateName, int $siteRootId): bool {
		$nonBlacklistedTemplates = $this->getNonBlacklistedTemplates($siteRootId);
		if (array_key_exists($extensionKey, $nonBlacklistedTemplates) && array_key_exists(
			$templateName,
			$nonBlacklistedTemplates[$extensionKey]
		)) {
			return !$nonBlacklistedTemplates[$extensionKey][$templateName];
		}

		return TRUE;
	}

	/**
	 * Get the template keys in an array suitable for filtering
	 *
	 * @param int $pageUid
	 * @return array
	 * @throws InvalidArgumentException
	 * @throws BadFunctionCallException
	 * @throws NoSuchCacheException
	 */
	public function getTemplatesForFilter(int $pageUid): array {
		$registerArray = $this->getNonBlacklistedTemplates($pageUid);
		$templates = [];
		foreach ($registerArray as $extensions) {
			foreach ($extensions as $template => $key) {
				$templates[$key['extension']][] = [
					'name' => $key['templateName'],
					'is_manual' => $this->isManuallyRegisteredTemplate($key['templateName']),
				];
			}
		}

		array_unshift($templates, '');
		return $templates;
	}

	/**
	 * Read every registered file and create a registration entry in the registerArray if possible
	 *
	 * @return array
	 */
	private function registerExtensions(): array {
		$this->registerArray = [];

		if (isset($GLOBALS['sg_mail']) && is_array($GLOBALS['sg_mail'])) {
			foreach ($GLOBALS['sg_mail'] as $extensionName => $templates) {
				foreach ($templates as $templateKey => $registerFile) {
					$registerFile = GeneralUtility::getFileAbsFileName($registerFile);
					if (!is_file($registerFile)) {
						continue;
					}

					$configArray = (include $registerFile);
					$extensionKey = $configArray['extension_key'];
					$templateKey = $configArray['template_key'];
					if ($extensionKey === NULL) {
						continue;
					}

					$this->writeRegisterArrayEntry($extensionKey, $templateKey, $configArray);
				}
			}
		}

		// read automatically created extensionName registrations and write the entries
		$configurationLocation = $this->getRegistrationPath();
		$registerFolder = GeneralUtility::getFileAbsFileName($configurationLocation);
		if (!is_dir($registerFolder)) {
			GeneralUtility::mkdir_deep($registerFolder);
		}

		$configFiles = GeneralUtility::getFilesInDir($registerFolder);
		foreach ($configFiles as $configFile) {
			$pathToRegistrationFile = $registerFolder . '/' . $configFile;
			if (!is_file($pathToRegistrationFile)) {
				continue;
			}

			// get file name without folders
			$filename = basename($pathToRegistrationFile);
			$filenameWithoutHash = GeneralUtility::trimExplode('_', $filename, FALSE, 2)[1];
			$hash = md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] . '|' . $filenameWithoutHash);
			// if the filename doesn't start with the hash value, ignore it
			if (!str_starts_with($filename, $hash)) {
				continue;
			}

			$configArray = (include $pathToRegistrationFile);
			$extensionKey = $configArray['extension_key'];
			$templateKey = $configArray['template_key'];
			if ($extensionKey === NULL || $templateKey === NULL) {
				continue;
			}

			$this->writeRegisterArrayEntry($extensionKey, $templateKey, $configArray);
		}

		return $this->registerArray;
	}

	/**
	 * write a single entry into the register array
	 *
	 * @param string $extensionKey
	 * @param string $templateKey
	 * @param array $configArray
	 */
	private function writeRegisterArrayEntry(
		string $extensionKey,
		string $templateKey,
		array $configArray
	): void {
		// template_path means where to find the mail body content (usually it's set directly in the registration
		// as template_content)
		$templatePath = '';
		if (isset($configArray['template_path']) && $configArray['template_path']) {
			$templatePath = $configArray['template_path'];
		} elseif (ExtensionManagementUtility::isLoaded($extensionKey)) {
			// transform template directory name: your_templates => YourTemplates/
			$templateDirectoryParts = GeneralUtility::trimExplode('_', $templateKey);
			$templateDirectory = '';
			foreach ($templateDirectoryParts as $part) {
				$templateDirectory .= ucfirst($part);
			}

			$templateDirectory .= '/';
			$templatePath = ExtensionManagementUtility::extPath($extensionKey) .
				self::DEFAULT_TEMPLATE_PATH . $templateDirectory;
		}

		$description = $configArray['description'];
		$subject = $configArray['subject'];
		$marker = $configArray['markers'];
		$templateContent = $configArray['templateContent'];

		$this->registerArray[$extensionKey][$templateKey] = [
			'isManual' => $this->isManuallyRegisteredTemplate($templateKey),
			'templatePath' => $templatePath,
			'description' => $description,
			'marker' => $marker,
			'extension' => $extensionKey,
			'templateName' => $templateKey,
			'subject' => $subject,
			'templateContent' => $templateContent,
			'render_with_nl2br' => $configArray['render_with_nl2br'] ?? TRUE,
		];
	}

	/**
	 * Returns the path to the configured location where automatic mail template registrations should be
	 *
	 * @return string
	 */
	private function getRegistrationPath(): string {
		// get the location where automatic registrations should be stored
		return Environment::getPublicPath()
			. '/fileadmin/'
			. ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_mail']['configurationLocation'] ?? 'SgMail')
			. '/' . self::CONFIG_PATH;
	}

	/**
	 * Clear the sgmail register cache
	 *
	 * @throws NoSuchCacheException
	 */
	public function clearCaches(): void {
		$cacheManager = GeneralUtility::makeInstance(CacheManager::class);

		/** @var FrontendInterface $cache */
		$cache = $cacheManager->getCache(self::CACHE_NAME);
		$cache->flush();
	}

	/**
	 * Write the mail registration file
	 *
	 * @param string $templateKey
	 * @param string $extensionKey
	 * @param array $markers
	 * @param string $subject
	 * @param string $description
	 * @param bool $renderWithNl2br
	 * @return string
	 */
	public function writeRegisterFile(
		string $templateKey,
		string $extensionKey,
		array $markers,
		string $subject = '',
		string $description = '',
		bool $renderWithNl2br = TRUE,
	): string {
		$newRegisterArray = [
			'extension_key' => $extensionKey,
			'template_key' => $templateKey,
			'description' => $description,
			'subject' => $subject,
			'render_with_nl2br' => $renderWithNl2br,
			'markers' => [],
			'templateContent' => '<f:format.raw>{all_fields_html}</f:format.raw>',
		];

		foreach ($markers as $marker) {
			$markerName = $marker['identifier'];
			$markerArray = GeneralUtility::trimExplode(',', $markerName, FALSE, 2);
			$markerName = $markerArray[0];

			$newRegisterArray['markers'][] = [
				'marker' => $markerName,
				'type' => $marker['type'] ?? MailTemplateService::MARKER_TYPE_STRING,
				'value' => $marker['value'],
				'description' => $marker['description'],
				'markerLabel' => $marker['markerLabel']
			];
		}

		$registerFile = $this->getRegisterFilePath($templateKey);
		file_put_contents($registerFile, '<?php return ' . var_export($newRegisterArray, TRUE) . ';');
		return $registerFile;
	}

	/**
	 * Get the absolute file path to the register file
	 *
	 * @param string $templateKey
	 * @return string
	 */
	protected function getRegisterFilePath(string $templateKey): string {
		$configurationLocation = $this->getRegistrationPath();
		$registerFolder = GeneralUtility::getFileAbsFileName($configurationLocation);
		GeneralUtility::mkdir_deep($registerFolder);

		$hashPrefix = md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] . '|' . $templateKey . '.php');
		return GeneralUtility::getFileAbsFileName(
			$registerFolder . '/' . $hashPrefix . '_' . $templateKey . '.php'
		);
	}

	/**
	 * WDelete a mail register file
	 *
	 * @param string $templateKey
	 */
	public function deleteRegisterFile(string $templateKey): void {
		$registerFile = $this->getRegisterFilePath($templateKey);
		if (file_exists($registerFile)) {
			unlink($registerFile);
		}
	}

	/**
	 * Trims non-allowed characters from the form field marker name
	 *
	 * @param string $markerName
	 * @return string
	 */
	public static function normalizeFormFieldMarkerName(string $markerName = ''): string {
		return preg_replace('/[^a-z0-9\-_]/i', '', $markerName);
	}

	/**
	 * Register the caches for the RegisterService
	 * This method is for usage in ext_localconf.php to register the cache configuration
	 */
	public function registerCache(): void {
		$cacheConfigurations = $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'];
		if (!isset($cacheConfigurations[self::CACHE_NAME])) {
			$cacheConfigurations[self::CACHE_NAME] = [
				'backend' => Typo3DatabaseBackend::class,
				'frontend' => VariableFrontend::class,
				'options' => []
			];
		}

		if (!isset($cacheConfigurations[self::CACHE_NAME]['groups'])) {
			$cacheConfigurations[self::CACHE_NAME]['groups'] = [
				'citypower'
			];
		}
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'] = $cacheConfigurations;
	}

	/**
	 * Delete an email template (database entries and automatic config file)
	 * works only for manually created templates
	 *
	 * @param string $extensionKey
	 * @param string $templateName
	 * @param string $filePath
	 * @return bool
	 * @throws NoSuchCacheException
	 */
	public function deleteTemplate(string $extensionKey, string $templateName, string $filePath = ''): bool {
		$configurationLocation = $this->getRegistrationPath();
		$registerFolder = GeneralUtility::getFileAbsFileName($configurationLocation);
		GeneralUtility::mkdir_deep($registerFolder);

		if ($filePath === '') {
			$hashPrefix = md5($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] . '|' . $templateName . '.php');
			$filePath = GeneralUtility::getFileAbsFileName(
				$registerFolder . '/' . $hashPrefix . '_' . $templateName . '.php'
			);
		}

		if (file_exists($filePath)) {
			$success = unlink($filePath);
		} else {
			return FALSE;
		}

		// delete from register array
		unset($this->registerArray[$extensionKey][$templateName]);

		// delete from database
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_sgmail_domain_model_template'
		);
		$queryBuilder->delete('tx_sgmail_domain_model_template')
			->where(
				$queryBuilder->expr()->eq('extension_key', $queryBuilder->quote($extensionKey, \PDO::PARAM_STR))
			)->andWhere(
				$queryBuilder->expr()->eq('template_name', $queryBuilder->quote($templateName, \PDO::PARAM_STR))
			)->executeStatement();

		$this->clearCaches();

		return $success;
	}

	/**
	 * Check if  a template was manually registered (true) or is delivered by an extension (false)
	 *
	 * @param string $templateName
	 * @return bool
	 */
	public function isManuallyRegisteredTemplate(string $templateName): bool {
		if (!file_exists($this->getRegisterFilePath($templateName))) {
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Update an edited template entry, so that it uses the new keys
	 *
	 * @param string $oldTemplateName
	 * @param string $oldExtensionKey
	 * @param string $newTemplateName
	 * @param string $newExtensionKey
	 */
	public function migrateTemplateEntries(
		string $oldTemplateName,
		string $oldExtensionKey,
		string $newTemplateName,
		string $newExtensionKey
	): void {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_sgmail_domain_model_template'
		);
		$queryBuilder->update('tx_sgmail_domain_model_template')
			->where(
				$queryBuilder->expr()->eq('extension_key', $queryBuilder->quote($oldExtensionKey, \PDO::PARAM_STR))
			)
			->andWhere(
				$queryBuilder->expr()->eq('template_name', $queryBuilder->quote($oldTemplateName, \PDO::PARAM_STR))
			)
			->set(
				'extension_key',
				$newExtensionKey
			)->set('template_name', $newTemplateName)->executeStatement();
	}
}
