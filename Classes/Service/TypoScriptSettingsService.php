<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager;

/**
 * Returns the typoscript setup based on the page id
 *
 */
class TypoScriptSettingsService implements SingletonInterface {
	/**
	 * @var array
	 */
	protected static $typoScriptCache = [];

	/**
	 * Returns the typoscript settings of a given page and extension
	 *
	 * @param int $pageId
	 * @param string $extensionKey
	 * @return array
	 */
	public function getSettings(int $pageId, string $extensionKey): array {
		$cacheHash = $extensionKey . $pageId;
		if (!isset(self::$typoScriptCache[$cacheHash])) {
			// in v11 env we have an $_GET['id'] instead of POST
			$mode = '';
			$tmpId = 0;
			if (isset($_POST['id'])) {
				$mode = 'POST';
				$tmpId = $_POST['id'];
			}
			if (isset($_GET['id'])) {
				$mode = 'GET';
				$tmpId = $_GET['id'];
			}

			$typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
			$configurationManager = GeneralUtility::makeInstance(BackendConfigurationManager::class);

			self::$typoScriptCache[$cacheHash] = $typoScriptService->convertTypoScriptArrayToPlainArray(
				(array) $configurationManager->getTypoScriptSetup()['module.'][$extensionKey . '.']['settings.']
			);

			if ($mode === 'GET') {
				$_GET['id'] = $tmpId;
			} elseif ($mode === 'POST') {
				$_POST['id'] = $tmpId;
			}
		}

		return self::$typoScriptCache[$cacheHash];
	}
}
