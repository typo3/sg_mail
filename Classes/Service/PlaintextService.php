<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

declare(strict_types=1);

namespace SGalinski\SgMail\Service;

/**
 * Class PlaintextService
 */
class PlaintextService {
	/**
	 * Function makePlain() removes html tags and add linebreaks
	 *        Easy generate a plain email bodytext from a html bodytext
	 *
	 * @param array|string $content HTML Mail bodytext
	 * @return string $content
	 */
	public function makePlain($content): string {
		$content = $this->removeInvisibleElements($content);
		$content = $this->removeLinebreaksAndTabs($content);
		$content = $this->addLineBreaks($content);
		$content = $this->addSpaceToTableCells($content);
		$content = $this->extractLinkForPlainTextContent($content);
		$content = $this->removeTags($content);
		$array = [
			'<br >',
			'<br>',
			'<br/>',
			'<br />'
		];
		return trim(str_replace($array, PHP_EOL, $content));
	}

	/**
	 * Remove all invisible elements
	 *
	 * @param string $content
	 * @return string
	 */
	protected function removeInvisibleElements(string $content): string {
		$replaceContent = preg_replace(
			[
				'/<head[^>]*?>.*?<\/head>/siu',
				'/<style[^>]*?>.*?<\/style>/siu',
				'/<script[^>]*?>.*?<\/script>/siu',
				'/<object[^>]*?>.*?<\/object>/siu',
				'/<embed[^>]*?>.*?<\/embed>/siu',
				'/<applet[^>]*?>.*?<\/applet>/siu',
				'/<noframes[^>]*?>.*?<\/noframes>/siu',
				'/<noscript[^>]*?>.*?<\/noscript>/siu',
				'/<noembed[^>]*?>.*?<\/noembed>/siu',
			],
			[
				'', '', '', '', '', '', '', '', ''
			],
			$content
		);

		return $replaceContent ?: strip_tags($content);
	}

	/**
	 * Remove linebreaks and tabs
	 *
	 * @param string $content
	 * @return string
	 */
	protected function removeLinebreaksAndTabs(string $content): string {
		return trim(str_replace(["\n", "\r", "\t"], '', $content));
	}

	/**
	 * add linebreaks on some parts (</p> => </p><br>)
	 *
	 * @param string $content
	 * @return string
	 */
	protected function addLineBreaks(string $content): string {
		return str_replace(
			[
				'</p>',
				'</tr>',
				'<ul>',
				'</li>',
				'</h1>',
				'</h2>',
				'</h3>',
				'</h4>',
				'</h5>',
				'</h6>',
				'</div>',
				'</legend>',
				'</fieldset>',
				'</dd>',
				'</dt>'
			],
			'</p><br>',
			$content
		);
	}

	/**
	 * Add a space character to a table cell
	 *
	 * @param string $content
	 * @return string
	 */
	protected function addSpaceToTableCells(string $content): string {
		return str_replace(['</td>', '</th>'], '</td> ', $content);
	}

	/**
	 * Remove all tags but keep br and address
	 *
	 * @param string $content
	 * @return string
	 */
	protected function removeTags(string $content): string {
		return strip_tags($content, '<br><address>');
	}

	/**
	 * Extract uri from href attributes and decode it
	 *
	 *  replace links
	 *      <a href="xyz">LINK</a>
	 *      ->
	 *      LINK [xyz]
	 *
	 * @param string $content
	 * @return string
	 */
	protected function extractLinkForPlainTextContent(string $content): string {
		$pattern = '/<a[^>]+href\s*=\s*["\']([^"\']+)["\'][^>]*>(.*?)<\/a>/misu';
		$replaceContent = preg_replace_callback(
			$pattern,
			static function ($matches) {
				return $matches[2] . ' [' . htmlspecialchars_decode($matches[1]) . ']';
			},
			$content
		);

		return $replaceContent ?: $content;
	}
}
