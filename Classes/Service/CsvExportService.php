<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Service;

use SGalinski\SgMail\Finisher\Forms\FormsFinisher;
use Symfony\Component\Mime\MimeTypes;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;

/**
 * This service is used within the FormsFinisher, to generate a CSV file from the supplied marker data
 * and attach it to the mail (if the option in the form was checked).
 *
 * @see FormsFinisher
 */
class CsvExportService implements SingletonInterface {
	/**
	 * the path within fileadmin, where the temporary files are stored
	 */
	public const CSV_TEMP_PATH = 'Temp/CsvAttachments';

	/**
	 * @param array $markerData
	 * @param array $markerLabels
	 * @param MailTemplateService $mailTemplateService
	 * @return string
	 * @throws InsufficientFolderAccessPermissionsException
	 * @throws ExistingTargetFolderException
	 */
	public function generateCsvFromMarkerDataAndAttachToMail(
		array $markerData,
		array $markerLabels,
		MailTemplateService $mailTemplateService
	): string {
		$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			\TYPO3\CMS\Core\Resource\ResourceFactory::class
		);
		$tempCsvFilePath = $this->getTempCsvFilePath();
		$defaultStorage = $resourceFactory->getDefaultStorage();
		if (!$defaultStorage) {
			return '';
		}

		if (!$defaultStorage->hasFolder($tempCsvFilePath)) {
			$defaultStorage->createFolder($tempCsvFilePath);
		}

		if (!$defaultStorage->hasFolder($tempCsvFilePath)) {
			return '';
		}

		$tempCsvFolder = $defaultStorage->getFolder($tempCsvFilePath);

		if ($tempCsvFolder === NULL) {
			return '';
		}

		if (count($markerData) !== count($markerLabels)) {
			return '';
		}

		// Iterate over $markerData to find null values & parse marker values, depending on the type
		foreach ($markerData as $key => $value) {
			if ($value === NULL) {
				// Remove corresponding elements from both arrays
				unset($markerData[$key], $markerLabels[$key]);
			} elseif (is_array($value)) {
				if (is_array($value[0])) {
					$markerData[$key] = implode(', ', $value[0]);
				} else {
					$markerData[$key] = implode(', ', $value);
				}
			} elseif (is_bool($value)) {
				$valueAsString = $value ? 'true' : 'false';
				$markerData[$key] = $valueAsString;
			} elseif ($value instanceof \DateTime) {
				$markerData[$key] = $value->format('d.m.Y');
			} elseif ($value instanceof FileReference) {
				$originalResource = $value->getOriginalResource();
				if ($originalResource) {
					$value = $originalResource->getPublicUrl();
					$markerData[$key] = $value;
				} else {
					$markerData[$key] = get_class($value);
				}
			} elseif (is_object($value)) {
				if (method_exists($value, '__toString')) {
					$markerData[$key] = $value->__toString();
				} else {
					$markerData[$key] = get_class($value);
				}
			}
		}

		$csvData = fopen('php://temp', 'wb');
		// header row
		fputcsv($csvData, array_values($markerLabels));

		$row = [];
		foreach ($markerLabels as $labelKey => $labelValue) {
			// Check if the label key exists in the marker data
			if (array_key_exists($labelKey, $markerData)) {
				$row[] = $markerData[$labelKey];
			} else {
				$row[] = '';
			}
		}

		fputcsv($csvData, $row);
		rewind($csvData);
		$csvString = stream_get_contents($csvData);
		fclose($csvData);

		// this is needed, so that $mailTemplateService->addFileFromFilePathForDirectSending() works
		$mailTemplateService->setIgnoreMailQueue(TRUE);
		$temporaryFileName = GeneralUtility::tempnam('sg_mail-finisher-csvexport');
		if (file_exists($temporaryFileName)) {
			$contextOptions = [
				'http' => [
					'header' => "Content-Type: text/csv\r\n"
				]
			];
			$context = stream_context_create($contextOptions);
			// write csv into temp file
			file_put_contents($temporaryFileName, $csvString, 0, $context);
			$csvFileName = basename($temporaryFileName) . '.csv';
			$fileDestinationPath = $this->getTempCsvFilePath(TRUE) . '/' . $csvFileName;
			// move temp file into our temp directory within fileadmin
			if (rename($temporaryFileName, $fileDestinationPath)) {
				GeneralUtility::unlink_tempfile($temporaryFileName);
			}

			$csvFile = $tempCsvFolder->getFile($csvFileName);
			if (!$csvFile) {
				return $fileDestinationPath;
			}

			$mailTemplateService->addFileResourceAttachment(NULL, $csvFile);

			// return the temp file name, so that it can be deleted, after the mail has been sent
			return $csvFile->getIdentifier();
		}

		return '';
	}

	/**
	 * Returns the path to the configured location where the CsvExportService stores temporary files
	 *
	 * @param bool $absolute
	 * @return string
	 * @see CsvExportService
	 */
	private function getTempCsvFilePath(bool $absolute = FALSE): string {
		if ($absolute) {
			return Environment::getPublicPath()
				. '/fileadmin/'
				. ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_mail']['configurationLocation'] ?? 'SgMail')
				. '/' . self::CSV_TEMP_PATH;
		}

		return ($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_mail']['configurationLocation'] ?? 'SgMail')
			. '/' . self::CSV_TEMP_PATH;
	}
}
