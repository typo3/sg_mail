<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Service;

use DateTime;
use FluidTYPO3\Vhs\Utility\ContextUtility;
use Psr\Http\Message\ServerRequestInterface;
use SGalinski\SgMail\Domain\Model\Mail;
use SGalinski\SgMail\Domain\Model\Template;
use SGalinski\SgMail\Domain\Repository\LayoutRepository;
use SGalinski\SgMail\Domain\Repository\MailRepository;
use SGalinski\SgMail\Domain\Repository\TemplateRepository;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference as CoreFileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Type\File\FileInfo;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * MailTemplateService
 */
class MailTemplateService {
	public const MARKER_TYPE_STRING = 'String';
	public const MARKER_TYPE_ARRAY = 'Array';
	public const MARKER_TYPE_OBJECT = 'Object';
	public const MARKER_TYPE_FILE = 'File';
	public const MARKER_TYPE_DATE = 'Date';
	public const DEFAULT_LANGUAGE = 'default';
	public const DEFAULT_TEMPLATE_PATH = 'Resources/Private/Templates/SgMail/';
	public const CACHE_NAME = 'sg_mail_registerArrayCache';
	public const CACHE_LIFETIME_IN_SECONDS = 86400;
	public const REGISTER_FILE = 'Register.php';
	public const CONFIG_PATH = 'Configuration/MailTemplates';

	/**
	 * @var array
	 */
	private static $templateObjectCache = [];

	/**
	 * @var array
	 */
	private static $mailObjectCache = [];

	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;

	/**
	 * @var LayoutRepository
	 */
	protected $layoutRepository;

	/**
	 * @var MailRepository
	 */
	protected $mailRepository;

	/**
	 * @var RegisterService
	 */
	protected $registerService;

	/**
	 * @var string
	 */
	private $toAddresses = '';

	/**
	 * @var mixed|string
	 */
	private $fromAddress;

	/**
	 * @var string
	 */
	private $ccAddresses;

	/**
	 * @var string
	 */
	private $replyToAddress = '';

	/**
	 * @var SiteLanguage
	 */
	private $siteLanguage;

	/**
	 * @var bool
	 */
	private $ignoreMailQueue = TRUE;

	/**
	 * @var string $templateName
	 */
	private $templateName;

	/**
	 * @var string $subject
	 */
	private $subject;

	/**
	 * @var string
	 */
	private $overwrittenEmailBody = '';

	/**
	 * @var string $extensionKey
	 */
	private $extensionKey;

	/**
	 * @var array $markers
	 */
	private $markers = [];

	/**
	 * @var array $markerLabels
	 */
	private $markerLabels;

	/**
	 * @var string
	 */
	private $bccAddresses;

	/**
	 * @var int
	 */
	private $priority = Mail::PRIORITY_LOWEST;

	/**
	 * @TODO Cleanup the code and use a proper getter and mechanism to ensure that the PID is set or calculate it. Currently this is wrong to have 0 as the PID. This causes tons of possible bugs.
	 * @var int
	 */
	private $pid = 0;

	/**
	 * @var mixed|string
	 */
	private $fromName = '';

	/**
	 * @var string
	 */
	private $mailBodyToSend = '';

	/**
	 * @var string
	 */
	private $subjectToSend;

	/**
	 * Contains attachments that are attached to the mails by default and are set in the mail template directly
	 *
	 * @var string
	 */
	private $defaultAttachments = '';

	/**
	 * @param SiteLanguage $siteLanguage
	 * @return $this
	 */
	public function setSiteLanguage(SiteLanguage $siteLanguage): MailTemplateService {
		$this->siteLanguage = $siteLanguage;
		return $this;
	}

	/**
	 * @return SiteLanguage|null
	 * @throws SiteNotFoundException
	 */
	public function getSiteLanguage(): ?SiteLanguage {
		if (!$this->siteLanguage) {
			// Mailservice needs a SiteLanguage to Work, which will not always be set.
			// easiest Solution: Just grab first Possible Page's DefaultLanguage
			$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
			if ($this->pid) {
				$site = $siteFinder->getSiteByPageId($this->pid);
				if ($site) {
					$this->setSiteLanguage($site->getDefaultLanguage());
				}
			} else {
				$currentSite = $GLOBALS['TSFE']->site ?? NULL;
				if ($currentSite) {
					$this->setSiteLanguage($currentSite->getDefaultLanguage());
				} else {
					$possibleSites = $siteFinder->getAllSites();
					$site = current($possibleSites);
					$this->setSiteLanguage($site->getDefaultLanguage());
				}
			}
		}
		return $this->siteLanguage;
	}

	/**
	 * @param string $templateName
	 * @return MailTemplateService
	 */
	public function setTemplateName(string $templateName): MailTemplateService {
		$this->templateName = $templateName;
		return $this;
	}

	/**
	 * @param string $extensionKey
	 * @return MailTemplateService
	 */
	public function setExtensionKey(string $extensionKey): MailTemplateService {
		$this->extensionKey = $extensionKey;
		return $this;
	}

	/**
	 * @param array $markers
	 * @return MailTemplateService
	 */
	public function addMarkers(array $markers): MailTemplateService {
		$this->setMarkers(array_merge($this->markers, $markers));
		return $this;
	}

	/**
	 * @param array $markers
	 * @return MailTemplateService
	 */
	public function setMarkers(array $markers): MailTemplateService {
		$this->markers = $markers;

		/** @var ServerRequestInterface $request */
		$request = $GLOBALS['TYPO3_REQUEST'] ?? NULL;

		// add default marker "page"
		if (!isset($this->markers['page'])) {
			$tsfe = $GLOBALS['TSFE'] ?? NULL;
			$pageId = 0;
			if ($tsfe === NULL && $request && ContextUtility::isBackend()) {
				$pageId = $request->getQueryParams()['id'] ?? 0;
			} elseif ($tsfe) {
				$pageId = $tsfe->id;
			}

			if ($pageId > 0) {
				$page = GeneralUtility::makeInstance(PageRepository::class)->getPage($pageId);
				if ($page) {
					$this->markers['page'] = $page;
				}
			}
		}

		// add default marker "user"
		if ($request && !isset($this->markers['user'])) {
			$frontendUser = $request->getAttribute('frontend.user');
			if ($frontendUser) {
				$userData = $frontendUser->user ?? [];
				$this->markers['user'] = $userData;
			}
		}

		foreach ($markers as $key => $currentMarker) {
			if (!is_array($currentMarker) || !isset($currentMarker['markerLabel'])) {
				continue;
			}
			$this->markerLabels[$key] = $currentMarker['markerLabel'];
		}

		return $this;
	}

	/**
	 * @param int $priority
	 * @return MailTemplateService
	 */
	public function setPriority(int $priority): MailTemplateService {
		$this->priority = $priority;
		return $this;
	}

	/**
	 * @param string $fromAddress
	 * @param string $fromName
	 * @return MailTemplateService
	 */
	public function setFromAddress(string $fromAddress, string $fromName = ''): MailTemplateService {
		if ($fromAddress) {
			$this->fromAddress = $fromAddress;
		}

		return $this;
	}

	/**
	 * @param string $ccAddresses
	 * @return MailTemplateService
	 */
	public function setCcAddresses(string $ccAddresses): MailTemplateService {
		if ($ccAddresses) {
			$this->ccAddresses = $ccAddresses;
		}

		return $this;
	}

	/**
	 * @param string $bccAddresses
	 * @return MailTemplateService
	 */
	public function setBccAddresses(string $bccAddresses): MailTemplateService {
		if ($bccAddresses) {
			$this->bccAddresses = $bccAddresses;
		}

		return $this;
	}

	/**
	 * @param string $ccAddresses
	 * @return MailTemplateService
	 */
	public function appendCcAddresses(string $ccAddresses): MailTemplateService {
		if ($ccAddresses) {
			if ($this->ccAddresses !== '') {
				$this->ccAddresses .= ', ' . $ccAddresses;
			} else {
				$this->ccAddresses = $ccAddresses;
			}
		}

		return $this;
	}

	/**
	 * @param string $bccAddresses
	 * @return MailTemplateService
	 */
	public function appendBccAddresses(string $bccAddresses): MailTemplateService {
		if ($bccAddresses) {
			if ($this->bccAddresses !== '') {
				$this->bccAddresses .= ', ' . $bccAddresses;
			} else {
				$this->bccAddresses = $bccAddresses;
			}
		}

		return $this;
	}

	/**
	 * @param string $replyToAddress
	 * @return MailTemplateService
	 */
	public function setReplyToAddress(string $replyToAddress): MailTemplateService {
		if ($replyToAddress) {
			$this->replyToAddress = $replyToAddress;
		}

		return $this;
	}

	/**
	 * @param string $fromName
	 * @return MailTemplateService
	 */
	public function setFromName(string $fromName): MailTemplateService {
		$this->fromName = $fromName;
		return $this;
	}

	/**
	 * @param bool $ignoreMailQueue
	 * @return MailTemplateService
	 */
	public function setIgnoreMailQueue(bool $ignoreMailQueue): MailTemplateService {
		$this->ignoreMailQueue = $ignoreMailQueue;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSubjectToSend(): string {
		return $this->subjectToSend;
	}

	/**
	 * @param string $subjectToSend
	 * @return MailTemplateService
	 */
	public function setSubjectToSend(string $subjectToSend): MailTemplateService {
		$this->subjectToSend = $subjectToSend;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMailBodyToSend(): string {
		return $this->mailBodyToSend;
	}

	/**
	 * @param string $mailBodyToSend
	 * @return MailTemplateService
	 */
	public function setMailBodyToSend(string $mailBodyToSend): MailTemplateService {
		$this->mailBodyToSend = $mailBodyToSend;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getOverwrittenEmailBody(): string {
		return $this->overwrittenEmailBody;
	}

	/**
	 * @param string $overwrittenEmailBody
	 * @return MailTemplateService
	 */
	public function setOverwrittenEmailBody(string $overwrittenEmailBody): MailTemplateService {
		$this->overwrittenEmailBody = $overwrittenEmailBody;
		return $this;
	}

	/**
	 * set the page id from which this was called
	 *
	 * @param int $pid
	 * @return MailTemplateService
	 */
	public function setPid(int $pid): MailTemplateService {
		$this->pid = $pid;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSubject(): string {
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject(string $subject): void {
		$this->subject = $subject;
	}

	/**
	 * MailTemplateService constructor.
	 *
	 * @param string $templateName
	 * @param string $extensionKey
	 * @param array $markers
	 * @param array $markerLabels
	 */
	public function __construct(
		string $templateName = '',
		string $extensionKey = '',
		array $markers = [],
		array $markerLabels = []
	) {
		$this->templateName = $templateName;
		$this->extensionKey = $extensionKey;
		$this->setMarkers($markers);
		$this->markerLabels = $markerLabels;

		$this->registerService = GeneralUtility::makeInstance(RegisterService::class);
		$typoScriptSettingsService = GeneralUtility::makeInstance(TypoScriptSettingsService::class);
		$tsSettings = $typoScriptSettingsService->getSettings(0, 'tx_sgmail');

		// use defaultMailFromAddress if it is provided in LocalConfiguration.php; use the sg_mail TS setting as fallback
		if (filter_var($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'], FILTER_VALIDATE_EMAIL)) {
			$this->fromAddress = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'];
		} elseif (!filter_var($tsSettings['mail']['default']['from'], FILTER_VALIDATE_EMAIL)) {
			$this->fromAddress = 'noreply@example.org';
		} else {
			$this->fromAddress = $tsSettings['mail']['default']['from'];
		}

		if ($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName']) {
			$this->fromName = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'];
		}

		$bccAddresses = GeneralUtility::trimExplode(',', $tsSettings['mail']['default']['bcc']);
		$ccAddresses = GeneralUtility::trimExplode(',', $tsSettings['mail']['default']['cc']);
		foreach ($bccAddresses as $index => $email) {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				unset($bccAddresses[$index]);
			}
		}

		foreach ($ccAddresses as $index => $email) {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				unset($ccAddresses[$index]);
			}
		}

		$this->bccAddresses = implode(',', $bccAddresses);
		$this->ccAddresses = implode(',', $ccAddresses);
	}

	/**
	 * @return LayoutRepository
	 */
	protected function getLayoutRepository(): LayoutRepository {
		if ($this->layoutRepository === NULL) {
			$this->layoutRepository = GeneralUtility::makeInstance(LayoutRepository::class);
		}

		return $this->layoutRepository;
	}

	/**
	 * @return TemplateRepository
	 */
	protected function getTemplateRepository(): TemplateRepository {
		if ($this->templateRepository === NULL) {
			$this->templateRepository = GeneralUtility::makeInstance(TemplateRepository::class);
		}

		return $this->templateRepository;
	}

	/**
	 * @return MailRepository
	 */
	protected function getMailRepository(): MailRepository {
		if ($this->mailRepository === NULL) {
			$this->mailRepository = GeneralUtility::makeInstance(MailRepository::class);
		}

		return $this->mailRepository;
	}

	/**
	 * Return default markers for sg_mail
	 *
	 * @param string $translationKey
	 * @param array $marker
	 * @param string $extensionKey
	 * @return array
	 */
	public static function getDefaultTemplateMarker(
		string $translationKey,
		array $marker,
		string $extensionKey = 'sg_mail'
	): array {
		$languagePath = 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:' . $translationKey;
		// Need the key for translations
		if (trim($extensionKey) === '') {
			return [];
		}

		$generatedMarker = [];
		foreach ($marker as $markerName) {
			$generatedMarker[] = [
				'marker' => $markerName,
				'value' => $languagePath . '.example.' . $markerName,
				'description' => $languagePath . '.description.' . $markerName,
				'backend_translation_key' => $translationKey . '.example.' . $markerName,
				'extension_key' => $extensionKey
			];
		}

		return $generatedMarker;
	}

	/**
	 * Adds a file resource as attachment
	 *
	 * @param FileReference|null $fileReference
	 * @param File|null $file
	 * @param string|null $filePath
	 * @return MailTemplateService
	 * @throws InsufficientFolderAccessPermissionsException
	 */
	public function addFileResourceAttachment(
		FileReference $fileReference = NULL,
		FileInterface $file = NULL,
		string $filePath = NULL
	): MailTemplateService {
		if ($filePath) {
			$storageRepository = GeneralUtility::makeInstance(StorageRepository::class);
			$defaultStorage = $storageRepository->getDefaultStorage();
			if ($defaultStorage) {
				$dirname = str_replace([Environment::getPublicPath() . '/', 'fileadmin'], '', dirname($filePath));
				$folder = $defaultStorage->getFolder($dirname);
				if ($folder) {
					$file = $folder->getStorage()->getFileInFolder(basename($filePath), $folder);
				}
			}
		}

		if (!$file) {
			if (!$fileReference) {
				return $this;
			}

			$originalResource = $fileReference->getOriginalResource();
			if (!$originalResource) {
				return $this;
			}

			$file = $originalResource->getOriginalFile();
			if (!$file) {
				return $this;
			}
		}

		$coreFileReferenceMailFile = GeneralUtility::makeInstance(ResourceFactory::class)
			->createFileReferenceObject(
				[
					'uid_local' => $file->getUid(),
					'table_local' => 'sys_file',
					'uid' => uniqid('NEW_MAIL', TRUE)
				]
			);

		$newFileReference = GeneralUtility::makeInstance(FileReference::class);
		$newFileReference->setOriginalResource($coreFileReferenceMailFile);

		$this->markers[] = $newFileReference;
		return $this;
	}

	/**
	 * Set the default attachments
	 *
	 * @param string $attachmentString A newline separated string of attachment paths
	 * @return void
	 */
	public function setDefaultAttachments(string $attachmentString) {
		$this->defaultAttachments = $attachmentString;
	}

	/**
	 * Returns the default attachments
	 *
	 * @return string
	 */
	public function getDefaultAttachments(): string {
		return $this->defaultAttachments;
	}

	/**
	 * Set preview markers for the template editor
	 *
	 * @throws NoSuchCacheException
	 */
	public function setPreviewMarkers(): void {
		$previewMarkers = [];

		/** @var array $markerArray */
		$markerArray = $this->registerService->getRegisterArray()[$this->extensionKey][$this->templateName]['marker'];
		foreach ($markerArray as $marker) {
			$markerPath = GeneralUtility::trimExplode('.', $marker['marker']);
			$temporaryMarkerArray = [];
			foreach (array_reverse($markerPath) as $index => $markerPathSegment) {
				if ($index === 0) {
					if (isset($marker['markerLabel']) && $marker['markerLabel']) {
						$markerPathSegment = $marker['markerLabel'];
					}

					if (isset($marker['backend_translation_key']) && $marker['backend_translation_key']) {
						$temporaryMarkerArray[$markerPathSegment] = LocalizationUtility::translate(
							$marker['backend_translation_key'],
							$marker['extension_key']
						);
					} else {
						$temporaryMarkerArray[$markerPathSegment] = $marker['value'];
					}
				} else {
					$temporaryMarkerArray = [$markerPathSegment => $temporaryMarkerArray];
				}
			}

			$previewMarkers[] = $temporaryMarkerArray;
		}

		$this->setMarkers(array_merge_recursive(...$previewMarkers));
	}

	/**
	 * Send the Email
	 *
	 * @param bool $isPreview
	 * @param bool $isNewsletter
	 * @return bool email was sent or added to mail queue successfully?
	 * @throws IllegalObjectTypeException
	 * @throws NoSuchCacheException
	 * @throws UnknownObjectException
	 * @throws SiteNotFoundException
	 */
	public function sendEmail(bool $isPreview = FALSE, bool $isNewsletter = FALSE): bool {
		try {
			// TODO should be done inside the constructor, BUT we need the PID here that we can't auto-calculate in
			// situations like a multidomain instance and the scheduler. The pid should be mandatory inside the
			// constructor, but too late. So we need a workaround at this place.
			$template = $this->getTemplate();
			if ($template !== NULL) {
				$this->loadTemplateValues($template);
			}
		} catch (\Exception $e) {
			// debugging reasons, should we log that somehow? Can be a template blacklisting issue, so
			// could be pretty spammy with useless stuff
			// die($e->getMessage());
			return FALSE;
		}

		// get default template content from register array
		$defaultTemplateContent = $this->getDefaultTemplateContent($template);
		$this->parseValuesForMail($template, $defaultTemplateContent);

		$mail = $this->addMailToMailQueue();
		self::$mailObjectCache[$mail->getUid()] = $mail; // add it to cache to avoid extra DB queries
		if ($this->ignoreMailQueue) {
			return $this->sendMailFromQueue($mail->getUid());
		}

		return TRUE;
	}

	/**
	 * Get the template object
	 *
	 * @return null|Template
	 * @throws \Exception
	 */
	private function getTemplate(): ?Template {
		$siteLanguage = $this->getSiteLanguage();
		$isTemplateBlacklisted = $this->registerService->isTemplateBlacklisted(
			$this->extensionKey,
			$this->templateName,
			(int) $this->pid
		);

		if ($isTemplateBlacklisted) {
			throw new \Exception('The mail template is missing or blacklisted!');
		}

		$templateHash = $this->getTemplateHash();
		if (
			isset(self::$templateObjectCache[$templateHash])
			&& self::$templateObjectCache[$templateHash] instanceof Template
		) {
			return self::$templateObjectCache[$templateHash];
		}

		$templateRepository = $this->getTemplateRepository();
		/** @var Template $template */
		$template = $templateRepository->findByTemplateProperties(
			$this->extensionKey,
			$this->templateName,
			[$siteLanguage],
			$this->pid
		)->getFirst();

		if ($template === NULL && $siteLanguage && !empty($siteLanguage->getFallbackLanguageIds())) {
			$site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($this->pid);
			$fallbackLanguages = [];
			foreach ($siteLanguage->getFallbackLanguageIds() as $fallbackLanguageId) {
				$fallbackLanguages[] = $site->getLanguageById($fallbackLanguageId);
			}

			$template = $templateRepository->findByTemplateProperties(
				$this->extensionKey,
				$this->templateName,
				$fallbackLanguages,
				$this->pid
			)->getFirst();
		}

		// Fallback
		if ($template === NULL) {
			$templateArray = $this->registerService->findTemplate(
				$this->extensionKey,
				$this->templateName,
				$siteLanguage
			);
			$templateArray['pid'] = $this->pid;
			$template = $templateRepository->createWithoutPersist($templateArray);
		}

		self::$templateObjectCache[$templateHash] = $template;
		return $template;
	}

	/**
	 * Get the hash for the object cache
	 *
	 * @return string
	 * @throws SiteNotFoundException
	 */
	private function getTemplateHash(): string {
		$siteLanguage = $this->getSiteLanguage();
		return md5(
			$this->extensionKey . '_' .
			$this->templateName . '_' .
			$this->pid . '_' .
			($siteLanguage ? $siteLanguage->getLanguageId() : 0)
		);
	}

	/**
	 * use all values from the given template
	 *
	 * @param Template $template
	 */
	public function loadTemplateValues(Template $template): void {
		// templates are always on the site root id, so ensure that the mail itself is also saved only on the site root
		$this->setPid((int) $template->getPid());

		// Who knows why this might be useful. At least it doesn't harm.
		$this->extensionKey = $template->getExtensionKey();
		$this->templateName = $template->getTemplateName();

		// Works only if the pid is set properly and this class has its own much better method call. Don't use!
		// $this->siteLanguage = $template->getSiteLanguage();

		// From Name Fallback if not set
		$fromName = trim($template->getFromName());
		if ($fromName === '') {
			$fromName = $this->fromName;
		}

		if ($fromName === '' && $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName']) {
			$fromName = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'];
		}
		$this->setFromName($fromName);

		// overwrite the "from" mail address if set inside the template
		if ($this->fromAddress === '' || $template->getFromMail() !== '') {
			// includes a fallback to the system address
			$fromMail = $this->getValidFromMail(trim($template->getFromMail()));
			$this->setFromAddress($fromMail, $fromName);
		}

		// Append CC/BCC
		$this->appendCcAddresses($template->getCc());
		$this->appendBccAddresses($template->getBcc());

		// overwrite replyTo and To if not set by the user OR if overwritten inside the template
		if ($this->replyToAddress === '' || $template->getReplyTo() !== '') {
			$this->setReplyToAddress($template->getReplyTo());
		}

		if ($this->toAddresses === '' || $template->getToAddress() !== '') {
			$this->setToAddresses($template->getToAddress());
		}

		if (trim($template->getDefaultAttachments()) !== '') {
			$this->setDefaultAttachments($template->getDefaultAttachments());
		}
	}

	/**
	 * Sets the fromMail property of the mailTemplateService.
	 * Checks validity and uses all available fallbacks
	 *
	 * @param string $fromMail
	 * @return string
	 */
	private function getValidFromMail(string $fromMail): string {
		$fromMail = trim($fromMail);
		if (!filter_var($fromMail, FILTER_VALIDATE_EMAIL)) {
			$fromMail = $this->fromAddress;
		}

		if (!filter_var($fromMail, FILTER_VALIDATE_EMAIL)) {
			$fromMail = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'];
			if (!filter_var($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'], FILTER_VALIDATE_EMAIL)) {
				$fromMail = 'noreply@example.com';
			}
		}

		return $fromMail;
	}

	/**
	 * Get the default content for this template
	 *
	 * @param Template|null $template
	 * @return bool|string
	 * @throws NoSuchCacheException
	 */
	protected function getDefaultTemplateContent(Template $template = NULL) {
		$defaultTemplateContent =
			$this->registerService->getRegisterArray()[$this->extensionKey][$this->templateName]['templateContent'];

		// If there is no template for this language, use the default template
		if ($template === NULL && $defaultTemplateContent === NULL) {
			$templatePath =
				$this->registerService->getRegisterArray()[$this->extensionKey][$this->templateName]['templatePath'];

			// only standard template file is considered since version 4.1
			$defaultTemplateFile = $templatePath . 'template.html';
			if (file_exists($defaultTemplateFile)) {
				$defaultTemplateContent = file_get_contents($defaultTemplateFile);
			} else {
				// use configured default html template
				$typoScriptSettingsService = GeneralUtility::makeInstance(TypoScriptSettingsService::class);

				$tsSettings = $typoScriptSettingsService->getSettings(0, 'tx_sgmail');
				$defaultTemplateFile = GeneralUtility::getFileAbsFileName(
					$tsSettings['mail']['defaultHtmlTemplate']
				);

				if (file_exists($defaultTemplateFile)) {
					$defaultTemplateContent = file_get_contents($defaultTemplateFile);
				} else {
					return FALSE;
				}
			}
		} elseif ($template !== NULL) {
			$defaultTemplateContent = $template->getContent();
		} else {
			$defaultTemplateContent = '';
		}

		return $defaultTemplateContent;
	}

	/**
	 * @param string $toAddresses
	 * @return MailTemplateService
	 */
	public function setToAddresses(string $toAddresses): MailTemplateService {
		$normalizedToAddresses = trim(preg_replace('~\x{00a0}~iu', ' ', $toAddresses));
		$this->toAddresses = $normalizedToAddresses;
		return $this;
	}

	/**
	 * Sets the values to send the mail with from the template or register service
	 *
	 * @param Template|null $template
	 * @param string $defaultTemplateContent
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 */
	protected function parseValuesForMail(
		Template $template = NULL,
		string $defaultTemplateContent = ''
	): void {
		/** @var StandaloneView $emailView */
		$emailView = GeneralUtility::makeInstance(StandaloneView::class);

		$emailView->assignMultiple($this->markers);
		$emailView->assign('all_fields', $this->getAllMarker($this->markers));
		$emailView->assign('all_fields_html', $this->getAllMarkerHTML($this->markers));

		//TODO: make this as the lines below the next block
		$overwrittenEmailBody = $this->getOverwrittenEmailBody();
		$overwrittenSubject = '';
		if ($this->subject !== '' && $this->subject !== NULL) {
			$overwrittenSubject = $this->subject;
		}

		// parse markers
		if ($template !== NULL) {
			$subject = $this->parseMarkers(
				trim(empty($overwrittenSubject) ? $template->getSubject() : $overwrittenSubject),
				$emailView
			);
			$layoutId = $template->getLayout();
			$templateContent = $template->getContent();
			$this->setSubjectToSend($subject);
		} else {
			$subject = $this->registerService->getRegisterArray()[$this->extensionKey][$this->templateName]['subject'];
			if (is_array($subject)) {
				$siteLanguage = $this->getSiteLanguage();
				if ($siteLanguage) {
					$subject = trim(
						$this->registerService->getRegisterArray()
						[$this->extensionKey][$this->templateName]['subject'][$siteLanguage->getTypo3Language()]
					);
				}
			}

			$subject = $this->parseMarkers(
				(empty($overwrittenSubject) ? $subject : $overwrittenSubject),
				$emailView
			);

			$layoutId = 0;
			$templateContent = $defaultTemplateContent;
		}

		$this->setSubjectToSend($subject);

		// Parse the markers
		if ($this->fromName) {
			$this->setFromName($this->parseMarkers($this->fromName, $emailView));
		}

		if ($this->fromAddress) {
			$this->setFromAddress($this->parseMarkers($this->fromAddress, $emailView));
		}

		if ($this->replyToAddress) {
			$this->setReplyToAddress($this->parseMarkers($this->replyToAddress, $emailView));
		}

		if ($this->ccAddresses) {
			$this->setCcAddresses($this->parseMarkers($this->ccAddresses, $emailView));
		}

		if ($this->bccAddresses) {
			$this->setBccAddresses($this->parseMarkers($this->bccAddresses, $emailView));
		}

		if ($this->toAddresses) {
			$this->setToAddresses($this->parseMarkers($this->toAddresses, $emailView));
		}

		// reset template source back to default
		$emailView->setTemplateSource(
			empty($overwrittenEmailBody) ? $templateContent : $overwrittenEmailBody
		);

		$emailBody = $emailView->render();
		if ($emailBody === NULL) {
			// Render function from StandaloneView CAN return null in some instances. We need to catch that case,
			// because a lot of PHP string functions do not allow for NULL as parameter any longer
			$emailBody = '';
		}

		// insert <br> tags, but replace every instance of three or more successive breaks with just two.
		if ($template->getRenderWithNl2br()) {
			$emailBody = nl2br($emailBody);
			$emailBody = preg_replace('/(<br[\s]?[\/]?>[\s]*){3,}/', '<br><br>', $emailBody);
		}

		$languageId = 1;
		$siteLanguage = $this->getSiteLanguage();
		if ($siteLanguage) {
			$languageId = $siteLanguage->getLanguageId();
		}

		$layout = $this->getLayoutRepository()->findByUidOrDefault($layoutId, $this->pid, $languageId);
		$emailHTMLHead = '';
		if ($layout) {
			$emailHTMLHead = $layout->getHeadContent();
			$emailBody = str_replace('###CONTENT###', $emailBody, $layout->getContent());
		}

		// don't send duplicate HTML bodies
		if (strpos($emailBody, '<html') !== FALSE) {
			$this->mailBodyToSend = $emailBody;
		} else {
			$this->mailBodyToSend = '<html><head>' . $emailHTMLHead . '</head><body>' . $emailBody . '</body></html>';
		}
	}

	/**
	 * @param mixed $value
	 * @return string
	 */
	private function convertValueToString(mixed $value): string {
		if (is_bool($value)) {
			$convertedValue = $value ? 'true' : 'false';
		} elseif ($value instanceof DateTime) {
			$convertedValue = $value->format('d.m.Y');
		} elseif ($value instanceof FileReference) {
			$originalResource = $value->getOriginalResource();
			if ($originalResource) {
				$value = $originalResource->getPublicUrl();
				$convertedValue = $value;
			} else {
				$convertedValue = get_class($value);
			}
		} elseif (\is_object($value)) {
			if (\method_exists($value, '__toString')) {
				$convertedValue = $value->__toString();
			} else {
				$convertedValue = get_class($value);
			}
		} else {
			$convertedValue = (string) $value;
		}

		return $convertedValue;
	}

	/**
	 * Get a single variable containing a list of all markers
	 *
	 * @param array $markers
	 * @return string
	 */
	private function getAllMarker(array $markers): string {
		$allMarker = '';
		foreach ($markers as $key => $value) {
			// is_numeric check is used to filter out markers without corresponding labels, e.g. markers added via addFileResourceAttachment()
			if (is_numeric($key) || in_array($key, ['user', 'page'], TRUE)) {
				continue;
			}

			if (array_key_exists($key, $this->markerLabels) && $this->markerLabels[$key] !== NULL) {
				$key = $this->markerLabels[$key];
			}

			if (is_array($value)) {
				foreach ($value as $innerKey => $innerValue) {
					if (is_array($value)) {
						foreach ($value as $innerInnerKey => $innerInnerValue) {
							if (is_array($innerInnerValue)) {
								$allMarker .= $key . '.' . $innerKey . '.' . $innerInnerKey . ': ARRAY[]' . PHP_EOL;
							} else {
								$innerInnerValue = $this->convertValueToString($innerInnerValue);
								$allMarker .= $key . '.' . $innerKey . '.' . $innerInnerKey .
									': ' . $innerInnerValue . PHP_EOL;
							}
						}
					} else {
						$innerValue = $this->convertValueToString($innerValue);
						$allMarker .= $key . '.' . $innerKey . ': ' . $innerValue . PHP_EOL;
					}
				}
			} else {
				$value = $this->convertValueToString($value);
				$allMarker .= $key . ': ' . $value . PHP_EOL;
			}
		}

		return $allMarker;
	}

	/**
	 * Get a single variable containing a list of all markers in table markup
	 *
	 * @param array $markers
	 * @return string
	 */
	private function getAllMarkerHTML(array $markers): string {
		$allMarker = '<table>';
		$allMarker .= '<style> table { text-align: left; } table tr th, table tr td { border-bottom: 1px solid rgba(0,0,0,0.2); padding: 2px 6px 4px; vertical-align: top; text-align: left;} </style>';
		foreach ($markers as $key => $value) {
			// is_numeric check is used to filter out markers without corresponding labels, e.g. markers added via addFileResourceAttachment()
			if (is_numeric($key) || in_array($key, ['user', 'page'], TRUE)) {
				continue;
			}

			if (array_key_exists($key, $this->markerLabels) && $this->markerLabels[$key] !== NULL) {
				$key = $this->markerLabels[$key];
			}

			if (is_array($value)) {
				foreach ($value as $innerKey => $innerValue) {
					if (is_array($value)) {
						foreach ($value as $innerInnerKey => $innerInnerValue) {
							if (is_array($innerInnerValue)) {
								$allMarker .= '<tr><th>' . $key . '.' . $innerKey . '.' . $innerInnerKey .
									' </th><td>ARRAY[]</td></tr>';
							} else {
								$innerInnerValue = $this->convertValueToString($innerInnerValue);
								$allMarker .= '<tr><th>' . $key . '.' . $innerKey . '.' . $innerInnerKey .
									' </th><td> ' . $innerInnerValue . '</td></tr>';
							}
						}
					} else {
						$innerValue = $this->convertValueToString($innerValue);
						$allMarker .= '<tr><th>' . $key . '.' . $innerKey . ' </th><td> ' . $innerValue . '</td></tr>';
					}
				}
			} else {
				$value = $this->convertValueToString($value);
				$allMarker .= '<tr><th>' . $key . ' </th><td> ' . $value . '</td></tr>';
			}
		}

		return $allMarker . '</table>';
	}

	/**
	 * Parses markers in an email View.
	 * !!! CHANGES THE SOURCE PATH AND IT SHOULD BE RESET BACK TO THE ORIGINAL!!!
	 *
	 * @param string $text
	 * @param StandaloneView $emailView
	 * @return string
	 */
	protected function parseMarkers(string $text, StandaloneView $emailView): string {
		if (str_contains($text, '{')) {
			$emailView->setTemplateSource($text);
			return $emailView->render();
		}

		return $text;
	}

	/**
	 * Adds new mail to the mail queue.
	 *
	 * @return Mail
	 * @throws IllegalObjectTypeException
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 */
	public function addMailToMailQueue(): Mail {
		$mail = GeneralUtility::makeInstance(Mail::class);
		$mail->setPid($this->pid);
		$mail->setExtensionKey($this->extensionKey);
		$mail->setTemplateName($this->templateName);
		$mail->setSiteLanguage($this->getSiteLanguage());
		$mail->setBlacklisted(
			$this->registerService->isTemplateBlacklisted(
				$this->extensionKey,
				$this->templateName,
				$this->pid
			)
		);
		$mail->setFromAddress($this->fromAddress);
		$mail->setFromName($this->fromName);
		$mail->setToAddress($this->toAddresses);
		$mail->setMailSubject($this->getSubjectToSend());
		$mail->setMailBody($this->getMailBodyToSend());
		$mail->setPriority($this->priority);
		$mail->setBccAddresses($this->bccAddresses);
		$mail->setCcAddresses($this->ccAddresses);
		$mail->setSendingTime(0);
		$mail->setLastSendingTime(0);
		$mail->setReplyTo($this->replyToAddress);
		$mail->setAttachmentPaths($this->defaultAttachments);
		foreach ($this->markers as $marker) {
			if ($marker instanceof FileReference) {
				// we need to create proper copies of the attachment so that the original file reference does not get
				// moved over to the mail model and worst case, the original model loses the reference because of this
				$originalResource = $marker->getOriginalResource();
				if ($originalResource instanceof CoreFileReference) {
					$coreFileReferenceMailFile = GeneralUtility::makeInstance(ResourceFactory::class)
						->createFileReferenceObject(
							[
								'uid_local' => $originalResource->getOriginalFile()->getUid(),
								'table_local' => 'sys_file',
								'uid' => uniqid('NEW_MAIL', TRUE)
							]
						);
					$newFileReference = GeneralUtility::makeInstance(FileReference::class);
					$newFileReference->setOriginalResource($coreFileReferenceMailFile);
					$mail->addAttachment($newFileReference);
				}
			}
		}

		$mailRepository = $this->getMailRepository();
		$mailRepository->add($mail);
		$mailRepository->persist();
		return $mail;
	}

	/**
	 * Send a Mail from the queue, identified by its id
	 *
	 * @param int $uid
	 * @return bool
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function sendMailFromQueue(int $uid): bool {
		if (!isset(self::$mailObjectCache[$uid]) || !self::$mailObjectCache[$uid] instanceof Mail) {
			$mailToSend = $this->getMailObjectByUid($uid);
			if ($mailToSend === FALSE) {
				return FALSE;
			}
		} else {
			$mailToSend = self::$mailObjectCache[$uid];
		}

		$this->sendMailsFromQueue([$mailToSend]);
		$success = FALSE;
		if ($mailToSend->getStatus() === Mail::STATUS_SENT) {
			$success = TRUE;
		}

		unset(self::$mailObjectCache[$uid]); // free the memory
		return $success;
	}

	/**
	 * Get the mail object by uid and check if it's blacklisted
	 *
	 * @param int $uid
	 * @return null|Mail
	 */
	protected function getMailObjectByUid(int $uid): ?Mail {
		$mailRepository = GeneralUtility::makeInstance(MailRepository::class);

		$mailObject = $mailRepository->findOneByUid($uid);
		if (!$mailObject || $mailObject->getBlacklisted()) {
			return NULL;
		}

		return $mailObject;
	}

	/**
	 * Add html and plain body text to the mail message object
	 *
	 * @param MailMessage $mailMessage
	 * @param string $htmlBody
	 * @param string $plainBody
	 */
	protected static function addBodyToMailMessage(
		MailMessage $mailMessage,
		string $htmlBody = '',
		string $plainBody = ''
	): void {
		$mailMessage->html($htmlBody);
		$mailMessage->text($plainBody);
	}

	/**
	 * Attach a file
	 *
	 * @param MailMessage $mailMessage
	 * @param FileInterface|string $file
	 */
	protected static function attachToMailMessage(MailMessage $mailMessage, $file): void {
		if ($file instanceof FileInterface) {
			$mailMessage->attach(
				$file->getContents(),
				$file->getName(),
				$file->getMimeType()
			);
		} elseif (is_string($file)) {
			$info = new FileInfo($file);
			$mailMessage->attach(
				file_get_contents($info->getRealPath()),
				$info->getFilename(),
				$info->getMimeType()
			);
		}
	}

	/**
	 * Sends the given mail objects
	 *
	 * @param Mail[] $mails
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function sendMailsFromQueue(array $mails): void {
		$mailRepository = $this->getMailRepository();
		foreach ($mails as $mail) {
			try {
				/** @var MailMessage $mailMessage */
				$mailMessage = GeneralUtility::makeInstance(MailMessage::class);
				$toAddresses = trim($mail->getToAddress());
				$addressesArray = GeneralUtility::trimExplode(',', $mail->getToAddress(), TRUE);
				if (count($addressesArray) > 1) {
					$toAddresses = $addressesArray;
				}

				if ($toAddresses === '') {
					$isValidMailAddress = \filter_var(
						$GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'],
						FILTER_VALIDATE_EMAIL
					);
					if ($isValidMailAddress) {
						$mailMessage->setTo($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']);
					} else {
						$mail->setStatus(Mail::STATUS_ERROR);
						$mail->setErrorMessage(LocalizationUtility::translate('error.noValidToAddress', 'sg_mail'));
						// @TODO this is queue breaking if you have more errors than mails that should be sent by the scheduler command
						continue;
					}
				} else {
					$mailMessage->setTo($toAddresses);
				}

				$mailMessage->setFrom($mail->getFromAddress(), $mail->getFromName());
				$mailMessage->setSubject($mail->getMailSubject());
				$plaintextService = GeneralUtility::makeInstance(PlaintextService::class);
				$plaintextBody = $plaintextService->makePlain($mail->getMailBody());
				self::addBodyToMailMessage($mailMessage, $mail->getMailBody(), $plaintextBody);

				if ($mail->getBccAddresses()) {
					$mailMessage->setBcc(GeneralUtility::trimExplode(',', $mail->getBccAddresses()));
				}

				if ($mail->getCcAddresses()) {
					$mailMessage->setCc(GeneralUtility::trimExplode(',', $mail->getCcAddresses()));
				}

				if ($mail->getReplyTo()) {
					$mailMessage->setReplyTo($mail->getReplyTo());
				}

				if (!empty($mail->getAttachmentPaths())) {
					// explode the line separated list of attachments and trim the whitespace from the path
					$attachmentPaths = array_map(
						static function (string $attachmentPath) {
							return trim($attachmentPath);
						},
						explode(PHP_EOL, $mail->getAttachmentPaths())
					);

					foreach ($attachmentPaths as $attachmentPath) {
						if (is_file(Environment::getPublicPath() . '/' . $attachmentPath)) {
							$attachmentPath = Environment::getPublicPath() . '/' . $attachmentPath;
						} elseif (!is_file($attachmentPath)) {
							continue;
						}

						self::attachToMailMessage($mailMessage, $attachmentPath);
					}
				}

				$attachments = $mail->getAttachments();
				if ($attachments->count() > 0) {
					foreach ($attachments as $attachment) {
						/** @var FileReference $attachment */
						$originalResource = $attachment->getOriginalResource();
						if (!$originalResource) {
							continue;
						}

						$file = $originalResource->getOriginalFile();
						self::attachToMailMessage($mailMessage, $file);
					}
				}

				$dateTime = new DateTime();
				if ($mail->getSendingTime() === 0) {
					$mail->setSendingTime($dateTime->getTimestamp());
				}
				$mail->setLastSendingTime($dateTime->getTimestamp());

				$success = $mailMessage->send();
				if (!$success) {
					$mail->setStatus(Mail::STATUS_ERROR);
					$mail->setErrorMessage(LocalizationUtility::translate('error.mailsending', 'sg_mail'));
				} else {
					$mail->setErrorMessage('');
					$mail->setStatus(Mail::STATUS_SENT);
				}
			} catch (\Exception $exception) {
				$mail->setStatus(Mail::STATUS_ERROR);
				$mail->setErrorMessage($exception->getMessage());
			}

			$mailRepository->update($mail);
		}

		$mailRepository->persist();
	}
}
