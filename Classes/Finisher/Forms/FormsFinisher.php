<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Finisher\Forms;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use SGalinski\SgMail\Events\BeforeSendingFormsMailEvent;
use SGalinski\SgMail\Service\CsvExportService;
use SGalinski\SgMail\Service\MailTemplateService;
use SGalinski\SgMail\Service\RegisterService;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;

/**
 * This finisher sends an email via sg_mail after form submission and enables customization of mail markers
 */
class FormsFinisher extends AbstractFinisher {
	/**
	 * This contains type names of fields we don't care about in sg_mail
	 *
	 * @var array
	 */
	public const IGNORE_FIELDS = [
		'Honeypot',
		'SgHoneypot'
	];

	/**
	 * @var Site
	 */
	protected $site;

	/**
	 * @var Context
	 */
	protected $context;

	/**
	 * @var EventDispatcherInterface
	 */
	private $eventDispatcher;

	/**
	 * @var CsvExportService
	 */
	protected CsvExportService $csvExportService;

	/**
	 * @param CsvExportService $csvExportService
	 * @throws SiteNotFoundException
	 */
	public function __construct(CsvExportService $csvExportService) {
		$this->csvExportService = $csvExportService;
		if (isset($GLOBALS['TSFE']) && $GLOBALS['TSFE']->id) {
			$this->site = GeneralUtility::makeInstance(SiteFinder::class)
				->getSiteByPageId((int) $GLOBALS['TSFE']->id);
			$this->context = GeneralUtility::makeInstance(Context::class);
		}

		try {
			$this->eventDispatcher = GeneralUtility::getContainer()->get(EventDispatcherInterface::class);
		} catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
			throw new \RuntimeException('Could not initialize EventDispatcherInterface in forms finisher.');
		}
	}

	/**
	 * Get the current SiteLanguage
	 *
	 * @return SiteLanguage
	 * @throws AspectNotFoundException
	 */
	protected function getSiteLanguage(): SiteLanguage {
		$languageId = $this->context->getPropertyFromAspect('language', 'id', 0);
		return $this->site->getLanguageById($languageId);
	}

	/**
	 * Send email with the sgmail api to one or more recipients
	 * overwrites the mail markers with custom identifiers if provided
	 *
	 * @return null|string
	 * @throws AspectNotFoundException
	 * @throws IllegalObjectTypeException
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 * @throws UnknownObjectException
	 * @throws InsufficientFolderAccessPermissionsException|\TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException
	 */
	protected function executeInternal(): ?string {
		$formValues = $this->finisherContext->getFormValues();
		$formRuntime = $this->finisherContext->getFormRuntime();
		$formDefinition = $formRuntime->getFormDefinition();

		$markers = [];
		$markerLabels = [];
		foreach ($formValues as $identifier => $value) {
			$formElement = $formDefinition->getElementByIdentifier($identifier);
			if (!$formElement) {
				continue;
			}

			$type = $formElement->getType();
			if (\in_array($type, self::IGNORE_FIELDS, TRUE)) {
				continue;
			}

			$markerName = $markerLabel = $identifier;
			$formElementProperties = $formElement->getProperties();
			if (isset($formElementProperties['markerName']) &&
				\trim($formElementProperties['markerName']) !== ''
			) {
				$markerNameArray = GeneralUtility::trimExplode(
					',',
					\trim($formElementProperties['markerName']),
					FALSE,
					2
				);
				$markerName = $markerNameArray[0] ?? $identifier;
				$markerLabel = $markerNameArray[1] ?? $identifier;
			}

			if ($type === 'StaticText') {
				$value = $formElement->getLabel();
			}

			$markerLabels[$markerName] = $markerLabel;
			$markerName = RegisterService::normalizeFormFieldMarkerName($markerName);
			if (is_array($value)) {
				$value = implode(', ', $value);
			}

			$markers[$markerName] = $value;
		}

		$templateName = trim((string) $this->parseOption('template'));
		if ($templateName === '') {
			$templateName = $formDefinition->getIdentifier();
		}

		$extensionKey = trim((string) $this->parseOption('extension'));
		$extensionKey = $extensionKey ?: 'sg_mail';
		$mailTemplateService = GeneralUtility::makeInstance(
			MailTemplateService::class,
			$templateName,
			$extensionKey,
			$markers,
			$markerLabels
		);

		$mailTemplateService->setIgnoreMailQueue(TRUE);
		$mailTemplateService->setSiteLanguage($this->getSiteLanguage());
		$mailTemplateService->setPid($this->site->getRootPageId());

		$mailToAddress = trim((string) $this->parseOption('mailTo'));
		$mailToReplacementAddresses = $this->parseOption('mailToReplacement');
		if (is_array($mailToReplacementAddresses) && count($mailToReplacementAddresses)) {
			$defaultReplacement = '';
			foreach ($mailToReplacementAddresses as $replacement => $search) {
				$search = trim($search);
				$replacement = trim($replacement);
				if ($defaultReplacement === '') {
					$defaultReplacement = $replacement;
				}

				// Case: fill mail address if only a replacement is added and all other fields are empty
				if ($search === '' && $mailToAddress === '') {
					$mailToAddress = $replacement;
					continue;
				}

				if ($mailToAddress === $search) {
					$mailToAddress = $replacement;
				}
			}

			// take the first entry of the list if the mail address can't be validated, acts also as the default
			if (!filter_var($mailToAddress, FILTER_VALIDATE_EMAIL)) {
				$mailToAddress = $defaultReplacement;
			}
		}

		if ($mailToAddress !== '' && filter_var($mailToAddress, FILTER_VALIDATE_EMAIL)) {
			$mailTemplateService->setToAddresses($mailToAddress);
		}

		$fromAddress = trim((string) $this->parseOption('mailFrom'));
		if ($fromAddress !== '') {
			$mailTemplateService->setFromAddress($fromAddress);
		}

		$fromName = trim((string) $this->parseOption('mailFromName'));
		if ($fromName !== '') {
			$mailTemplateService->setFromName($fromName);
		}

		$replyTo = trim((string) $this->parseOption('replyTo'));
		if ($replyTo !== '') {
			$mailTemplateService->setReplyToAddress($replyTo);
		}

		$ccAddresses = trim((string) $this->parseOption('cc'));
		if ($ccAddresses !== '') {
			$mailTemplateService->setCcAddresses($ccAddresses);
		}

		$bccAddresses = trim((string) $this->parseOption('bcc'));
		if ($bccAddresses !== '') {
			$mailTemplateService->setBccAddresses($bccAddresses);
		}

		$exportCsv = (bool) $this->parseOption('exportCsv');
		if ($exportCsv) {
			$csvTempFileName = $this->csvExportService->generateCsvFromMarkerDataAndAttachToMail(
				$markers,
				$markerLabels,
				$mailTemplateService
			);
		}

		/** @var BeforeSendingFormsMailEvent $event */
		$event = $this->eventDispatcher->dispatch(
			new BeforeSendingFormsMailEvent($mailTemplateService, $this->finisherContext)
		);
		$mailTemplateService = $event->getMailTemplateService();

		$mailTemplateService->sendEmail();

		// remove csv file after sending the email
		if ($exportCsv && $csvTempFileName !== '') {
			$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
				\TYPO3\CMS\Core\Resource\ResourceFactory::class
			);

			$defaultStorage = $resourceFactory->getDefaultStorage();
			if ($defaultStorage && $defaultStorage->hasFile($csvTempFileName)) {
				$csvFile = $defaultStorage->getFileByIdentifier($csvTempFileName);
				$csvFile->delete();
			}
		}

		return NULL;
	}
}
