# Ext: sg_mail

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/sg_mail

Please report bugs here: https://gitlab.sgalinski.de/typo3/sg_mail

## About

This extension provides an email templating service and mail queue functionality for all your TYPO3 extensions.
It also supports Templates in various languages, which can be managed in the backend.

Additionally, sg_mail comes with multiple features that help to integrate with [ext:form](https://docs.typo3.org/typo3cms/extensions/form/).

sg_mail is multi-site and multi-language ready.

## Usage
### Registering your Extension

To register your extension with sg_mail, you need a **Configuration File** for each template you want to integrate.
It needs to be a *.php* file and has to be inside the configuration folder
**typo3conf/ext/{your_extension}/Configuration/SgMail** of your extension.

Additionally, you need to make your registration file known in your **ext_localconf.php** file:

```PHP
// register mail templates
$GLOBALS['sg_mail']['my_extension_key']['unique_template_name'] = 'EXT:my_extension_key/Configuration/SgMail/MyTemplate.php';
```

sg_mail will automatically parse valid configuration files of all extensions within your TYPO3 instance.

Inside the file you have mandatory and optional settings you can define:

#### Mandatory

- **extension_key**: Needed to associate this template with the appropriate extension,
- **template_key**: A unique identifier of your template.
- **description**: A short description of your templates usage, displayed in the backend template editor. This should be a **language label**
- **subject**: The default mail subject used for this Template. Here you can also provide a language label.
- **markers**: An array of placeholder variables.
  Are dynamically replaced by the appropriate values. If you don't want any markers, provide an empty array.

  A marker needs to be structured as follows:

  - **marker**: The variables name used in your template.
  - **type**: Here you can specify the variable type by using one of the constants in the **\SGalinski\SgMail\Service\MailTemplateService** class.
  - **value**: An example value of this marker. Also used for previewing your mails in the backend module.
  - **description**: A short text describing the purpose of this marker.


#### Optional

- **template_path**: You can provide a path to your mail template that differs from the default path with this setting.
- **renderWithNl2br**: You can set this to false, if you don't want newlines to automatically be converted to HTML linebreaks. (Default is true)
#### The template folder

If not overwritten in your configuration file, the default location for your extensions templates is **ext/sg_example/Resources/Private/Templates/SgMail/{TemplateNameInUpperCamelCase}**.
Inside this folder you need a folder for every template you want to register. The name of the template file itself should be **template.html**

#### Example:

Your configuration file for the template "confirm_mail" should be located at **../Configuration/ConfirmMail.php**.

A possible ConfirmMail.php file could look like this:

```PHP
return [
    'extension_key' => 'sg_example',
    'template_key' => 'confirm_mail',
    'description' => 'LLL:EXT:sg_example/Resources/Private/Language/locallang.xlf:mail.confirm.description',
    'subject' => 'LLL:EXT:sg_example/Resources/Private/Language/locallang.xlf:mail.confirm.subject',
    'markers' => [
        [
            'marker' => 'username',
            'type' => \SGalinski\SgMail\Service\MailTemplateService::MARKER_TYPE_STRING,
            'value' => 'max.mustermann@example.org',
            'description' => 'LLL:EXT:sg_energieportal/Resources/Private/Language/locallang.xlf:mail.marker.username'
        ],
        //... more markers
    ]
];
```

The default path to the template.html file: ext/sg_example/Resources/Private/Templates/SgMail/ConfirmMail

### Send an email with the MailTemplateService

Basic Usage:

1. Get an instance of MailTemplateService
2. Provide the **template_name**, **extension_key** and marker array in the constructor
3. Set or override all desired fields (i.e. setLanguage, setToAddresses, setTemplateName, setExtensionKey etc.)
4. Invoke the **sendEmail()** function

Example:

```PHP
// get an instance of the service
/** @var MailTemplateService $mailService */
$mailService = GeneralUtility::makeInstance(MailTemplateService::class, 'confirm_mail', 'sg_example', ['username' => $username]);

$mailService->setFromName($this->settings['fromName']);
$mailService->setFromAddress($this->settings['fromEmail']);
$mailService->setToAddresses([$emailAddress]);
$mailService->setIgnoreMailQueue(TRUE);

// set the pageId of the rootpage, otherwise the templates could be duplicated (mandatory since version 4.0!!!)
$site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($GLOBALS['TSFE']->id);
$mailService->setPid($site->getRootPageId());

// set the proper language for the mail (not necessary if you want the default language)
$mailService->setSiteLanguage(GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId(123)->getLanguageById(0));

// finally send the mail
$success = $mailService->sendEmail();
if (!$success) {
    // the mail sending failed
}
```

### The mail queue

The mail queue holds all the mails that have been sent or should be sent in the future.
You can view the mail queue in the backend module and interact with the entries.

You can set the priority (setPriority function of the service) for your mails with the constants
of the **SGalinski\SgMail\Domain\Model\Mail** class. With the Flag **ignoreMailQueue** you can switch between
queueing your Mail (false) or immediately sending it (true).

* PRIORITY_LOWEST
* PRIORITY_LOW
* PRIORITY_MEDIUM
* PRIORITY_HIGH
* PRIORITY_HIGHEST

The higher the priority, the more likely the mail will get sent immediately (depending on how "busy" the MailQueue is).

#### The command controller

To enable the mail queue functionality you need to configure the **SendMailCommandController**.

Go to your scheduler module in the TYPO3 Backend and set up a scheduled CommandController task.
Choose the **SgMail SendMail:runSendMail** CommandController command and supply a frequency in seconds or [cron](https://en.wikipedia.org/wiki/Cron) format.

For more information on the TYPO3 scheduler extension read its [manual](https://docs.typo3.org/typo3cms/extensions/scheduler/Index.html).

## Language handling

When using the MailTemplateService API, you need to provide an instance of `SiteLanguage` to the API either by setting it via `setSiteLanguage` or by loading values from a `Template` instance which has the `SiteLanguage` set.

In your template editor you automatically have all languages of your page shown.

### Example

```Typoscript
finishers {
    1 {
        class = SGalinski\SgMail\Service\FormhandlerFinisherService
        config {
            checkBinaryCrLf = message
            template_key = your_template
            extension_key = extension_key
            to_address = admin@sgalinski.de
            from_address = info@sgalinski.de
            from_name = sgalinski.de
            ignore_mail_queue = TRUE
        }
    }
}
```

## The Backend Module
With this extension comes a new module in the "WEB" section of your TYPO3 Backend.

Clicking on it loads the administration panel for all your mail templates and the mailing queue.

The module provides two modes:

In the **Template Editor** mode you see an overview of all the template markers for the selected template. Here you can edit & reset your templates in all the languages of your TYPO3 instance. Additionally you can test your mails by entering an email address and clicking **Send Test Mail**.

With the **Mail Queue** mode, you can see the current content of your mailing queue. For each queue entry you have the following options:

<img height="20px" width="20px" src="https://camo.githubusercontent.com/e3de477db4caba47f3adc70db73b401be474ec23/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6f70656e2e737667"> Edit
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/3c5e6daff1f31fd3c4bf0ac3e70520133d06441c/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d686964652e737667"> Disable/Enable
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/7773a0fc11517dc70b81f9ba516991f3669493e1/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d64656c6574652e737667"> Delete
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/ee057cb37045beeccf8078f74e65e1774ec5e001/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d696e666f2e737667"> Show further information
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/91c383d7beded93dbe6a62e2a1ae94bf82d1d783/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d686973746f72792d6f70656e2e737667"> Show history
<br>
.... Expand/Collapse the options menu
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/53fd52618f310e4c31cca5a57df3b314bd0a7a9c/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d696e736572742d7265666572656e63652e737667"> Send the mail again (if already sent)
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/53fd52618f310e4c31cca5a57df3b314bd0a7a9c/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d696e736572742d7265666572656e63652e737667"> Send this mail (if not already sent)
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/4b1188209e740e17a4ec0cd6583425696809017b/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d766965772e737667"> View the content of this mail

Additionally, you can now filter the mail queue or export it to a csv file.

## Blacklisting templates

It is possible to exlude certain templates for all or specific domains.
Excluded templates are not shown in the backend module, and their mails are never sent.
Mails from these templates are still logged, and you can search for them in the backend module.

### Blacklisting templates for all domains
In the extension configuration setting **general.excludeTemplatesAllDomains**, you can specify which templates should be excluded for all domains with a comma seperated list.

Example:

```
sg_comments.approved, sg_comments.declined
```

The templates "approved" and "declined" of the extension sg_comments are blacklisted for every domain.

### Blacklisting templates for certain domains

In the extension configuration setting **general.excludeTemplates**, ou can specify which templates should be excluded for which domains with a Semicolon separated list of comma separated list.

The comma seperated lists contain the page id, and the templates to be excluded.

Example:

```
1, sg_comments.approved, sg_example.welcome;10, sg_comments.declined
```

The template "approved" of the extension sg_comments, and the template "welcome" of the extension sg_example are blacklisted for the domain with the page id 1.

The template "declined" of the extension sg_comments is blacklisted for the domain with the page id 10.

If the page ids are missing or not correspond to a site root, the configuration has simply no effect.

### Searching for emails from blacklisted templates
In the backend module mode "Queue" you can filter for "Blacklisted" mails. Note: you shouldn't select a template filter to see the blacklisted mails.

## Developer Guide

### Database and Models

This extension uses two database tables:

- **tx_sgmail_domain_model_template**: Stores all template settings that have been provided via the backend module. The Default settings exist only in the register array and not in the database.

- **tx_sgmail_domain_model_mail**:
  This table holds all the mails in the mailing queue.
  When they are sent, the flag **sent** is set to true, and the **sending_time** is inserted.
  You can inspect the mailing queue with the backend module.
  If an email should ignore the mail queue, it is still inserted in this table with the **sent** flag set to true.
  The mails have a status associated to them. If a mail fails to submit, the status will be set to error and the error message is saved in the error_message field so you can review it for the individual mail.

The extbase model classes for these tables are located at the *Domain\Model* folder.

### Service Classes

#### MailTemplateService
This class provides you with an API to the mailing functionality and various helper functions regarding the your templates.

Here is an overview of some important functions:

###### function getDefaultTemplateMarker
Reads custom example template marker from your **locallang.xlf**. This is only useful if you need multi language examples in your Backend Marker

###### function sendEmail
Sends your mail or adds it to the mailing queue, depending on the settings. You can specify in with the boolean parameter if the preview values should be used or not.

###### function sendMailFromQueue
Forces the sending of an E-Mail from within the queue. If it has already been sent, it gets send again.

###### function sendMailsFromQueue
Same as sendMailFromQueue, but this method accepts an array of `Mail` instances instead of the uid of one `Mail`.

###### function addFileResourceAttachment
You can attach a file resource to the mail that will be sent via the markers array.

###### function addMailToMailQueue
Add the currently built mail to the mail queue

###### functions setX
Sets the various variables to the supplied values. With these functions you can overwrite for instance from/cc/bcc Adresses, from name etc.

You can also tell the **MailTemplateService** to not ignore this mail when adding to the mailing queue: **function setIgnoreMailQueue**.

---

### Ext:form Integration

#### Template marker
This extension provides you the possibility to customize the identifier of the form input fields. These identifiers are used in the mail templates as marker names.
To change a fields identifier, select it in the form editor and enter your desired identifier in the field labeled "Email template marker name". After saving the form, the change identifier name will be overwritten.

#### New mail finisher
sg_mail comes with two new finisher for your forms with the following settings regarding sg_mail integration:

- **Template key**: If this field is empty, a unique template name will be generated and automatically inserted into the field, after the form definition has been saved.
  The generated template name is based on the form identifier, concatenated with the finisher identifier (separated by `-`).
- **Automatic Registration**: A sg_mail registration will automatically be created. See the **Automatic Registration** section for more information
- **Ignore Mail Queue**: Mails will get send immediately and not added to the mail queue


#### Automatic Registration
If you select to automatically register your forms with sg_mail, the default html template for the emails will be used.

You can customize the TypoScript settings in the **setup.ts** in order to have control over automatically generated registration files:

```Typoscript
# extension where automatic generated registrations are put. Will be appended with /Configuration/MailTemplates/. Make sure these folders exist!
configurationLocation = your_extension_key

# default html template file, which serves as a fallback for all mail templates
defaultHtmlTemplate = EXT:project_theme/Configuration/MailTemplates/Default.html
```


---

### Events

- **BeforeSendingFormsMailEvent:** Contains the mutable mail template and context of the submitted form.

#### How to listen to the events (e.g. BeforeSendingFormsMailEvent)

Register a Listener in your Services.yaml of your extension

```yaml
services:
  SGalinski\SomeExtension\EventListener\MyListener:
    tags:
      - name: event.listener
        identifier: 'myListener'
        event: SGalinski\SgMail\Events\BeforeSendingFormsMailEvent
```

Create your listener class

```php
namespace SGalinski\SomeExtension\EventListener;
use SGalinski\SgMail\Events\BeforeSendingFormsMailEvent;

class MyListener
{
    public function __invoke(BeforeSendingFormsMailEvent $event): void
    {
        /** Logic goes here **/
    }
}
```

---

#### TypoScriptSettingsService

This Service enables you to access your typoscript settings outside your controller classes.
You simply need to supply your page id, and the extension key.
If you have no page id specific settings, set the page id to 0.
