<?php

use SGalinski\SgMail\Domain\Model\Mail;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail',
		'label' => 'mail_subject',
		'label_alt_force' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'status, error_message, blacklisted, mail_subject, mail_body, to_address, from_address, from_name, bcc_addresses, cc_addresses, extension_key, template_name, sending_time, last_sending_time, language',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'translationSource' => 'l10n_source',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'enablecolumns' => [
			'disabled' => 'hidden',
		],
		'default_sortby' => 'ORDER BY priority DESC',
		'iconfile' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIconTCA.svg',
		'hideTable' => TRUE,
		'hideAtCopy' => TRUE,
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'hidden,--palette--;;1,blacklisted,extension_key,sys_language_uid,status,error_message,--palette--;;priority,--palette--;;sent_by,to_address,mail_subject,mail_body,--palette--;;mail,--palette--;;copy,reply_to,--palette--;;sending_time,attachments'
		],
	],
	'palettes' => [
		'sent_by' => [
			'showitem' => 'from_name, from_address'
		],
		'priority' => [
			'showitem' => 'template_name, priority'
		],
		'copy' => [
			'showitem' => 'bcc_addresses, cc_addresses'
		],
		'sending_time' => [
			'showitem' => 'sending_time, last_sending_time'
		]
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language']
		],
		'status' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.status',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.status.pending', 'value' => Mail::STATUS_PENDING],
					['label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.status.sent', 'value' => Mail::STATUS_SENT],
					['label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.status.error', 'value' => Mail::STATUS_ERROR],
				]
			]
		],
		'error_message' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.error_message',
			'config' => [
				'type' => 'text',
				'readOnly' => TRUE
			],
			'displayCond' => 'FIELD:status:=:' . Mail::STATUS_ERROR
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
			'config' => [
				'type' => 'check',
			],
		],
		'blacklisted' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.blacklisted',
			'config' => [
				'type' => 'check',
			],
		],
		'mail_subject' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.mail_subject',
			'config' => [
				'type' => 'input',
				'eval' => 'trim',
				'required' => TRUE
			],
		],
		'mail_body' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.mail_body',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10,
				'eval' => 'trim',
			],
		],
		'to_address' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.to_address',
			'config' => [
				'type' => 'email',
				'required' => TRUE
			],
		],
		'from_address' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.from_address',
			'config' => [
				'type' => 'email',
				'required' => TRUE
			],
		],
		'priority' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.priority',
			'config' => [
				'type' => 'number'
			],
		],
		'from_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.from_name',
			'config' => [
				'type' => 'input',
				'eval' => ''
			],
		],
		'cc_addresses' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.cc_addresses',
			'config' => [
				'type' => 'input',
				'eval' => ''
			],
		],
		'bcc_addresses' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.bcc_addresses',
			'config' => [
				'type' => 'input',
				'eval' => ''
			],
		],
		'extension_key' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.extension_key',
			'config' => [
				'type' => 'input'
			],
		],
		'template_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.template_name',
			'config' => [
				'type' => 'input'
			],
		],
		'sending_time' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.sending_time',
			'config' => [
				'type' => 'datetime',
				'size' => 13,
				'checkbox' => 0,
				'default' => 0,
				'readOnly' => TRUE
			],
		],
		'last_sending_time' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.last_sending_time',
			'config' => [
				'type' => 'datetime',
				'size' => 13,
				'checkbox' => 0,
				'default' => 0,
				'readOnly' => TRUE
			],
		],
		'reply_to' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_mail.reply_to',
			'config' => [
				'type' => 'input'
			],
		],
		'attachments' => [
			'exclude' => TRUE,
			'label' => 'Attachments',
			'config' => [
				'type' => 'file',
				'maxitems' => 9999
			],
		],
		'attachment_paths' => [
			'exclude' => TRUE,
			'label' => 'Attachment Paths',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10
			]
		]
	]
];

return $columns;
