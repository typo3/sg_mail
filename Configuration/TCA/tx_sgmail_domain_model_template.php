<?php

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template',
		'label' => 'extension_key',
		'label_alt' => 'template_name',
		'label_alt_force' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'extension_key, template_name, language, subject, from_name, from_mail, reply_to, to_address',
		'delete' => 'deleted',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'translationSource' => 'l10n_source',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'enablecolumns' => [
			'disabled' => 'hidden',
		],
		'default_sortby' => 'ORDER BY extension_key ASC, template_name ASC',
		'iconfile' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIconTCA.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'hidden,--palette--;;1,sys_language_uid,extension_key,layout,template_name,subject,from_name,from_mail,to_address,content,cc,bcc,reply_to,render_with_nl2br'
		],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language']
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
			'config' => [
				'type' => 'check',
			],
		],
		'render_with_nl2br' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.render_with_nl2br',
			'config' => [
				'type' => 'check',
				'default' => '1'
			],
		],
		'layout' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.layout',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0]
				],
				'foreign_table' => 'tx_sgmail_domain_model_layout',
				'foreign_table_where' => 'AND tx_sgmail_domain_model_layout.pid=###CURRENT_PID### AND tx_sgmail_domain_model_layout.sys_language_uid IN (-1,0)',
				'default' => 0
			],
		],
		'extension_key' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.extension_key',
			'config' => [
				'type' => 'input',
				'eval' => 'trim',
				'required' => TRUE
			],
		],
		'content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.content',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10,
			],
		],
		'template_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.template_name',
			'config' => [
				'type' => 'input',
				'eval' => 'trim',
				'required' => TRUE
			],
		],
		'subject' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.subject',
			'config' => [
				'type' => 'input',
				'required' => TRUE
			],
		],
		'from_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.from_name',
			'config' => [
				'type' => 'input'
			],
		],
		'from_mail' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.from_mail',
			'config' => [
				'type' => 'input'
			],
		],
		'cc' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.cc',
			'config' => [
				'type' => 'input'
			],
		],
		'bcc' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.bcc',
			'config' => [
				'type' => 'input'
			],
		],
		'reply_to' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.reply_to',
			'config' => [
				'type' => 'input'
			],
		],
		'to_address' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.to_address',
			'config' => [
				'type' => 'input'
			],
		],
		'default_attachments' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.default_attachments',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10
			]
		]
	]
];

return $columns;
