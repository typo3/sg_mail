<?php

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'searchFields' => 'name, content',
		'delete' => 'deleted',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'translationSource' => 'l10n_source',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'enablecolumns' => [
			'disabled' => 'hidden',
		],
		'default_sortby' => 'name',
		'hideTable' => TRUE,
		'iconfile' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIconTCA.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'hidden,--palette--;;name,head_content,content'
		],
	],
	'palettes' => [
		'name' => [
			'showitem' => 'name, default'
		],
	],
	'columns' => [
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
			'config' => [
				'type' => 'check',
			],
		],
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => ['type' => 'language']
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0]
				],
				'foreign_table' => 'tx_sgmail_domain_model_layout',
				'foreign_table_where' => 'AND tx_sgmail_domain_model_layout.uid=###REC_FIELD_l10n_parent### AND tx_sgmail_domain_model_layout.sys_language_uid IN (-1,0)',
				'default' => 0
			]
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
				'default' => ''
			]
		],
		'default' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.default',
			'config' => [
				'type' => 'check',
			],
		],
		'name' => [
			'exclude' => TRUE,
			'l10n_display' => 'defaultAsReadonly',
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.name',
			'config' => [
				'type' => 'input',
				'eval' => 'trim',
				'required' => TRUE
			],
		],
		'content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.content',
			'description' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.content.info',
			'config' => [
				'type' => 'text',
				'renderType' => 't3editor'
			],
		],
		'head_content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.head_content',
			'description' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.head_content.info',
			'config' => [
				'type' => 'text',
				'renderType' => 't3editor'
			],
		],
	]
];

return $columns;
