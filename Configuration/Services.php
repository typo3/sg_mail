<?php

use SGalinski\SgMail\Command\DeleteOldMailsCommand;
use SGalinski\SgMail\Command\SendMailCommandController;
use SGalinski\SgMail\Domain\Repository\FrontendUserGroupRepository;
use SGalinski\SgMail\Domain\Repository\LayoutRepository;
use SGalinski\SgMail\Domain\Repository\MailRepository;
use SGalinski\SgMail\Domain\Repository\TemplateRepository;
use SGalinski\SgMail\Event\Listener\ExcludeFromReferenceIndexListener;
use SGalinski\SgMail\Finisher\Forms\FormsFinisher;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\DataHandling\Event\IsTableExcludedFromReferenceIndexEvent;
use TYPO3\CMS\Form\Mvc\Persistence\FormPersistenceManager;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgMail\\', __DIR__ . '/../Classes/');

	$services->set(DeleteOldMailsCommand::class)
		->tag('console.command', ['command' => 'sg_mail:deleteOldMails']);
	$services->set(SendMailCommandController::class)
		->tag('console.command', ['command' => 'sg_mail:sendMail']);
	$services->set(FrontendUserGroupRepository::class)
		->public();
	$services->set(LayoutRepository::class)
		->public();
	$services->set(MailRepository::class)
		->public();
	$services->set(TemplateRepository::class)
		->public();
	$services->set(FormsFinisher::class)
		->public();
	// We need to allow the initialisation via GeneralUtility for our FormFinisher migration wizard
	$services->set(FormPersistenceManager::class)
		->public();
	$services->set(ExcludeFromReferenceIndexListener::class)
		->tag('event.listener', [
			'identifier' => 'sg-mail/excludeFromRefIndex',
			'event' => IsTableExcludedFromReferenceIndexEvent::class
		]);
};
