<?php

return [
	'dependencies' => [
		'core',
		'backend'
	],
	'imports' => [
		'@sgalinski/sg-mail/' => 'EXT:sg_mail/Resources/Public/JavaScript/'
	]
];
