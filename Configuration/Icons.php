<?php

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

$iconList = [];
$sgMailIcons = [
	'extension-sg_mail-module' => 'ModuleIcon.svg',
];

foreach ($sgMailIcons as $identifier => $path) {
	$iconList[$identifier] = [
		'provider' => SvgIconProvider::class,
		'source' => 'EXT:sg_mail/Resources/Public/Icons/' . $path,
	];
}

return $iconList;
