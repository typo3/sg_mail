<?php

use SGalinski\SgMail\Controller\ConfigurationController;
use SGalinski\SgMail\Controller\LayoutController;
use SGalinski\SgMail\Controller\MailController;
use SGalinski\SgMail\Controller\NewsletterController;
use SGalinski\SgMail\Controller\QueueController;
use SGalinski\SgMail\Controller\SiteController;

return [
	'web_SgMail' => [
		'parent' => 'web',
		'access' => 'user',
		'workspaces' => 'live',
		'path' => '/module/web/SgMail',
		'labels' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang.xlf',
		'iconIdentifier' => 'extension-sg_mail-module',
		'extensionName' => 'SgMail',
		'controllerActions' => [
			MailController::class => [
				'index', 'sendTestMail', 'empty', 'reset'
			],
			QueueController::class => [
				'index', 'sendMail', 'export', 'preview'
			],
			ConfigurationController::class => [
				'index', 'create', 'edit', 'delete'
			],
			LayoutController::class => [
				'index'
			],
			NewsletterController::class => [
				'index', 'sendTestMail', 'empty'
			],
			SiteController::class => [
				'index'
			]
		]
	]
];
