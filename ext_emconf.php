<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "sg_mail".
 ***************************************************************/

$EM_CONF['sg_mail'] = [
	'title' => 'Mail Templates',
	'description' => 'Mail Templates',
	'category' => 'module',
	'version' => '9.2.3',
	'state' => 'stable',
	'author' => 'Fabian Galinski',
	'author_email' => 'fabian@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' => [
		'depends' => [
			'typo3' => '12.4.0-12.4.99',
			'php' => '8.1.0-8.3.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
