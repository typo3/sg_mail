<?php

use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;

call_user_func(
	static function () {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_sgmail_domain_model_mail'] = [
			'dateField' => 'sending_time',
			'expirePeriod' => 180
		];
		$GLOBALS['TYPO3_CONF_VARS']['BE']['stylesheets']['sg_mail'] = 'EXT:sg_mail/Resources/Public/StyleSheets/BackendSkin';
	}
);
