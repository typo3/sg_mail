CREATE TABLE tx_sgmail_domain_model_mail (
	mail_subject text NOT NULL,
	mail_body text NOT NULL,
	to_address varchar(255) DEFAULT '' NOT NULL,
	from_address varchar(255) DEFAULT '' NOT NULL,
	from_name varchar(255) DEFAULT '' NOT NULL,
	cc_addresses varchar(255) DEFAULT '' NOT NULL,
	bcc_addresses varchar(255) DEFAULT '' NOT NULL,
	priority int(11) unsigned DEFAULT '0' NOT NULL,
	extension_key varchar(255) DEFAULT '' NOT NULL,
	template_name varchar(255) DEFAULT '' NOT NULL,
	reply_to varchar(255) DEFAULT '' NOT NULL,
	sending_time int(11) unsigned DEFAULT '0' NOT NULL,
	last_sending_time int(11) unsigned DEFAULT '0' NOT NULL,
	blacklisted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	attachments int(11) unsigned DEFAULT '0' NOT NULL,
	attachment_paths text,
	status varchar(255) DEFAULT 'pending' NOT NULL,
	error_message text
);

CREATE TABLE tx_sgmail_domain_model_template (
	layout int(11) DEFAULT '0' NOT NULL,
	subject text NOT NULL,
	extension_key varchar(255) DEFAULT '' NOT NULL,
	template_name varchar(255) DEFAULT '' NOT NULL,
	from_name varchar(255) DEFAULT '' NOT NULL,
	from_mail varchar(255) DEFAULT '' NOT NULL,
	to_address varchar(255) DEFAULT '' NOT NULL,
	cc varchar(255) DEFAULT '' NOT NULL,
	bcc varchar(255) DEFAULT '' NOT NULL,
	reply_to varchar(255) DEFAULT '' NOT NULL,
	content text NOT NULL,
	default_attachments text NOT NULL,
	render_with_nl2br tinyint(4) DEFAULT '1' NOT NULL
);

CREATE TABLE tx_sgmail_domain_model_layout (
	default tinyint(4) DEFAULT '0' NOT NULL,
	name tinytext NOT NULL,
	content text,
	head_content text
);
